
#import "SaveVedio.h"
#import <AssetsLibrary/ALAssetsLibrary.h>

@implementation SaveVedio


+ (void)indirectShareVedio:(NSURL *)url {
    NSArray *activityItems = @[url];

    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityItems
                                                                            applicationActivities:nil];
    //不出现在活动项目
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard,UIActivityTypeAssignToContact,UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList];

    //给activityVC的属性completionHandler写一个block。
    //用以UIActivityViewController执行结束后，被调用，做一些后续处理。
    UIActivityViewControllerCompletionWithItemsHandler myBlock = ^(UIActivityType activityType, BOOL completed, NSArray * returnedItems, NSError * activityError)
    {

        if (completed)
        {

        }
        else
        {

        }
    };

    // 初始化completionHandler，当post结束之后（无论是done还是cancell）该blog都会被调用
    activityVC.completionWithItemsHandler = myBlock;

    UIViewController * rootVc = [UIApplication sharedApplication].keyWindow.rootViewController;
    [rootVc presentViewController:activityVC animated:TRUE completion:nil];
}
+ (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *) filePathString
{
    NSURL* URL= [NSURL fileURLWithPath: filePathString];
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    else
    {
        NSLog(@"Success excluding from backup");
    }
    return success;
}

@end


#if defined (__cplusplus)
extern "C" {
#endif

    void _save(char* filePath)
    {
        NSString *urlPath = [NSString stringWithUTF8String:filePath];
        //[SaveVedio addSkipBackupAttributeToItemAtPath:urlPath];
        NSURL *url=[NSURL URLWithString:urlPath];
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library writeVideoAtPathToSavedPhotosAlbum:url
                                    completionBlock:^(NSURL *assetURL, NSError *error) {
                                        if (error) {
                                            NSLog(@"保存视频到相册失败:%@",error);
                                        } else {
                                            NSLog(@"保存视频到相册成功:%@",assetURL);
                                            NSString *s=[[assetURL absoluteString] stringByRemovingPercentEncoding];
                                            const char * urlChar=[s UTF8String];
                                            UnitySendMessage( "UIPanel", "SaveVideoCallback", urlChar);
//                                            UnitySendMessage( "CanvasShare", "SaveVideo", (@"save  photo failed").UTF8String);
                                           // [SaveVedio indirectShareVedio:assetURL];
                                        }
                                    }];
    }
    
    void _share(char* filePath)
    {
         NSString *urlPath = [NSString stringWithUTF8String:filePath];
         NSURL *url=[NSURL URLWithString:urlPath];
         [SaveVedio indirectShareVedio:url];
    }
    
    
    void _getNum()
    {
        UnitySendMessage("MainManager", "GetNumCallback", "48000");
    }
    
    void _getString()
    {
        UnitySendMessage("MainManager", "GetStringCallback", "");
    }
    
#if defined (__cplusplus)
}
#endif
