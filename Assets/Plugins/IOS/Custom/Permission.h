//
//  Permission.h
//  Unity-iPhone
//
//  Created by yons on 2019/6/18.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>

NS_ASSUME_NONNULL_BEGIN

@interface Permission : NSObject

#if NS_BLOCKS_AVAILABLE
typedef void(^ReturnBlock)(BOOL isOpen);
#endif

@end

NS_ASSUME_NONNULL_END
