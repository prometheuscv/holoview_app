//
//  Permission.m
//  Unity-iPhone
//
//  Created by yons on 2019/6/18.
//

#import "Permission.h"

@implementation Permission


+ (void)openRecordServiceWithBlock:(ReturnBlock)returnBlock
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_8_0
    AVAudioSessionRecordPermission permissionStatus = [[AVAudioSession sharedInstance] recordPermission];
    if (permissionStatus == AVAudioSessionRecordPermissionUndetermined) {
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            if (returnBlock) {
                returnBlock(granted);
            }
        }];
    } else if (permissionStatus == AVAudioSessionRecordPermissionDenied) {
        returnBlock(NO);
    } else {
        returnBlock(YES);
    }
#endif
}

+ (void)openAlbumServiceWithBlock:(ReturnBlock)returnBlock
{
    BOOL isOpen;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_8_0
    PHAuthorizationStatus authStatus = [PHPhotoLibrary authorizationStatus];
    if(authStatus==PHAuthorizationStatusNotDetermined)
    {
       [PHPhotoLibrary requestAuthorization: ^(PHAuthorizationStatus status) {
           if (status == PHAuthorizationStatusAuthorized) {
               returnBlock(YES);
           }else{
               returnBlock(NO);
           }
       }];
        return;
    }
    else
    {
        isOpen = YES;
        if (authStatus == PHAuthorizationStatusRestricted || authStatus == PHAuthorizationStatusDenied||authStatus==PHAuthorizationStatusNotDetermined) {
            isOpen = NO;
        }
    }
#else
    ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
    isOpen = YES;
    if (author == ALAuthorizationStatusRestricted || author == ALAuthorizationStatusDenied) {
        isOpen = NO;
    }
#endif
    if (returnBlock) {
        returnBlock(isOpen);
    }
}

@end


#if defined (__cplusplus)
extern "C" {
#endif
    
    void _IsOpenRecord()
    {
        [Permission openRecordServiceWithBlock:^(BOOL isopen){
            NSNumber *tNum=[NSNumber numberWithBool:isopen];
            NSString *tStr=[NSString stringWithFormat:@"%@",tNum];
            UnitySendMessage("MainManager", "PermissionCallback", tStr.UTF8String);
                }];
    }
    
    void _IsOpenAlbum()
    {
        [Permission openAlbumServiceWithBlock:^(BOOL isopen){
            NSNumber *tNum=[NSNumber numberWithBool:isopen];
            NSString *tStr=[NSString stringWithFormat:@"%@",tNum];
//            free((void *)isOpen);
//            isOpen=NULL;
//            isOpen=(char*) malloc([tStr length] + 1);
//            isOpen=memcpy(isOpen,[tStr UTF8String],([tStr length] + 1));
            UnitySendMessage("MainManager", "PermissionCallback", tStr.UTF8String);
        }];
    }
    
    void _OpenSystemSetting()
    {
        NSURL * url = [NSURL URLWithString: UIApplicationOpenSettingsURLString];
        if ( [[UIApplication sharedApplication] canOpenURL: url] ) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
    
#if defined (__cplusplus)
}
#endif
