//
//  OpenApp.m
//  Unity-iPhone
//
//  Created by yons on 2019/5/9.
//

#import "OpenApp.h"

@implementation OpenApp
-(void)openWechat{
    NSURL * url = [NSURL URLWithString:@"weixin://"];
    BOOL canOpen = [[UIApplication sharedApplication] canOpenURL:url];
    //先判断是否能打开该url
    if (canOpen)
    {   //打开微信
        [[UIApplication sharedApplication] openURL:url];
    }else {
       // [MBProgressHUD showMoreLine:@"您的设备未安装微信APP" view:nil];
    }
}


@end

#if defined (__cplusplus)
extern "C"{
#endif

    void _open(const char *originUrl,bool isCheck){
        //NSURL * url = [NSURL URLWithString:@"weixin://"];
        NSString *nsurl=[NSString stringWithUTF8String:originUrl];
        NSURL * url = [NSURL URLWithString:nsurl];
        BOOL canOpen = [[UIApplication sharedApplication] canOpenURL:url];
        //先判断是否能打开该url
        if (canOpen)
        {   //打开微信
            if(isCheck)
            {
                UnitySendMessage("SceneManager", "CheckCallBack", nsurl.UTF8String);
                NSLog( @"check");
            }
            else
            [[UIApplication sharedApplication] openURL:url];
        }else {
            NSString *temp=[nsurl stringByAppendingString:@"fail" ];
            UnitySendMessage("SceneManager", "CheckCallBack", temp.UTF8String);
            NSLog(@"not install %@",temp);
        }
    }
    
    
#if defined (__cplusplus)
}
#endif
