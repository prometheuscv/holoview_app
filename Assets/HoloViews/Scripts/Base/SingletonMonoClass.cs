﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonMonoClass<T> :  MonoBehaviour where T : SingletonMonoClass<T>
{
    static T instance;

    public static T Instance
    {
        set { instance = value; }
        get
        {
            return instance;
        }
    }
}
