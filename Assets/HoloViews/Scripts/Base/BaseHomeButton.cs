﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BaseHomeButton : MonoBehaviour
{
    protected Text text;
    public bool selected;
    public bool leftButton;
    public BasePageController page;
    // Start is called before the first frame update

    private void Awake()
    {
        text = GetComponentInChildren<Text>(); 
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public virtual void SelectButton()
    {
        if (selected)
        {
            text.color = new Color(0.73f,0.74f,0.75f);
            Debug.Log("hide:"+selected);
            page.HidePage(null);
        }
        else
        {
            text.color = new Color(1f,1f,1f);
            Debug.Log("show:"+selected);
            page.ShowPage(null);
        }
        selected = !selected;
    }
}
