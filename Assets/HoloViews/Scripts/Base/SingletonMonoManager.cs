﻿using System.Collections;
using UnityEngine;

/// <summary>
/// 单例模式的Mono管理器父类，所有单例模式的Mono管理器均继承自它，并且通通挂在ManagerScene场景下的MainManager物体上，该场景该物体
/// 在游戏过程中始终不会销毁
/// </summary>
public abstract class SingletonMonoManager<T> : MonoBehaviour where T : SingletonMonoManager<T>
{
    static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = GameObject.FindWithTag("MainManager");
                if (go)
                {
                    instance = go.GetComponent<T>();
                    if (instance == null)
                    {
                        instance = go.AddComponent<T>();
                    }
                }
            }
            return instance;
        }
    }
    public abstract IEnumerator Init();
}