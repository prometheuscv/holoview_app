﻿using System;
using UnityEngine;
using DG.Tweening;

public class BasePageController : MonoBehaviour
{
    protected CanvasGroup canvasGroup;
    public GameObject itemParent;
    
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    
    public virtual void ShowPage(Action callbcak)
    {
        canvasGroup.blocksRaycasts = true;
        canvasGroup.DOFade(1,0.2f).OnComplete(() =>
        {
            if (callbcak!=null)
            {
                callbcak.Invoke();
            }
        });
    }

    public virtual void HidePage(Action callbcak)
    {
        canvasGroup.blocksRaycasts = false;
        canvasGroup.DOFade(0,0.2f).OnComplete(() =>
        {
            if (callbcak!=null)
            {
                callbcak.Invoke();
            }
        });
    }

    public void CleanItems()
    {
        foreach (Transform items in itemParent.transform)
        {
            Destroy(items.gameObject);
        }
    }
    
}
