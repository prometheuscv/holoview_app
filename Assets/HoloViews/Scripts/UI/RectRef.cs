﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class RectRef : MonoBehaviour,IBeginDragHandler,IDragHandler,IEndDragHandler
{
    //高度 往下拉是负数   往上拉是正数
    float f = 300f;
    //是否刷新
    bool isRef = false;
    //是否处于拖动
    bool isDrag = false;
    //显示、隐藏刷新字段
    public Action<bool> callback1;
    //如果满足刷新条件 执行的方法
    public Action callback2;
    private float StartY;
    public ScrollRect ScrollRect;
    private bool isTop;
    private void Awake()
    {
        ScrollRect.onValueChanged.AddListener(IsTop);
    }

    public void IsTop(Vector2 vec)
    {
        if (vec.y>=1)
        {
            isTop = true;
        }
        else
        {
            isTop = false;
        }
    }

    public  void OnBeginDrag(PointerEventData eventData)
    {
        if (isDrag==false)
        {
            StartY = eventData.position.y;
            isDrag = true;
        }
    }

    public  void OnDrag(PointerEventData eventData)
    {
        if (StartY-eventData.position.y>f&&isTop)
        {
            callback1?.Invoke(true);
            isRef = true;
        }
        else
        {
            callback1?.Invoke(false);
            isRef = false;
        }
    }

    public  void OnEndDrag(PointerEventData eventData)
    {

        callback1?.Invoke(false);
        if (isRef)
            callback2?.Invoke();
        isRef = false;
        isDrag = false;
        isTop = false;
    }

    private void OnDestroy()
    {
        ScrollRect.onValueChanged.RemoveListener(IsTop);
    }
}
