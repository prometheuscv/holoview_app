﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ToastManager : SingletonMonoClass<ToastManager>
{
    public CanvasGroup canvasGroup;
    public GameObject toastBox;
    public Text toastText;
    public GameObject dialogBox;
    public Text dialogTitle;
    public Text dialogDescri;
    public Action confirmCallBack;
    public Action cancelCallBack;
    public Image confirmImage;
    public List<Sprite> confirmSprite;

    public GameObject progressBox;
    public Image progressBoxImage;
    public Text progressBoxImageText;
    public Text progressBoxImageTitle;
    private void Awake()
    {
        Instance = this;
    }

    public void ShowCanvasGroup()
    {
        canvasGroup.DOFade(1,0.2f);
        canvasGroup.blocksRaycasts = true;
    }
    
    public void HideCanvasGroup()
    {
        canvasGroup.alpha = 0;
        canvasGroup.blocksRaycasts = false;
    }

    public void ShowToastBox(string str,float duration = 2)
    {
        toastBox.SetActive(true);
        toastText.text = str;
        ShowCanvasGroup();
        Invoke("HideToastBox",duration);
    }
    
    public void HideToastBox()
    {
        toastBox.SetActive(false);
        toastText.text = "";
        HideCanvasGroup();
    }


    private bool doProgressBoxCallBack;
    public void ShowProgressBox(string str, Action callback, float duration = 2)
    {
        doProgressBoxCallBack = true;
        progressBox.SetActive(true);
        progressBoxImageTitle.text = str;
        ShowCanvasGroup();

        float number = 0;
        Tween t = DOTween.To(() => number, x => number = x, 1, duration);
        t.OnUpdate(() =>
        {
            progressBoxImage.fillAmount = number;
            int numInt = (int) (number * 100);
            progressBoxImageText.text = $"{numInt}%";
        }).OnComplete(() =>
        {
            if (doProgressBoxCallBack)
            {
                if (number == 1)
                {
                    HideProgressBox();
                    if (callback != null)
                    {
                        callback.Invoke();
                    }
                }
            }
        });
    }

    public void HideProgressBox()
    {
        doProgressBoxCallBack = false;
        progressBoxImageText.text = "0%";
        progressBoxImage.fillAmount = 0;
        progressBox.SetActive(false);
        progressBoxImageTitle.text = "";
        HideCanvasGroup();
    }
    
    public void ShowDialogBox(string title,string descri,Action confirmCallBack,Action cancelCallBack,int confirmSpriteIndex = 0)
    {
        dialogTitle.text = title;

        if (string.IsNullOrEmpty(descri))
        {
            dialogTitle.alignment = TextAnchor.LowerLeft;
            dialogDescri.text = "";
        }
        else
        {
            dialogTitle.alignment = TextAnchor.MiddleLeft;
            dialogDescri.text = descri;
        }
        
        this.confirmCallBack = confirmCallBack;
        this.cancelCallBack = cancelCallBack;
        confirmImage.sprite = confirmSprite[confirmSpriteIndex];
        dialogBox.SetActive(true);
        ShowCanvasGroup();
    }
    

    
    public void OnClickDialogConfirm()
    {
        if (confirmCallBack!=null)
        {
            confirmCallBack.Invoke();
        }
        CleanDialogBox();
        HideCanvasGroup();
    }
    
    public void OnClickDialogCancel()
    {
        if (cancelCallBack!=null)
        {
            cancelCallBack.Invoke();
        }
        CleanDialogBox();
        HideCanvasGroup();
    }

    public void CleanDialogBox()
    {
        dialogTitle.text = "";
        dialogDescri.text = "";
        dialogBox.SetActive(false);
        confirmCallBack = null;
        cancelCallBack = null;
    }
}
