﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ScrollRectRef : ScrollRect
{
    //高度 往下拉是负数   往上拉是正数
    float f = 300f;
    //是否刷新
    bool isRef = false;
    //是否处于拖动
    bool isDrag = false;
    //显示、隐藏刷新字段
    public Action<bool> callback1;
    //如果满足刷新条件 执行的方法
    public Action callback2;
    public float StartY;
    protected override void Awake()
    {
        base.Awake();
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);
        
        if (isDrag==false)
        {
            StartY = eventData.position.y;
            isDrag = true;
        }
    }

    public override void OnDrag(PointerEventData eventData)
    {
        base.OnDrag(eventData);
        if (StartY-eventData.position.y>f)
        {
            callback1?.Invoke(true);
            isRef = true;
        }
        else
        {
            callback1?.Invoke(false);
            isRef = false;
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);
        callback1?.Invoke(false);
        if (isRef)
            callback2?.Invoke();
        isRef = false;
        isDrag = false;
    }
}
