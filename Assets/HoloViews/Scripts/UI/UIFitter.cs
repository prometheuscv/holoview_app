﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFitter : MonoBehaviour
{
    private RectTransform trans;

    public float bottom =68;
    public float top=-88;
    // Start is called before the first frame update
    void Start()
    {
        float screenRatio = (float)Math.Round((float) Screen.height / (float) Screen.width,2);
        if (screenRatio>1.78f&& Application.platform != RuntimePlatform.Android)
        {
            trans = GetComponent<RectTransform>();
            if (bottom!=0)
            {
                trans.offsetMin = new Vector2(0,bottom);
            }

            if (top!=0)
            {
                trans.offsetMax = new Vector2(0,top);
            }
        }
        else
        {
        }
    }
}
