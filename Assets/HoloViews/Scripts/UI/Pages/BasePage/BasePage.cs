﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePage : MonoBehaviour
{
   public GameObject page;
   public Transform itemParent;
   public virtual void UpdateUI()
   {
   }

   public virtual void OpenPage()
   {
      page.SetActive(true);
   }

   public virtual void ClosePage()
   {
      page.SetActive(false);
   }

   public void CleanChildren()
   {
      foreach (Transform trans  in itemParent)
      {
         Destroy(trans.gameObject);
      }
   }
}
