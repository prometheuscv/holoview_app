﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseButton : MonoBehaviour
{
    public bool selected;
    public bool needIndicator;
    public bool needHighlight;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void OnClickButton()
    {
        if (needHighlight)
        {
            HighlightButton();
        }
        selected = !selected;
    }

    public void HighlightButton()
    {
        var textComp = GetComponentInChildren<Text>();
        if (textComp)
        {
            if (selected)
            {
                textComp.color = new Color(0.73f,0.74f,0.75f);
            }
            else
            {
                textComp.color = new Color(1f,1f,1f);
            } 
        }
    }
}
