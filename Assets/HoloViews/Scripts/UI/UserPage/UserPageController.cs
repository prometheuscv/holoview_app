﻿    using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

    public class UserPageController : SingletonMonoClass<UserPageController>
{
    public CanvasGroup canvasGroup;
    public GameObject LoginPage;
    public GameObject userPage;
    //短信登陆面板
    public string phoneNum;
    public string verifyNum;
    public Text errorText;
    public InputField phoneNumInput;
    public InputField verifyNumInput;
    public Button getVerifyNumButton;
    public Text getVerifyNumText;
    public GameObject privacyPolicyPage;
    
    //用户面板
    public GameObject userTitle;
    public GameObject userSetting;
    public GameObject userManager;
    public List<VideoItemPrefabController> userVideoItems;
    public List<VideoItemPrefabController> deletedVideoList;
    public GameObject videoItemPrefab;
    public GameObject userVideoParent;
    public GameObject deleteBar;
    public Button deleteButton;

    public Transform userManagerPos1;
    public Transform userManagerPos2;
    
    public Toggle deleteSelectAllToggle;

    public Image userAvatar;
    public Text nickNameText;
    public Text phoneNumText;
    
    //用户设置面板
    public GameObject userSettingPage;
    public GameObject aboutUsPage;
    public bool isUserVideoManaged;
    public RectRef userVideoPageRectRef;
    public GameObject refTip;
    public bool isTest;
    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {        
        phoneNumInput.onEndEdit.AddListener(SetPhoneNumInput);
        verifyNumInput.onEndEdit.AddListener(SetVerifyNumInput);
        EventManager.Instance.AddEventListener(Constant.EVENT_UPDATE_USER_VIDEO,InitUserVideoItems);
        deleteSelectAllToggle.onValueChanged.AddListener(SelectAllToggle);
        UpdateUserUI();
        userVideoPageRectRef.callback1 = (bo) => { refTip.SetActive(bo); };
        userVideoPageRectRef.callback2 = () => { NetworkDataInfoManager.Instance.GetUserVideoDataInfo(InitUserVideoItems2);};
    }

    public void UpdateUserUI()
    {
        UserDataInfo userDataInfo = NetworkDataInfoManager.Instance.userDataInfo;
        
        if (userDataInfo!=null)
        {
            nickNameText.text = userDataInfo.name;
            phoneNumText.text = $"手机号:{userDataInfo.phone}";
        }
    }

    public void CleanUserUI()
    {
        nickNameText.text = "";
        phoneNumText.text = "";
    }

    public void ShowPrivacyPolicy()
    {
        privacyPolicyPage.SetActive(true);
    }
    
    public void HidePrivacyPolicy()
    {
        privacyPolicyPage.SetActive(false);
    }

    public void ShowUserSettingPage()
    {
        userSettingPage.SetActive(true);
        if (isUserVideoManaged)
        {
            UserManageVideo();
        }
    }
    
    public void HideUserSettingPage()
    {
        userSettingPage.SetActive(false);
    }

    public void ShowAboutUsPage()
    {
        aboutUsPage.SetActive(true);
    }

    public void HideAboutUspage()
    {
        aboutUsPage.SetActive(false);
    }

    public void BrowseInternet()
    {
        Application.OpenURL(Constant.WebUrl);
    }

    public void CopyEmailAddress()
    {
        ToastManager.Instance.ShowToastBox("复制成功，粘贴在邮箱与我们联系");
        UnityEngine.GUIUtility.systemCopyBuffer = Constant.Email;
    }

    public void SelectAllToggle(bool bo)
    {
        if (bo)
        {
            for (int i =0;i<userVideoItems.Count;i++)
            {
                userVideoItems[i].toggle.isOn = true;
                //homeLocalVideoItems[i].OnToggle(true);
            }
        }
        else
        {
            for (int i =0;i<userVideoItems.Count;i++)
            {
                userVideoItems[i].toggle.isOn = false;
                //homeLocalVideoItems[i].OnToggle(false);
            }
        }
    }
    
    public void OnClickDeleteUserVideo()
    {
        ToastManager.Instance.ShowDialogBox($"确定删这{deletedVideoList.Count}个资源包吗？", "(仅删除资源包，不删除购买记录)", () =>
        {
            var resourceManager = ResourceManager.Instance;
            var meshVideoManager = MeshVideoManager.Instance;
            
            DeleteVideoDataInfo deleteVideoDataInfo = new DeleteVideoDataInfo();
            deleteVideoDataInfo.ids = new List<string>();
            for (int i = 0; i < deletedVideoList.Count; i++)
            {
                deleteVideoDataInfo.ids.Add(deletedVideoList[i].videoItemDataInfo.id.ToString());
            }
            
            
            NetworkDataInfoManager.Instance.DeleteUserVideo(deleteVideoDataInfo, () =>
            {
                for (int i = 0; i < deletedVideoList.Count; i++)
                {

                    if (NetworkDataInfoManager.Instance.userVideoDataInfo.data.Contains(deletedVideoList[i].videoItemDataInfo))
                    {
                        NetworkDataInfoManager.Instance.userVideoDataInfo.data.Remove(deletedVideoList[i]
                            .videoItemDataInfo);
                    }

                    userVideoItems.Remove(deletedVideoList[i]);
                    string videoName = meshVideoManager.GetNameByPath(deletedVideoList[i].videoItemDataInfo.video);
                    if (resourceManager.IsFileExist("/video", videoName))
                    {
                        resourceManager.DeleteFile("/video", videoName);
                      
                    }
                    Destroy(deletedVideoList[i].gameObject);
                }

                UserManageVideo();
            });
            
            
        }, null,1);
    }

    public void OnEndDragUserScrollRect(Vector2 pointerData)
    {
        if (pointerData.y<0.6f)
        {
            userTitle.SetActive(true);   
            userSetting.SetActive(false);
            userManager.transform.SetParent(userManagerPos1);
            userManager.transform.localPosition = Vector3.zero;
        }
        else
        {
            userTitle.SetActive(false);  
            userSetting.SetActive(true);
            userManager.transform.SetParent(userManagerPos2);
            userManager.transform.localPosition = Vector3.zero;
        }
    }

    public void ShowPage()
    {
        canvasGroup.blocksRaycasts = true;
        canvasGroup.DOFade(1, 0.2f);
    }

    public void HidePage(Action callbcak)
    {
        canvasGroup.blocksRaycasts = false;
        canvasGroup.DOFade(0,0.2f).OnComplete(() =>
        {
            if (callbcak!=null)
            {
                callbcak.Invoke();
            }
        });
    }

    public void UpdateErrorText(string str)
    {
        errorText.text = str;
    }

    //登录面板
    public void SetPhoneNumInput(string num)
    {
        phoneNum = num;
        if (phoneNum.Length==11)
        {
            getVerifyNumText.color = new Color(0.18F,0.45F,0.85F);
        }
        else
        {
            getVerifyNumText.color = new Color(0.8f,0.8f,0.8f);
        }
    }
    
    public void SetVerifyNumInput(string num)
    {
        verifyNum = num;
    }

    public void ShowUserPage()
    {
        userPage.SetActive(true);
        if (userVideoItems.Count==0)
        {
            var info = NetworkDataInfoManager.Instance.userVideoDataInfo;
            if (info!=null&&info.data != null&&info.data.Count>0)
            {
                InitUserVideoItems(null);
            }
            else
            {
                if (isTest == false)
                {
                    NetworkDataInfoManager.Instance.GetUserVideoDataInfo(null);
                }
            }
        }
        ShowPage(); 
    }
    
    public void HideUserPage()
    {
        HidePage(() =>
        {
            userPage.SetActive(false);
        });
        if (isUserVideoManaged)
        {
            UserManageVideo();
        }
        CleanLoginInfo();
    }

    public void ShowLoginPage()
    {
        LoginPage.SetActive(true);
        ShowPage(); 
    }
    
    public void HideLoginPage()
    {
        HidePage(() =>
        {
            LoginPage.SetActive(false);
        });
        CleanLoginInfo();
    }
    
    public void GetVerifyCode()
    {
        if (phoneNum.Length!=11)
        {
            errorText.text = "手机格式不正确";
        }
        else
        {
            errorText.text = "";
            getVerifyNumButton.interactable = false;
            NetworkDataInfoManager.Instance.GetVerifySMS(phoneNum);
        }
    }
    
    public void ConfirmVerifyCode()
    {
        if (verifyNum.Length!=6)
        {
            errorText.text = "验证码格式不正确";
        }
        else
        {
            errorText.text = "";
            NetworkDataInfoManager.Instance.Login(phoneNum, verifyNum, () =>
            {
                isTest = false;
                HideLoginPage();
                ShowUserPage();
                HomeSceneManager.Instance.InitAvatarUI(null);
                UpdateUserUI();
            });
        }
    }

    public void ExitLogin()
    {
        ToastManager.Instance.ShowDialogBox("退出登录？","", () =>
        {
            if (!isTest)
            {
                NetworkDataInfoManager.Instance.UserQuitLogin();
            }
            else
            {
                NetworkDataInfoManager.Instance.isLogin = false;
            }
            HideUserSettingPage();
            HideUserPage();
            HomeSceneManager.Instance.InitAvatarUI(null);
            CleanUserUI();
        },null,1);
    }

    public IEnumerator GetvVerifyNumCountDown(int duration)
    {
        getVerifyNumButton.interactable = false;
        getVerifyNumText.color = new Color(0,0,0);
        while (duration>=0&&LoginPage.activeSelf==true)
        {
            getVerifyNumText.text = $"{duration}s";
            duration -= 1;    
            yield return new WaitForSeconds(1);
        }
        getVerifyNumButton.interactable = true;
        getVerifyNumText.color = new Color(0.8f,0.8f,0.8f);
        getVerifyNumText.text = "获取验证码";
    }

    public void CleanLoginInfo()
    {
        errorText.text = "";
        phoneNumInput.text = "";
        verifyNumInput.text = "";
        phoneNum = "";
        verifyNum = "";
    }
    
    public void InitUserVideoItems(BaseEvent baseEvent)
    {
        UserVideoDataInfo userVideoDataInfo = NetworkDataInfoManager.Instance.userVideoDataInfo;
        userVideoItems.Clear();
        CleanChildren();
        if (userVideoDataInfo!=null&&userVideoDataInfo.data != null&&userVideoDataInfo.data.Count>0)
        {
            
            for (int i=0;i<userVideoDataInfo.data.Count;i++)
            {
                var obj = Instantiate(videoItemPrefab, userVideoParent.transform);
                var comp = obj.GetComponent<VideoItemPrefabController>();
                comp.InitItem(userVideoDataInfo.data[i]);
                comp.isUserItem = true;
                userVideoItems.Add(comp);
            }
        }
        else
        {

        }
    }
    
    public void InitUserVideoItems2()
    {
        UserVideoDataInfo userVideoDataInfo = NetworkDataInfoManager.Instance.userVideoDataInfo;
        userVideoItems.Clear();
        CleanChildren();
        if (userVideoDataInfo!=null&&userVideoDataInfo.data != null&&userVideoDataInfo.data.Count>0)
        {
            
            for (int i=0;i<userVideoDataInfo.data.Count;i++)
            {
                var obj = Instantiate(videoItemPrefab, userVideoParent.transform);
                var comp = obj.GetComponent<VideoItemPrefabController>();
                comp.InitItem(userVideoDataInfo.data[i]);
                comp.isUserItem = true;
                userVideoItems.Add(comp);
            }
        }
        else
        {

        }
    }
    
    public void UserManageVideo()
    {
        if (isUserVideoManaged)
        {
            for (int i = 0; i < userVideoItems.Count; i++)
            {
                userVideoItems[i].OpenSeletcedToggle(false);
            }
            
            userManager.GetComponentInChildren<Text>().text = "管理";
            deleteBar.SetActive(false);
            deletedVideoList.Clear();
        }
        else
        {
            for (int i = 0; i < userVideoItems.Count; i++)
            {
                userVideoItems[i].OpenSeletcedToggle(true);
            }
            
            userManager.GetComponentInChildren<Text>().text = "取消";
            deleteBar.SetActive(true);
        }

        isUserVideoManaged = !isUserVideoManaged;
    }
    
    public void CleanChildren()
    {
        foreach (Transform trans  in userVideoParent.transform)
        {
            Destroy(trans.gameObject);
        }
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveEventListener(Constant.EVENT_UPDATE_USER_VIDEO,InitUserVideoItems);
        phoneNumInput.onEndEdit.RemoveListener(SetPhoneNumInput);
        verifyNumInput.onEndEdit.RemoveListener(SetVerifyNumInput);
        deleteSelectAllToggle.onValueChanged.RemoveListener(SelectAllToggle);
    }
}
