﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoItemPrefabController : MonoBehaviour
{
    public bool isUserItem;
    public ModelItemDataInfo videoItemDataInfo;
    public Image image;
    public Text title;
    public Text describe;
    public bool exist;
    public GameObject existTip;
    public Texture2D tex;
    public Toggle toggle;
    // Start is called before the first frame update
    void Start()
    {
        if (isUserItem)
        {
            toggle.onValueChanged.AddListener(OnToggleUser); 
        }
        else
        {
            toggle.onValueChanged.AddListener(OnToggle); 
        }
    }

    public void OnToggleUser(bool bo)
    {
        var info = UserPageController.Instance.deletedVideoList;
        if (bo)
        {
            if (!info.Contains(this))
            {
                info.Add(this);
                if (info.Count>0)
                {
                    UserPageController.Instance.deleteButton.interactable = true;
                }
                else
                {
                    UserPageController.Instance.deleteButton.interactable = false;
                }
            }
        }
        else
        {
            if (info.Contains(this))
            {
                info.Remove(this);
                if (info.Count>0)
                {
                    HomeSceneManager.Instance.deleteButton.interactable = true;
                }
                else
                {
                    HomeSceneManager.Instance.deleteButton.interactable = false;
                }
            }
        }
    }

    public void OnToggle(bool bo)
    {
        var info = HomeSceneManager.Instance.deletedVideoList;
        if (bo)
        {
            if (!info.Contains(this))
            {
                info.Add(this);
                if (info.Count>0)
                {
                    HomeSceneManager.Instance.deleteButton.interactable = true;
                }
                else
                {
                    HomeSceneManager.Instance.deleteButton.interactable = false;
                }
            }
        }
        else
        {
            if (info.Contains(this))
            {
                info.Remove(this);
                if (info.Count>0)
                {
                    HomeSceneManager.Instance.deleteButton.interactable = true;
                }
                else
                {
                    HomeSceneManager.Instance.deleteButton.interactable = false;
                }
            }
        }
    }

    public void InitItem(ModelItemDataInfo videoItemDataInfo)
    {
       
        this.videoItemDataInfo = videoItemDataInfo;
        CheckExsit();
        title.text = videoItemDataInfo.title;
        describe.text = videoItemDataInfo.info;

        if (videoItemDataInfo.imageData!=null)
        {
            tex = new Texture2D(1,1);
            tex.name = "VideoItemPrefab:"+videoItemDataInfo.id+" exist:"+exist;
            tex.LoadImage(videoItemDataInfo.imageData);
            image.sprite = Sprite.Create(tex,new Rect(0,0,tex.width,tex.height),Vector2.zero);
        }
        else
        {
            if (!string.IsNullOrEmpty(videoItemDataInfo.image))
            {
                NetworkManager.Instance.GetOssFileByUrlAsync(videoItemDataInfo.image, (data, para) =>
                {
                    byte[] resourceData = (byte[])data;
                    if (resourceData!=null)
                    {
                        tex = new Texture2D(1,1);
                        tex.name = "VideoItemPrefab:" + videoItemDataInfo.id+" exist:"+exist;;
                        tex.LoadImage(resourceData);
                        image.sprite = Sprite.Create(tex,new Rect(0,0,tex.width,tex.height),Vector2.zero);
                    }
                }, (progress) =>
                {
                    //Debug.Log("progress"+progress);
                });
            }  
        }
    }

    public void CheckExsit()
    {
        if (MeshVideoManager.Instance.CheckVideoNameExist(videoItemDataInfo.video))
        {
            exist = true;
            existTip.SetActive(true);
        }
        else
        {
            exist = false;
            existTip.SetActive(false);
        }
    }

    public void OpenSeletcedToggle(bool isOn)
    {
        if (isOn)
        {
            toggle.gameObject.SetActive(true);
            existTip.SetActive(false);
        }
        else
        {
            toggle.gameObject.SetActive(false);
            if (exist)
            {
                existTip.SetActive(true);
            }

            toggle.isOn = false;
        }
    }

    public void OnClickButton()
    {
        MeshVideoManager.Instance.CheckMeshVideoExist(videoItemDataInfo.video,videoItemDataInfo.size);
    }

    public void OnDestroy()
    {
        if (isUserItem)
        {
            toggle.onValueChanged.RemoveListener(OnToggleUser); 
        }
        else
        {
            toggle.onValueChanged.RemoveListener(OnToggle); 
        }

        if (tex!=null)
        {
            //Debug.Log("Destroy:"+tex.name);
            Destroy(tex);
        }

        if (image.sprite!=null)
        {
            Destroy(image.sprite);
        }
    }
}
