﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageChangeController : SingletonMonoClass<StageChangeController>
{
    public GameObject page;
    public GameObject stageItemPrefab;
    public GameObject stageItemScrollbar;
    public GameObject stageItemParent;
    public Scrollbar scrollbar;
    public StageChangeItemController curentStageChangeItemController;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void show()
    {
        page.SetActive(true);
    }

    public void hide()
    {
        page.SetActive(false);
    }

    private void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void InitItem(List<VRSceneCfgInfo> vrSceneCfgInfos)
    {
        if (vrSceneCfgInfos.Count > 3)
        {
            scrollbar.gameObject.SetActive(true);
        }
        else
        {
            scrollbar.gameObject.SetActive(false);
        }

        for (int i = 0; i < vrSceneCfgInfos.Count; i++)
        {
            var obj = Instantiate(stageItemPrefab, stageItemParent.transform);
            obj.GetComponent<StageChangeItemController>().InitItem(vrSceneCfgInfos[i].stageId);
        }
    }
}
