﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageChangeItemController : MonoBehaviour
{
    public int stageID;
    public Button button;
    public Image Image;
    public GameObject HighLight;
    private Action callback;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitItem(int id)
    {
        stageID = id;
        var tex = VRStageManager.Instance.LoadVRSceneIconByID(stageID);
        tex.name = "stageID:"+stageID;
        Image.sprite = Sprite.Create(tex,new Rect(0,0,tex.width,tex.height),Vector2.zero);
    }

    public void OnClick()
    {
        var stageChangeController = StageChangeController.Instance;
        if (stageChangeController.curentStageChangeItemController!=null)
        {
            stageChangeController.curentStageChangeItemController.HighLight.SetActive(false);
        }
        
        VRStageManager.Instance.LoadVRSceneByID(stageID);
        HighLight.SetActive(true);
        StageChangeController.Instance.curentStageChangeItemController = this;
    }
}
