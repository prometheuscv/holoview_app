﻿using System;
using System.IO;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class SharePageController : SingletonMonoClass<SharePageController>
{
#if UNITY_IOS && !UNITY_EDITOR
    [DllImport("__Internal")]
    static extern void _iosSaveImageToPhotosAlbum(string readAddr);
    [DllImport("__Internal")]
    static extern void _save(string filePath);
    [DllImport("__Internal")]
    static extern void _open(string originUrl, bool isCheck);
#endif
    public Slider Slider;
    public RawImage shareImage;
    public GameObject backButton;
    public GameObject videoPlayButton;
    public GameObject saveButton;
    public GameObject ShareButton;
    public CanvasGroup sharePageCanvasGroup;
    public CanvasGroup sharePlatformCanvasGroup;
    public bool isVideo;
    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    public void ShowSharePage()
    {
        if (isVideo)
        {
            Slider.gameObject.SetActive(true);
            videoPlayButton.SetActive(true);
        }
        else
        {
            Slider.gameObject.SetActive(false);
            videoPlayButton.SetActive(false);
        }
        sharePageCanvasGroup.alpha = 1;
        CameraUIController.Instance.EnableMainUI(true);
        //sharePageCanvasGroup.DOFade(1, 0.1f);
        sharePageCanvasGroup.blocksRaycasts = true;
        EventManager.Instance.DispathchEvent(Constant.EVENT_PAUSE_MESHVIDEO,true);
    }
    
    public void ShowSharePagePhoto(Texture2D tex)
    {
        MediaShareManager.Instance.isVideo = false;
        shareImage.texture = tex;
        isVideo = false;
        ShowSharePage();
    }
    
    public void ShowSharePageVideo(Texture tex)
    {
        MediaShareManager.Instance.isVideo = true;
        shareImage.texture = tex;
        isVideo = true;
        ShowSharePage();
        PLayVideo();
      
    }
    
    public void HideSharePage()
    {
        sharePageCanvasGroup.alpha = 0;    
        sharePageCanvasGroup.blocksRaycasts = false;
        EventManager.Instance.DispathchEvent(Constant.EVENT_PAUSE_MESHVIDEO,false);
        MediaShareManager.Instance.PauseVideo();
        AudioManager.Instance.SetMute(false);
    }

    public void PLayVideo()
    {
        MediaShareManager.Instance.PlayVideo();
        videoPlayButton.SetActive(false);
    }
    
    public void ShowSharePlatform()
    {
        backButton.SetActive(false);
        videoPlayButton.SetActive(false);
        saveButton.SetActive(false);
        ShareButton.SetActive(false);
        sharePlatformCanvasGroup.DOFade(1, 0.2f);
        sharePlatformCanvasGroup.blocksRaycasts = true;
    }

    public void HIdeSharePlatform()
    {
        if (isVideo)
        {
            videoPlayButton.SetActive(true);
        }
        else
        {
            videoPlayButton.SetActive(false);
        }
        backButton.SetActive(true);
        saveButton.SetActive(true);
        ShareButton.SetActive(true);
        sharePlatformCanvasGroup.DOFade(0, 0.2f);
        sharePlatformCanvasGroup.blocksRaycasts = false;
    }
    
    public void OnClickWX()
    {
#if UNITY_IOS && !UNITY_EDITOR
    _open(Constant.EShareTips_weixin, true);
#endif
        
#if UNITY_ANDROID && !UNITY_EDITOR
        MediaShareManager.Instance.AndroidShare(Constant.EShareTips_weixin);
#endif
        
    }

    public void OnClickPYQ()
    {
        MediaShareManager.Instance.isMomentShare = true;
#if UNITY_IOS && !UNITY_EDITOR
    _open(Constant.EShareTips_weixin, true);
#endif
        
#if UNITY_ANDROID && !UNITY_EDITOR
        MediaShareManager.Instance.AndroidShare(Constant.EShareTips_weixin);
#endif
    }
    
    public void SetVideoProgress(double progress)
    {
        Slider.value = (float)progress;
    }
    
    public void OnClickSaveButton()
    {
        if (Application.platform == RuntimePlatform.OSXEditor)
        {
            if (MediaShareManager.Instance.isVideo)
                SaveVideo();
            else
                SavePhoto();
        }
        else if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            //PermissionManager._IsOpenAlbum();
            EventManager.Instance.AddEventListener(Constant.EVENT_GET_PERMISSION_RESULT, OnGetPermissionResult);   
        }
        else if(Application.platform == RuntimePlatform.Android)
        {
            if (MediaShareManager.Instance.isVideo)
                SaveVideo();
            else
                SavePhoto(); 
        }
    }

    void OnGetPermissionResult(BaseEvent evt)
    {
        EventManager.Instance.RemoveEventListener(Constant.EVENT_GET_PERMISSION_RESULT, OnGetPermissionResult);
        Debug.Log((string)evt.Data);
        if ((string)evt.Data == "0")
        {
            ToastManager.Instance.ShowDialogBox("请到系统“设置-咚啪”中授权相应功能","", () =>
            {
                //PermissionManager._OpenSystemSetting();
                
            },null);
            return;
        }
        if (MediaShareManager.Instance.isVideo)
            SaveVideo();
        else
            SavePhoto();
    }

    void SavePhoto()
    {
        ToastManager.Instance.ShowProgressBox("正在保存成功", () =>
        {
            ToastManager.Instance.ShowToastBox("保存成功，请到手机相册查看",1);
        },1);
        
#if UNITY_EDITOR
        ResourceManager.Instance.WriteFile(null, "cachedPhoto.png", ((Texture2D)SharePageController.Instance.shareImage.texture).EncodeToPNG());
#endif
        
#if UNITY_IOS && !UNITY_EDITOR
        ResourceManager.Instance.WriteFile(null, "cachedPhoto.png", ((Texture2D)SharePageController.Instance.shareImage.texture).EncodeToPNG());
        _iosSaveImageToPhotosAlbum(Application.persistentDataPath + "/cachedPhoto.png");
#endif
        
#if UNITY_ANDROID && !UNITY_EDITOR
        var destination = "/mnt/sdcard/DCIM/HoloView/";
        if (!Directory.Exists(destination))
        {
            Directory.CreateDirectory(destination);
        }
        destination = destination + ArUtilityManager.Instance.GetNameInTime()+".png";
        //保存文件
        File.WriteAllBytes(destination, ((Texture2D)SharePageController.Instance.shareImage.texture).EncodeToPNG());
        savePngAndUpdate(destination);
#endif
        
    }
    
    
#if UNITY_ANDROID && !UNITY_EDITOR
    //调用iOS或Android原生方法保存图片后更新相册.
    private void savePngAndUpdate(string fileName)
    {
        GetAndroidJavaObject().Call("scanFile", fileName, "保存成功辣٩(๑>◡<๑)۶ "); //这里我们可以设置保存成功弹窗内容
    }

    //用于获取Android原生方法类对象
    private AndroidJavaObject GetAndroidJavaObject()
    {
        return new AndroidJavaObject("xyz.prometh.customplugins.SaveImageActivity"); //设置成我们aar库中的签名+类名
    }
#endif


    void SaveVideo()
    {
        ToastManager.Instance.ShowProgressBox("正在保存成功", () =>
        {
            ToastManager.Instance.ShowToastBox("保存成功，请到手机相册查看",1);
        },1);
        
        
#if UNITY_ANDROID && !UNITY_EDITOR
        StartCoroutine(ResourceManager.Instance.ReadFileRealPath(Constant.EResourceType_BinaryData,ReplayCamManager.replayResultUrl,
            (request) =>
            {
                var datas= request.downloadHandler.data;
                
                var destination = "/mnt/sdcard/DCIM/HoloView/";
                if (!Directory.Exists(destination))
                {
                    Directory.CreateDirectory(destination);
                }
                destination = destination + ArUtilityManager.Instance.GetNameInTime()+".mp4";
                //保存文件
                File.WriteAllBytes(destination, datas);
                savePngAndUpdate(destination);
            }));
#endif
        
#if UNITY_IOS && !UNITY_EDITOR
        _save(RecordManager.replayResultUrl);
#endif
    }

    private void OnDestroy()
    {
        Destroy(shareImage);
    }
}
