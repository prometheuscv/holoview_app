﻿using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class CameraUIController : SingletonMonoClass<CameraUIController>
{

    public CanvasGroup mainCanvasGroup;
    //0拍照 1录像
    public int currentType = 1;

    public GameObject cameraUISwitch;
    public GameObject recordButton;
    public GameObject stopRecordButton;
    public GameObject photoButton;

    public GameObject cameraPhotoCricleIn;
    public Text countText;
    public Image countImage;
    public float countDuration =  15;
    public float countTimer;
    private bool isRecording;
    private bool isPhotoing;

    private void Awake()
    {
        Instance = this;
    }

    public void OnClickRecordVideo()
    {
        RecordManager.Instance.ReadyRecording();
        cameraUISwitch.SetActive(false);
        stopRecordButton.SetActive(true);
        recordButton.SetActive(false);
        isRecording = true;
        StartCoroutine(RecordingCountDown());
    }

    public IEnumerator RecordingCountDown()
    {
       
        countTimer = 0;
        while (isRecording&&countTimer<=15)
        {
            countImage.fillAmount = countTimer / 15f;
            countTimer += 0.1f;
            countText.text = countTimer.ToString("F1")+"S";
            yield return new WaitForSeconds(0.1f);
        }
        Debug.Log("IEnumerator RecordingCountDown");
        OnClickStopRecordVideo_();
        yield return null;
    }

    public void OnClickStopRecordVideo()
    {
       
        StopAllCoroutines();
        OnClickStopRecordVideo_();
    }

    public void OnClickStopRecordVideo_()
    {
        Debug.Log("OnClickStopRecordVideo");
        EnableMainUI(false); 
        isRecording = false;
        cameraUISwitch.SetActive(true);
        recordButton.SetActive(true);
        stopRecordButton.SetActive(false);
        countTimer = 0;
        countText.text = "0.0s";
        countImage.fillAmount = 0;
        RecordManager.Instance.StopRecording();
    }
    
    public void OnClickTakePhoto()
    {
        if (!isPhotoing)
        {
            EnableMainUI(false);
            StartCoroutine(MediaShareManager.Instance.TakePhoto(FinishTakePhoto));
        }
    }

    public void EnableMainUI(bool bo)
    {
        if (bo)
        {
            mainCanvasGroup.alpha = 1;
        }
        else
        {
            mainCanvasGroup.alpha = 0;
        }
    }

    public void FinishTakePhoto()
    {
        EnableMainUI(true);
        isPhotoing = true;
        cameraPhotoCricleIn.transform.DOPunchScale(new Vector3(0.5f,0.5f,0.5f), 0.3f).OnComplete(() =>
        {
            SharePageController.Instance.ShowSharePagePhoto(MediaShareManager.Instance.sharePhoto);
            isPhotoing = false;
        });
    }
    
   
}
