﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class CameraUISwitch : MonoBehaviour, IEndDragHandler
{
    public CameraUIController cameraUIController;
    public GameObject content;
    public ScrollRect scorllRect;
    private RectTransform contentRectTf;
    public Text typeRecordText;
    public Text typePhotoText;
    
    void Start()
    {
        this.contentRectTf = this.content.GetComponent<RectTransform>();
    }

    public void OnSelectType(int type)
    {
        Debug.Log("OnSelectType!" + type);
        this.scorllRect.StopMovement();
        cameraUIController.currentType = type;

        if (cameraUIController.currentType == 0)
        {
            typePhotoText.fontSize = 32;
            typeRecordText.fontSize = 28;
            this.contentRectTf.DOLocalMoveX(-50, .2f);
            cameraUIController.recordButton.SetActive(false);
            cameraUIController.photoButton.SetActive(true);
        }
        else if (cameraUIController.currentType == 1)
        {
            typeRecordText.fontSize = 32;
            typePhotoText.fontSize = 28;
            this.contentRectTf.DOLocalMoveX(50, .2f);
            cameraUIController.recordButton.SetActive(true);
            cameraUIController.photoButton.SetActive(false);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.scorllRect.StopMovement();
        Vector2 afterDragPagePos = this.content.transform.localPosition;
        var currentPosX = afterDragPagePos.x; //当前拖动的位置  负
        Debug.Log("currentPosX:"+currentPosX);

        if (currentPosX<=0)
        {
            this.contentRectTf.DOLocalMoveX(-50, .2f);
            cameraUIController.currentType = 0;
            cameraUIController.recordButton.SetActive(false);
            cameraUIController.photoButton.SetActive(true);
        }
        else
        {
            this.contentRectTf.DOLocalMoveX(50, .2f);
            cameraUIController.currentType = 1;
            cameraUIController.recordButton.SetActive(true);
            cameraUIController.photoButton.SetActive(false);
        }
    }
}
