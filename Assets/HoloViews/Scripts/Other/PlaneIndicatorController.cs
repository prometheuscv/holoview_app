﻿using System;
using UnityEngine;

public class PlaneIndicatorController : MonoBehaviour
{
    private MeshRenderer meshRenderer;
    private ARSceneManager arSceneManager;
    public bool isAniShow;

    private void Awake()
    {
        meshRenderer = GetComponentInChildren<MeshRenderer>();
    }
    void Start()
    {
        arSceneManager = ARSceneManager.Instance;
        CheckStatus();

    }

    void Update()
    {
        CheckStatus();
    }

    void CheckStatus()
    {
        if (meshRenderer.enabled == true && meshRenderer.gameObject.activeSelf)
        {
            arSceneManager.SetPlaneFinderAni(false);
            arSceneManager.SetPlaceModelTip(false);
            isAniShow = true;
        }
        else
        {
            if (isAniShow == true)
                arSceneManager.SetPlaceModelTip(true);
            else
            {
                arSceneManager.SetPlaneFinderAni(true);
                arSceneManager.SetPlaceModelTip(false);
            }
        }
    }
}
