﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPageInterface
{
    void updateUI();
    void OpenPage();
    void closePage();
}
