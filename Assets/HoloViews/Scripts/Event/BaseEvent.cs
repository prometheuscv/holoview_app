﻿
/// <summary>
/// 事件基类
/// </summary>
public class BaseEvent
{
    public BaseEvent(string type, object data = null, object sender = null)
    {
        Type = type;
        Data = data;
        Sender = sender;
    }

    /// <summary>
    /// 事件类型
    /// </summary>
    public string Type { get; private set; }

    /// <summary>
    /// 事件参数（可选，有可能不指定）
    /// </summary>
    public object Data { get; private set; }

    /// <summary>
    /// 事件派发者（可选，有可能不指定）
    /// </summary>
    public object Sender { get; private set; }
}
