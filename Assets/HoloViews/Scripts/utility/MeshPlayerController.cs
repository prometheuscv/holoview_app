﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using prometheus;

public class MeshPlayerController : MonoBehaviour
{
    public List<MeshFilter> meshFilters;
    public int modelID;
    public int mSourceIdx = 0;
    public string sourceUrl;
    public MeshPlayerPlugin meshPlayerPlugin;
    /// <summary>
    /// The materials. 目前0代表特效材质
    /// </summary>
    public List<Material> materials;

    /// <summary>
    /// 暂时不启用特效材质  纹理异常 参考Test场景
    /// </summary>
    bool isArMode;

    AudioSource audioSource;
    BoxCollider boxCollider;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        sourceUrl = MeshVideoManager.Instance.currentVideoSourcePath;
        for (int i = 0; i < materials.Count; i++)
            SetMaterial(i);
        OpenSource(sourceUrl);
    }

    public void OpenSource(string Url)
    {
        if (meshPlayerPlugin.mIsPlaying)
        {
            meshPlayerPlugin.Pause();
        }
        else
        {
        }
        meshPlayerPlugin.OpenSource(Url);

    }


    public void SetMaterial(int mat)
    {
        if (mat == 0)
        {
            meshPlayerPlugin.SetMaterial(materials[0], true);
            InitDissolve();
        }
        else if (mat == 1)
        {
            meshPlayerPlugin.SetMaterial(materials[1]);
        }
    }

    public void PlayOrPause()
    {
        if (meshPlayerPlugin.IsPlaying)
        {
            PauseVideo();
        }
        else
        {
            PlayVideo();
        }
    }

    public void PlayVideo()
    {
        meshPlayerPlugin.Play();
    }

    public void PauseVideo()
    {
        meshPlayerPlugin.Pause();
    }

    async void WriteFile(string filename, byte[] data)
    {
        using (FileStream SourceStream = File.Open(filename, FileMode.OpenOrCreate))
        {
            SourceStream.Seek(0, SeekOrigin.End);
            await SourceStream.WriteAsync(data, 0, data.Length);
            Debug.Log("write file");
            //继续操作
            OpenSource(sourceUrl);
        }
    }

    void ReplayAudio(BaseEvent evt)
    {
        StartCoroutine(DelayReplayAudio());
    }

    IEnumerator DelayReplayAudio()
    {
        yield return new WaitForSeconds(4.5f);
        audioSource.time = 0;
        audioSource.Play();
    }

    public void UpdateCollider()
    {
        boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.size = new Vector3(boxCollider.size.x * 0.8f, boxCollider.size.y, boxCollider.size.z * 0.8f);
        //CheckFeet();
    }

    public void CheckFeet()
    {
        gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y - gameObject.GetComponent<MeshFilter>().mesh.bounds.min.y, gameObject.transform.localPosition.z);
    }


    public void InitDissolve()
    {
        UpdateCollider();
        if (isArMode)
        {
            ModelEffectManager.Instance.InitDissolve(materials[0], 0.4f, boxCollider.bounds.size.y, 1f,
                gameObject.transform.GetComponentInChildren<ParticleSystem>(),
                transform.parent.localScale.x);
        }
        else
        {
            ModelEffectManager.Instance.InitDissolve(materials[0], 0f, boxCollider.bounds.size.y, 1f,
                gameObject.transform.GetComponentInChildren<ParticleSystem>(),
                transform.parent.localScale.x);
        }
    }
}
