﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ModelViewController : MonoBehaviour,IDragHandler
{
    public GameObject model;
    public Transform cameraParent_X;
    public Transform cameraParent_Y;
    private bool scaleChange;
    private bool positonChange;
    private bool isFirstFrameWithTwoTouches;

    private float mouseScrollWheelScale = 0.02f;
    private float rotateSpeed = 0.2f;

    public float scaleMax;
    public float scaleMin;
    
    private float cachedTouchDistance;
    private float cachedAugmentationScale;
  
    private float deltaX;
    private float deltaY;

    void Update()
    {

        var touches = Input.touches;
        if (touches.Length <2)
        {
            scaleChange = false;
            positonChange = false;
            isFirstFrameWithTwoTouches = true;
            cachedAugmentationScale = model.transform.localScale.x;
            return;
        }

        if (touches.Length == 2)
        {
            var t0 = touches[0];
            var t1 = touches[1];

            if (t0.deltaPosition.x/t1.deltaPosition.x>0 ||t0.deltaPosition.y/t1.deltaPosition.y>0)
            {
                positonChange = true;
                var midX = (t0.deltaPosition.x + t1.deltaPosition.x) / 20;
                var midY = (t0.deltaPosition.y + t1.deltaPosition.y) / 20;
                cameraParent_X.Translate(-midX*0.01f,-midY*0.01f,0);
                
            }

//            if (t0.deltaPosition.y > 0 && t1.deltaPosition.y > 0)
//            {
//                cameraParent_X.Translate(0,-0.1f,0);
//            }
//            else if (t0.deltaPosition.y < 0 && t1.deltaPosition.y < 0)
//            {
//                cameraParent_X.Translate(0,0.1f,0);
//            }
//            else if (t0.deltaPosition.x < 0 && t1.deltaPosition.x < 0)
//            {
//                cameraParent_X.Translate(0.05f,0,0);
//            }
//            else if (t0.deltaPosition.x > 0 && t1.deltaPosition.x > 0)
//            {
//                cameraParent_X.Translate(-0.05f,0,0);
//            }
            else 
            {
                scaleChange = true;
                float currentTouchDistance = Vector2.Distance(t0.position, t1.position);
                if (isFirstFrameWithTwoTouches)
                {
                    cachedAugmentationScale = model.transform.localScale.x;
                    cachedTouchDistance = currentTouchDistance;
                    isFirstFrameWithTwoTouches = false;
                }

                float scaleMultiplier = currentTouchDistance / cachedTouchDistance;
                //scaleMultiplier = Mathf.Clamp(scaleMultiplier, scaleMin, scaleMax);
                float scaleAmount = cachedAugmentationScale * scaleMultiplier;
                //model.transform.localScale = new Vector3(scaleAmount, scaleAmount, scaleAmount);
                model.transform.localScale = Vector3.Lerp( model.transform.localScale,new Vector3(scaleAmount, scaleAmount, scaleAmount),0.5f);
            }


        }
        else
        {
         
        }
        
        

#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR

#else
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (model.transform.localScale.x < scaleMax)
            {
                model.transform.localScale += Vector3.one * mouseScrollWheelScale;
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (model.transform.localScale.x > scaleMin)
            {
                model.transform.localScale -= Vector3.one * mouseScrollWheelScale;
            }
        }
#endif
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!scaleChange&&!positonChange)
        {
            deltaX += eventData.delta.x;
            deltaY -= eventData.delta.y;
            cameraParent_X.localEulerAngles =new Vector3(0,deltaX*rotateSpeed,0);
            cameraParent_Y.localEulerAngles =new Vector3(Mathf.Clamp(deltaY*rotateSpeed,-60,60),0,0);
        }
    }
}
