﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class utility
{
    public static FileSizeData GetFileSizeFloat(long Length)
    {
        FileSizeData size = new FileSizeData();

        if (Length < 1024)
        {
            size.size = Length;
            int j=(int)(size.size * 100);
            size.size = (float)j/100;
            size.unit = "Byte";
        }
        else if(Length >= 1024 && Length < 1048576)
        {
            size.size = ((float)Length / 1024f);
            int j=(int)(size.size * 100);
            size.size = (float)j/100;
            size.unit = "KB";
        }
        else if(Length >= 1048576 && Length < 1073741824)
        {
            size.size = ((float)Length / 1024f/1024f);
            int j=(int)(size.size * 100);
            size.size = (float)j/100;
            size.unit = "MB";
        }
        return size;
    }

}

[System.Serializable]
public class FileSizeData
{
    public float size;
    public string unit;
}
