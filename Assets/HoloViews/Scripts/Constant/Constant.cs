﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant
{
    //EResourceType
    public static string EResourceType_Text = "Text";
    public static string EResourceType_AssetBundle = "AssetBundle";
    public static string EResourceType_Texture = "Texture";
    public static string EResourceType_BinaryData = "BinaryData";

    //ENetworkEvent
    public static string EVENT_UPDATE_OSS_UPLOAD_PROGRESS = "1000";
    public static string EVENT_UPDATE_OSS_UPLOAD_SIZE = "1001";
    public static string EVENT_UPDATE_OSS_DOWNLOAD_SPEED = "1002";
    public static string EVENT_UPDATE_OSS_DOWNLOAD_STATUS = "1003";
    public static string EVENT_UPDATE_OSS_DOWNLOAD_PROGRESS = "1004";
    public static string EVENT_UPDATE_OSS_DOWNLOAD_COMPLETE = "1006";
    public static string EVENT_OSS_DOWNLOAD_ERROR = "1005";

    //EEvent
    public static string EVENT_UPDATE_USER_VIDEO = "EVENT_UPDATE_USER_VIDEO";
    public static string EVENT_UPDATE_INDEX_AVATAR = "EVENT_UPDATE_INDEX_AVATAR";
    public static string EVENT_PAUSE_MESHVIDEO = "EVENT_PAUSE_MESHVIDEO";
    public static string EVENT_GET_PERMISSION_RESULT = "EVENT_GET_PERMISSION_RESULT";

    //EScene
    public static string EScene_HomeScene = "HomeScene";
    public static string EScene_VRScene = "VRScene";
    public static string EScene_GroundARScene = "GroundARScene";
    public static string EScene_MakerARScene = "MakerARScene";
    public static string EScene_LoadingScene = "LoadingScene";
    public static string EScene_IndexLoadingScene = "IndexLoadingScene";
    public static string EScene_PreviewScene = "PreviewScene";

    //ShareTips
    public static string EShareTips_noWeixinHips = "您没有安装微信客户端";
    public static string EShareTips_noWeiboHips = "您没有安装微博客户端";
    public static string EShareTips_noQQHips = "您没有安装QQ客户端";
    public static string EShareTips_saveSuccess = "保存成功,请到手机相册查看";
    public static string EShareTips_weixin = "weixin://";
    public static string EShareTips_weibo = "sinaweibo://";
    public static string EShareTips_qq = "mqq://";
    public static string EShareTips_slogan = "咚啪，一起探索神奇虚拟世界！";
    public static string EShareTips_title = "还能这么玩？快来看看我在咚啪拍的视频吧！";

    //Other
    public static string Email = "toby@prometh.xyz";
    public static string WebUrl = "http://www.prometh.xyz/";
    
    //EPurchaseEvent
    public static string EPurchaseEvent_EVENT_PURCHASE_SUCCESS = "EVENT_PURCHASE_SUCCESS";
    public static string EPurchaseEvent_EVENT_FAKE_RECEIPT = "EVENT_FAKE_RECEIPT";
    public static string EPurchaseEvent_EVENT_PURCHASE_FAIL = "EVENT_PURCHASE_FAIL";
    public static string EPurchaseEvent_EVENT_RESTORE_RESULT = "EVENT_RESTORE_RESULT";

    //服务器相关配置
    public static string devURL = "https://0nsnvhvi98.execute-api.cn-northwest-1.amazonaws.com.cn/v1";
    public static string onlineURL = "https://xcx.prometh.xyz";
    public static string getHomeVideoInfo = "/sample_models"; //获取首页视频信息
    public static string userSign = "/users/sign_in";//用户登录注册
    public static string userSignOut = "/users/sign_out";//登出
    public static string getUserInfo = "/users";//获取用户信息
    public static string getVerifySMS = "/users/sms_code";//获取短信验证码

    public static string getUserVideoInfo = "/models";//获取用户视频信息
    public static string uploadUserVideoInfo = "/models";//上传用户视频信息
    public static string deleteUserVideoInfo = "/models";//删除用户视频信息
    public static string batchDeleteUserVideoInfo = "/models/batch_delete";//删除用户视频信息
    public static string shareMainDomainName = "https://app-dev.dongpa.xyz/share/";

    //OSS相关
    public static string OSS_endPoint = "http://oss.dongpa.xyz";
    public static string OSS_accessKeyId = "LTAIbTz7Lq0ovOO5";
    public static string OSS_accessKeySecret = "0YcsP87PgIm19YXYqCTw6dhusBxCSA";
    public static string OSS_bucketName = "prometh-test";
    public static string OSS_position = "oss-cn-shenzhen";
    public static string OSS_uploadVideoUrl = "AppHoloViews/Data/";//视频上传的阿里云路径"
    public static string OSS_uploadShareFilePath = "";
}

