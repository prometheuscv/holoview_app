using System.Collections;
using UnityEngine;

/// <summary>
/// Ar触控交互管理器 音乐模式旋转、移动uma，缩放父物体 剧场模式旋转、移动、缩放父物体
/// </summary>
public class MakerARTouchManager : SingletonMonoClass<MakerARTouchManager>
{
    public GameObject DefaultModel;
    public float currentScaleOffset;
    public Transform arPlane;

    /// <summary>
    /// 旋转、缩放的物体
    /// </summary>
    public GameObject currentHandlingModel;
    /// <summary>
    /// 用以射线检测的平面
    /// </summary>
    Plane m_Plane;
    /// <summary>
    /// 用以检测操控目标位置的射线
    /// </summary>
    Ray rayToModel;
    /// <summary>
    /// 射线检测的信息
    /// </summary>
    RaycastHit rayHitModel;
    LayerMask rayMaskModel = 1 << 10;
    Camera arCamera;
    /// <summary>
    /// 射线中心点
    /// </summary>
    float enter;
    /// <summary>
    /// 用以缩放的上一次缓存距离
    /// </summary>
    float cachedTouchDistance;
    /// <summary>
    /// 用以缩放的上一次缓存缩放比
    /// </summary>
    float cachedAugmentationScale;
    /// <summary>
    /// 用以缩放
    /// </summary>
    bool isFirstFrameWithTwoTouches;
    readonly float scaleRangeMin = 0.6f;
    readonly float scaleRangeMax = 50f;
    public float OffsetY;

    /// <summary>
    /// 限制拖拽距离
    /// </summary>
    float moveDistance;
    [HideInInspector]
    /// <summary>
    /// 是否激活Ar交互
    /// </summary>
    public bool enableTouchModel;

    #region MTA统计

    readonly float timerMax = 2f;
    float timer;
    bool isScaling;
    bool isDraging;
    bool isRotating;

    #endregion

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        arCamera = Camera.main;
        moveDistance = 1000;
        enableTouchModel = true;

        if (DefaultModel!=null)
        {
            currentHandlingModel = DefaultModel;
        }
    }

    void Update()
    {
        if (enableTouchModel)
        {
            timer += Time.deltaTime;

            if (Input.touchCount > 0)
            {
                if (Input.touchCount == 1)
                {
                    var currentTouch = Input.GetTouch(0);
                    switch (currentTouch.phase)
                    {

                        case TouchPhase.Moved:
                            currentHandlingModel.transform.Rotate(new Vector3(0, -currentTouch.deltaPosition.x, 0));
                            break;
                        case TouchPhase.Ended:
                            isDraging = false;
                            isRotating = false;
                            isScaling = false;
                            break;
                    }
                }
                else if (Input.touchCount == 2)
                {
                    float currentTouchDistance = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);

                    if (isFirstFrameWithTwoTouches)
                    {
                        //ChangeParent();
                        cachedTouchDistance = currentTouchDistance;
                        isFirstFrameWithTwoTouches = false;
                    }
                    float scaleMultiplier = currentTouchDistance / cachedTouchDistance;
                    float scaleAmount = cachedAugmentationScale * scaleMultiplier;
                    float scaleValueClamped = Mathf.Clamp(scaleAmount, scaleRangeMin * currentScaleOffset, scaleRangeMax * currentScaleOffset);

                    isDraging = false;
                    isRotating = false;
                    currentHandlingModel.transform.localScale = new Vector3(scaleValueClamped, scaleValueClamped, scaleValueClamped);
                }
            }
            else
            {
                isScaling = false;
                isFirstFrameWithTwoTouches = true;
                if (currentHandlingModel)
                    cachedAugmentationScale = currentHandlingModel.transform.localScale.x;
            }
        }
        else
            timer = 0;
    }
}