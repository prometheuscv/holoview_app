﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// 主管理器
/// </summary>
public class MainManager : SingletonMonoClass<MainManager>
{
    public URLTypes urlTypes;
    public string currentURLType;
    public void Awake()
    {
        Instance = this;
        Application.targetFrameRate = 60;

        switch (MainManager.Instance.urlTypes)
        {
            case URLTypes.dev:
                currentURLType = Constant.devURL;
                break;
            case URLTypes.online:
                currentURLType = Constant.onlineURL;
                break;
        }

    }

    private void Update()
    {

    }

    // 程序逻辑入口
    IEnumerator Start()
    {
        //初始化EventManager
        yield return StartCoroutine(EventManager.Instance.Init());
        //初始化LoomManager
        yield return StartCoroutine(LoomManager.Instance.Init());
        //初始化ShareSDKManager
        yield return StartCoroutine(ShareSDKManager.Instance.Init());
        //初始化NetworkManager
        yield return StartCoroutine(NetworkManager.Instance.Init());
        //初始化ResourceManager
        yield return StartCoroutine(ResourceManager.Instance.Init());
        //初始化SceneNavigationManager
        yield return StartCoroutine(SceneNavigationManager.Instance.Init());
        //初始化DataManager
        yield return StartCoroutine(NetworkDataInfoManager.Instance.Init());
        //初始化MeshVideoManager
        yield return StartCoroutine(MeshVideoManager.Instance.Init());
        //初始化PermissionManager
        yield return StartCoroutine(PermissionManager.Instance.Init());
        //初始化AudioManager
        yield return StartCoroutine(AudioManager.Instance.Init());
        //初始化ArUtilityManager
        yield return StartCoroutine(ArUtilityManager.Instance.Init());
        //初始化ArUtilityManager
        yield return StartCoroutine(ModelEffectManager.Instance.Init());
        InitComplete();
    }

    void InitComplete()
    {
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_IndexLoadingScene);
    }
    
    private void OnDestroy()
    {
    }
}