﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class MeshVideoManager : SingletonMonoManager<MeshVideoManager>
{
    public ResourceManager resourceManager;
    private bool isDownloading;
    private string videoPath;
    public string currentVideoSourcePath;
    
    public string downloadVideoUrl;
    public override IEnumerator Init()
    {
        resourceManager = ResourceManager.Instance;
        yield return null;
    }
    
    //视频下载与管理部分
    public string GetNameByPath(string path)
    {
        string name = "";
        int index = path.IndexOf("Video_");
        if (index > 0)
        {
            name = path.Substring(index);
        }
        return name;
    }

    public bool CheckVideoNameExist(string path)
    {
        string videoName = GetNameByPath(path);
        bool exist = resourceManager.IsFileExist("/video", videoName);
        return exist;
    }

    public bool CheckMeshVideoExist(string path,string size)
    {
        string videoName = GetNameByPath(path);
        if (resourceManager.IsFolderExist("/video"))
        {
            if (resourceManager.IsFileExist("/video", videoName))
            {
                GetVideoPath(videoName);
                LoadToPreview();
                return true;
            }
            else
            {
                ToastManager.Instance.ShowDialogBox("点击‘确定’开始下载资源包",$"(包体大小为{size})", () =>
                {
                    DownLoadMeshVideo(path);
                },null,0);
                return false;
            }
        }
        else
        {
            ToastManager.Instance.ShowDialogBox("点击‘确定’开始下载资源包",$"(包体大小为{size})", () =>
            {
                DownLoadMeshVideo(path);
            },null,0);
            return false;
        }
    }

    public void LoadToPreview()
    {
        SceneNavigationManager.Instance.UnLoadScene(SceneNavigationManager.Instance.currentSceneName);
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_PreviewScene);
    }

    private void GetVideoPath(string name)
    {
        currentVideoSourcePath = Application.persistentDataPath + "/video" + "/" + name;
    }
    
    private void DownLoadMeshVideo(string path)
    {
        SceneNavigationManager.Instance.UnLoadScene(SceneNavigationManager.Instance.currentSceneName);
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_LoadingScene);
        
        downloadVideoUrl = path;
        string videoName = GetNameByPath(path);
        isDownloading = true;

        NetworkManager.Instance.EnableOssDownloadSpeedStatistics();
        NetworkManager.Instance.GetOssFileByUrlAsync(path, (data, para) =>
            {
                byte[] resourceData = (byte[])data;
                if (resourceData != null)
                {
                    EventManager.Instance.DispathchEvent(Constant.EVENT_UPDATE_OSS_DOWNLOAD_COMPLETE);
                    resourceManager.WriteFile("/video", videoName, resourceData);
                    GetVideoPath(videoName);
                    LoadToPreview();
                }
                else
                {
                    EventManager.Instance.DispathchEvent(Constant.EVENT_OSS_DOWNLOAD_ERROR);
                }
                
                isDownloading = false;
                NetworkManager.Instance.DisableOssDownloadSpeedStatistics();
            },
            progressCallback: (percentDone) =>
            {
                float percentDoneF = ((float) percentDone)/100f;
                EventManager.Instance.DispathchEvent(
                    Constant.EVENT_UPDATE_OSS_DOWNLOAD_PROGRESS, percentDoneF);
            }, tagObj: null, errorCallback: s =>
            {
                EventManager.Instance.DispathchEvent(Constant.EVENT_OSS_DOWNLOAD_ERROR);
                isDownloading = false;
                NetworkManager.Instance.DisableOssDownloadSpeedStatistics();
            }, cancelable: true, true);
    }

    public void CancelDownload()
    {
        if (isDownloading)
        {
            NetworkManager.Instance.CancelDownloadByUrl(downloadVideoUrl);
        }
    }
}