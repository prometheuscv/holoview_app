﻿using System;
using System.Collections;
using System.IO;
using Aliyun.OSS;
using Aliyun.OSS.Common;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

/// <summary>
/// 核心管理器：网络管理器。负责向服务器、阿里云进行网络请求和下载
/// </summary>
public class NetworkManager : SingletonMonoManager<NetworkManager>
{
    /// <summary>
    /// 阿里云访问client
    /// </summary>
    private OssClient client;

    private string endPoint;
    private string accessKeyId;
    private string accessKeySecret;
    private string bucketName;

    /// <summary>
    /// 阿里云流式下载文件所定义的缓存大小，单位字节
    /// </summary>
    private const int BufferLength = 4096;

    /// <summary>
    /// 异步下载阿里云数据任务队列
    /// </summary>
    private List<GetOssFileTask> getOssFileTasksAsync;

    /// <summary>
    /// 同步下载阿里云数据任务队列
    /// </summary>
    private List<GetOssFileTask> getOssFileTasks;

    /// <summary>
    /// 异步上传阿里云数据任务队列
    /// </summary>
    private List<PutOssFileTask> putOssFileTasksAsync;

    /// <summary>
    /// 同步上传阿里云数据任务队列
    /// </summary>
    private List<PutOssFileTask> putOssFileTasks;

    /// <summary>
    /// 阿里云下载任务对象池
    /// </summary>
    private List<GetOssFileTask> getOssFileTaskPool;

    /// <summary>
    /// 阿里云下载任务对象池默认数量
    /// </summary>
    private const int GetOssFileTaskPoolNum = 20;

    /// <summary>
    /// 辅助变量，锁。
    /// </summary>
    private static readonly object LockObj = new object();

    /// <summary>
    /// 辅助变量，使用oss下载的字节数。用于辅助显示oss的下载速率
    /// </summary>
    private long ossDownloadedBytes;

    /// <summary>
    /// 辅助变量，使用oss上传的字节数。用于辅助显示oss的上传速率
    /// </summary>
    private long ossUploadedBytes;

    //下载缓存目录
    private string cachePath = "/cache";

    public Dictionary<string, CancellationTokenSource> cancelTokenDic =
        new Dictionary<string, CancellationTokenSource>();

    public bool cancelUpload;

    public UserToken userToken;

    public override IEnumerator Init()
    {
        //新建下载缓存目录
        cachePath = Application.persistentDataPath + cachePath;
        if (!Directory.Exists(cachePath))
        {
            Directory.CreateDirectory(cachePath);
        }

        endPoint = Constant.OSS_endPoint;
        accessKeyId = Constant.OSS_accessKeyId;
        accessKeySecret = Constant.OSS_accessKeySecret;
        bucketName = Constant.OSS_bucketName;

        // 创建ClientConfiguration实例
        var config = new ClientConfiguration
        {
            // 配置使用Cname，也就是自定义域名
            IsCname = true
        };
        // 创建OSSClient实例。
        client = new OssClient(endPoint, accessKeyId, accessKeySecret, config);

        getOssFileTasksAsync = new List<GetOssFileTask>();
        getOssFileTasks = new List<GetOssFileTask>();
        putOssFileTasksAsync = new List<PutOssFileTask>();
        putOssFileTasks = new List<PutOssFileTask>();

        getOssFileTaskPool = new List<GetOssFileTask>();
        for (int i = 0; i < GetOssFileTaskPoolNum; i++)
        {
            var task = new GetOssFileTask();
            getOssFileTaskPool.Add(task);
        }

        yield return null;
    }

    private void Update()
    {
        DoUpdate();
    }

    /// <summary>
    /// 网络调度核心，本质上是通过在主线程的Update当中轮询各种任务队列并执行之.
    /// </summary>
    private void DoUpdate()
    {
        if (getOssFileTasksAsync.Count > 0)
        {
            var getTaskAsync = getOssFileTasksAsync[0];
            if (!getTaskAsync.cancelable)
            {
                //使用Loom多线程进行处理
                LoomManager.Instance.RunAsync(() =>
                {
                    var result = DoGetOssFileByUrl(getTaskAsync.url, getTaskAsync, getTaskAsync.progressCallback,
                        getTaskAsync.errorCallback);
                    //运行完成后，切回主线程
                    LoomManager.Instance.QueueOnMainThread(getTaskAsync.callback, result, getTaskAsync.tag);
                });
            }
            else
            {
                var cancelableAction = new CancelableAction();
                var cancelToken = new CancellationTokenSource();
                cancelTokenDic[getTaskAsync.url] = cancelToken;
                cancelableAction.param = cancelToken;
                cancelableAction.action = token =>
                {
                    var result = DoGetOssFileByUrl(getTaskAsync.url, getTaskAsync, getTaskAsync.progressCallback,
                        getTaskAsync.errorCallback);
                    //运行完成后，切回主线程
                    LoomManager.Instance.QueueOnMainThread(getTaskAsync.callback, result, getTaskAsync.tag);
                    cancelTokenDic.Remove(getTaskAsync.url);
                };
                //使用Loom多线程进行处理
                LoomManager.Instance.RunAsync2(cancelableAction);
            }

            getOssFileTasksAsync.Remove(getTaskAsync);
            ReturnOssTaskToPool(getTaskAsync);
        }

        if (getOssFileTasks.Count > 0)
        {
            var getTask = getOssFileTasks[0];
            //使用单线程进行处理
            var result = DoGetOssFileByUrl(getTask.url, getTask, getTask.progressCallback, getTask.errorCallback);
            getTask.callback?.Invoke(result, getTask.tag);
            getOssFileTasks.Remove(getTask);
            ReturnOssTaskToPool(getTask);
        }

        if (putOssFileTasksAsync.Count > 0)
        {
            var putTaskAsync = putOssFileTasksAsync[0];
            //使用Loom多线程进行处理
            LoomManager.Instance.RunAsync(() =>
            {
                DoUploadOssFileByUrl(putTaskAsync.url, putTaskAsync.data, putTaskAsync.progressCallback,
                    putTaskAsync.errorCallback);
                //运行完成后，切回主线程
                LoomManager.Instance.QueueOnMainThread(putTaskAsync.callback, null, putTaskAsync.tag);
            });
            putOssFileTasksAsync.Remove(putTaskAsync);
        }

        if (putOssFileTasks.Count > 0)
        {
            var putTask = putOssFileTasks[0];
            //使用单线程进行处理
            DoUploadOssFileByUrl(putTask.url, putTask.data, putTask.progressCallback, putTask.errorCallback);
            putTask.callback?.Invoke(null, putTask.tag);
            putOssFileTasks.Remove(putTask);
        }
    }

    /// <summary>
    /// 开启oss下载或者上传速度统计
    /// </summary>
    public void EnableOssDownloadSpeedStatistics()
    {
        //0秒后立即调用，之后每1秒调用一次
        InvokeRepeating(nameof(DoOssDownloadSpeedStatistics), 0, 1f);
    }

    /// <summary>
    /// 关闭oss下载或者上传速度统计
    /// </summary>
    public void DisableOssDownloadSpeedStatistics()
    {
        CancelInvoke(nameof(DoOssDownloadSpeedStatistics));
    }

    /// <summary>
    /// 进行oss下载或者上传速度统计
    /// </summary>
    private void DoOssDownloadSpeedStatistics()
    {
        //考虑到有可能是多线程调用，所以需要加锁
        lock (LockObj)
        {
            //将byte转为KB
            long downloadSpeed = ossDownloadedBytes / 1024;
            //抛事件告知外部当前的下载速率
            EventManager.Instance.DispathchEvent(Constant.EVENT_UPDATE_OSS_DOWNLOAD_SPEED,
                downloadSpeed);
            ossDownloadedBytes = 0;
        }
        Debug.Log("进行速度统计");
    }

    /// <summary>
    /// 核心函数：获取阿里云上的文件数据，返回格式是byte[]
    /// </summary>
    /// <returns>The get OSSF ile by URL.</returns>
    /// <param name="url">URL.</param>
    /// <param name="getTask">Get task.</param>
    /// <param name="progressCallback">Progress callback.</param>
    /// <param name="errorCallback">Error callback.</param>
    private byte[] DoGetOssFileByUrl(string url, GetOssFileTask getTask,
        Action<int> progressCallback = null, Action<string> errorCallback = null)
    {
        byte[] result = null;
        byte[] buffer = new byte[BufferLength];
        try
        {
            FileStream fileStream = null;
            string cacheFilePath = null;
            long cacheFileLength = 0;

            // 下载文件，采用流下载
            var getObjectRequest = new GetObjectRequest(bucketName, url);
            var objectMetadata = client.GetObjectMetadata(bucketName, url);
            //如果要求使用缓存，则需要对现有的缓存文件进行判定，比对缓存文件已有的字节数和原始文件字节数，从而确定oss下载请求的数据段范围
            if (getTask.cacheable)
            {
                //对url进行解析，拿取对应的文件名
                var fileNames = url.Split('/');
                var cacheFileName = fileNames[fileNames.Length - 1] + ".cache";
                cacheFilePath = cachePath + "/" + cacheFileName;
                fileStream = File.Open(cacheFilePath, FileMode.OpenOrCreate);
                cacheFileLength = fileStream.Length;
                //核心语句：划定oss下载请求的数据段范围，小心是从0开头开始计数的，所以末尾要-1
                getObjectRequest.SetRange(cacheFileLength, objectMetadata.ContentLength - 1);
                //最后将文件指针移动到尾部
                fileStream.Seek(0, SeekOrigin.End);
            }

            //默认的progress回调
            void DefaultProgressCallback(object sender, StreamTransferProgressArgs args)
            {
                //考虑到有可能是多线程调用，所以需要加锁
                lock (LockObj)
                {
                    ossDownloadedBytes += args.IncrementTransferred;
                }

                if (progressCallback == null) return;
                //当前已下载的进度。这里需要结合是否有缓存文件作为区分依据
                int percentDone;
                if (getTask.cacheable)
                {
                    float t = 100 * (cacheFileLength + args.TransferredBytes) / objectMetadata.ContentLength;
                    percentDone = Mathf.RoundToInt(t);
                }
                else
                {
                    percentDone = args.PercentDone;
                }

                progressCallback(percentDone);
            }

            getObjectRequest.StreamTransferProgress += DefaultProgressCallback;

            Debug.Log("真实执行加载任务:" + url);
            var ossObject = client.GetObject(getObjectRequest);


            CancellationTokenSource cancelToken = null;
            if (cancelTokenDic.ContainsKey(getTask.url))
            {
                cancelToken = cancelTokenDic[getTask.url];
            }

            using (var requestStream = ossObject.Content)
            {
                //使用缓存的方式和非缓存方式，流读取/存储方式各有不同。无缓存，则使用MemoryStream直接存到内存当中，
                //有缓存，则需要边读边写入缓存文件当中
                if (!getTask.cacheable)
                {
                    result = new byte[requestStream.Length];
                    using (var ms = new MemoryStream())
                    {
                        while (true)
                        {
                            //如果被从外部取消了，则将result置空并直接break
                            if (cancelToken != null && cancelToken.IsCancellationRequested)
                            {
                                result = null;
                                break;
                            }

                            var read = requestStream.Read(buffer, 0, buffer.Length);
                            if (read <= 0)
                            {
                                result = ms.ToArray();
                                Debug.Log("Get object succeeded, url:" + url);
                                break;
                            }

                            ms.Write(buffer, 0, read);
                        }
                    }
                }
                else
                {
                    while (true)
                    {
                        //如果被从外部取消了，则直接break
                        if (cancelToken != null && cancelToken.IsCancellationRequested)
                        {
                            break;
                        }

                        var read = requestStream.Read(buffer, 0, buffer.Length);
                        if (read <= 0)
                        {
                            Debug.Log("Get object succeeded, url:" + url);
                            break;
                        }

                        //写入缓存文件当中
                        fileStream.Write(buffer, 0, read);
                    }
                }
            }

            //最后，如果使用了缓存文件，那么最后一步需要判断当前的缓存文件大小是否与服务端原始文件大小尺寸相同，如果相同，
            //则将result设置为该缓存文件所有的byte[]，并且删除该缓存文件，如果小于原始文件尺寸，则将result设置为null（也就是不变）
            if (getTask.cacheable)
            {
                fileStream.Close();
                var cacheBytes = File.ReadAllBytes(cacheFilePath);
                if (cacheBytes.LongLength == objectMetadata.ContentLength)
                {
                    result = cacheBytes;
                    File.Delete(cacheFilePath);
                }
            }

            getObjectRequest.StreamTransferProgress -= DefaultProgressCallback;
        }
        catch (OssException ex)
        {
            var errorLog =
                $"Oss Download Error————Failed with error code: {ex.ErrorCode}; Error info: {ex.Message}. \nRequestID:{ex.RequestId}\tHostID:{ex.HostId}";
            Debug.Log(errorLog);

            errorCallback?.Invoke(errorLog);
        }
        catch (Exception ex)
        {
            Debug.Log("System Error————Failed with error info:" + ex.Message);

            errorCallback?.Invoke(ex.Message);
        }

        return result;
    }


    /// <summary>
    /// 核心函数：上传文件数据到阿里云。
    /// </summary>
    /// <param name="url">URL.</param>
    /// <param name="data">Data.</param>
    /// <param name="progressCallback">Progress callback.</param>
    /// <param name="errorCallback"></param>
    private void DoUploadOssFileByUrl(string url, byte[] data,
        Action<int> progressCallback = null, Action<string> errorCallback = null)
    {
        try
        {
            using (Stream photoStream = new MemoryStream(data))
            {
                // 上传文件
                var putObjectRequest = new PutObjectRequest(bucketName, url, photoStream);

                //默认的progress回调
                void DefaultProgressCallback(object sender, StreamTransferProgressArgs args)
                {
                    //考虑到有可能是多线程调用，所以需要加锁
                    lock (LockObj)
                    {
                        ossDownloadedBytes += args.IncrementTransferred;
                    }

                    if (progressCallback == null) return;
                    //当前已上传的进度
                    var percentDone = args.PercentDone;
                    if (cancelUpload)
                    {
                        cancelUpload = false;
                        throw new Exception();
                    }

                    progressCallback(percentDone);
                }

                putObjectRequest.StreamTransferProgress += DefaultProgressCallback;

                client.PutObject(putObjectRequest);
                Debug.Log("Upload object succeeded, url:" + url);
                putObjectRequest.StreamTransferProgress -= DefaultProgressCallback;
            }
        }
        catch (OssException ex)
        {
            string errorLog = string.Format(
                "Oss Upload Error————Failed with error code: {0}; Error info: {1}. \nRequestID:{2}\tHostID:{3}",
                ex.ErrorCode, ex.Message, ex.RequestId, ex.HostId);
            Debug.Log(errorLog);

            errorCallback?.Invoke(errorLog);
        }
        catch (Exception ex)
        {
            Debug.Log("System Error————Failed with error info:" + ex.Message);
            errorCallback?.Invoke(ex.Message);
        }
    }

    public void CancelCurrentUpload()
    {
        cancelUpload = true;
    }

    GetOssFileTask GetOssTaskFromPool()
    {
        GetOssFileTask result;
        if (getOssFileTaskPool.Count > 0)
        {
            result = getOssFileTaskPool[0];
            getOssFileTaskPool.Remove(result);
        }
        else
        {
            result = new GetOssFileTask();
        }

        return result;
    }

    void ReturnOssTaskToPool(GetOssFileTask task)
    {
        task.url = null;
        task.callback = null;
        task.errorCallback = null;
        task.progressCallback = null;
        task.cancelable = false;
        task.cacheable = false;
        getOssFileTaskPool.Add(task);
    }

    /// <summary>
    /// 核心函数：同步获取阿里云上的文件数据。后台会将本次任务压入阿里云同步下载队列，并通过回调进行反馈。
    /// </summary>
    /// <param name="url">URL.</param>
    /// <param name="callback">Callback.</param>
    /// <param name="progressCallback">Progress callback.</param>
    /// <param name="tagObj">Tag.</param>
    /// <param name="errorCallback">Error callback.</param>
    /// <param name="cancelable"></param>
    /// <param name="cacheable"></param>
    public void GetOssFileByUrl(string url, Action<object, object> callback,
        Action<int> progressCallback = null, object tagObj = null, Action<string> errorCallback = null,
        bool cancelable = false, bool cacheable = false)
    {
        GetOssFileTask task = GetOssTaskFromPool();
        task.url = url;
        task.tag = tagObj;
        task.callback = callback;
        task.errorCallback = errorCallback;
        task.progressCallback = progressCallback;
        task.cancelable = cancelable;
        task.cacheable = cacheable;
        //压入同步下载任务队列
        getOssFileTasks.Add(task);
    }

    /// <summary>
    /// 核心函数：异步获取阿里云上的文件数据。后台会将本次任务压入阿里云异步下载队列，并通过回调进行反馈
    /// </summary>
    /// <param name="url">URL.</param>
    /// <param name="callback">Callback.</param>
    /// <param name="progressCallback">Progress callback.</param>
    /// <param name="tagObj">Tag.</param>
    /// <param name="errorCallback">Error callback.</param>
    /// <param name="cancelable"></param>
    /// <param name="cacheable"></param>
    public void GetOssFileByUrlAsync(string url, Action<object, object> callback,
        Action<int> progressCallback = null, object tagObj = null, Action<string> errorCallback = null,
        bool cancelable = false, bool cacheable = false)
    {
        GetOssFileTask task = GetOssTaskFromPool();
        task.url = url;
        task.tag = tagObj;
        task.callback = callback;
        task.errorCallback = errorCallback;
        task.progressCallback = progressCallback;
        task.cancelable = cancelable;
        task.cacheable = cacheable;
        //压入异步下载任务队列
        getOssFileTasksAsync.Add(task);
    }

    /// <summary>
    /// 核心函数：同步上传文件数据到阿里云。后台会将本次任务压入阿里云同步上传队列，并通过回调进行反馈
    /// </summary>
    /// <param name="url">URL.</param>
    /// <param name="data">Data.</param>
    /// <param name="callback">Callback.</param>
    /// <param name="progressCallback">Progress callback.</param>
    /// <param name="tagObj">Tag.</param>
    /// <param name="errorCallback">Error callback.</param>
    public void UploadOssFileByUrl(string url, byte[] data, Action<object, object> callback
        , Action<int> progressCallback = null, object tagObj = null,
        Action<string> errorCallback = null)
    {
        //压入同步上传任务队列
        putOssFileTasks.Add(new PutOssFileTask
        {
            url = url,
            data = data,
            callback = callback,
            progressCallback = progressCallback,
            tag = tagObj,
            errorCallback = errorCallback
        });
    }

    /// <summary>
    /// 核心函数：异步上传文件数据到阿里云。后台会将本次任务压入阿里云异步上传队列，并通过回调进行反馈
    /// </summary>
    /// <param name="url">URL.</param>
    /// <param name="data">Data.</param>
    /// <param name="callback">object1 真实数据 object2 上下文</param>
    /// <param name="progressCallback">Progress callback.</param>
    /// <param name="tagObj">Tag.</param>
    /// <param name="errorCallback">Error callback.</param>
    public void UploadOssFileByUrlAsync(string url, byte[] data, Action<object, object> callback
        , Action<int> progressCallback = null, object tagObj = null,
        Action<string> errorCallback = null)
    {
        //压入异步上传任务队列
        putOssFileTasksAsync.Add(new PutOssFileTask
        {
            url = url,
            data = data,
            callback = callback,
            progressCallback = progressCallback,
            tag = tagObj,
            errorCallback = errorCallback
        });
    }

    public void WebRequestJson(string url, string json, Func<byte[], bool> callback = null,
        Action<string> errorCallback = null, bool addToken = false)
    {
        StartCoroutine(DoWebRequestJson(url, json, callback, errorCallback, addToken));
    }

    private IEnumerator DoWebRequestJson(string url, string json, Func<byte[], bool> callback = null, Action<string> errorCallback = null, bool addToken = false)
    {
        using (var request = new UnityWebRequest(url, "POST"))
        {
            if (addToken)
            {
                request.SetRequestHeader("pm-token", userToken.token);
                Debug.LogWarning(request.GetRequestHeader("pm-token"));
            }
            else
            {

            }

            if (string.IsNullOrEmpty(json))
            {
                json = "null";
            }

            byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
            request.uploadHandler = new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");

            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                string errorLog = $"Web request json error————Code:{request.responseCode}, Error Info:{request.error}";
                Debug.Log(errorLog);

                if (errorCallback != null)
                {
                    errorCallback.Invoke(errorLog);
                }

                yield break;
            }

            if (callback == null) yield break;
            var postResult = request.downloadHandler.data;
            callback.Invoke(postResult);
        }
    }

    /// <summary>
    /// 与服务器的Post请求，请求的反馈类型是byte[]，回调函数自己处理反馈内容（转string或者转texture）
    /// </summary>
    /// <returns>The request post.</returns>
    /// <param name="url">URL.</param>
    /// <param name="form">Form.</param>
    /// <param name="callback">Callback.</param>
    /// <param name="errorCallback"></param>
    public void WebRequestPost(string url, WWWForm form, Func<byte[], bool> callback = null,
        Action<string> errorCallback = null, bool addToken = false)
    {
        StartCoroutine(DoWebRequestPost(url, form, callback, errorCallback, addToken));
    }

    private IEnumerator DoWebRequestPost(string url, WWWForm form, Func<byte[], bool> callback = null, Action<string> errorCallback = null, bool addToken = false)
    {
        using (var request = UnityWebRequest.Post(url, form))
        {
            if (addToken)
            {
                request.SetRequestHeader("pm-token", userToken.token);
            }
            else
            {

            }

            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                string errorLog = $"Web request post error————Code:{request.responseCode}, Error Info:{request.error}";
                Debug.Log(errorLog);

                if (errorCallback != null)
                {
                    errorCallback.Invoke(errorLog);
                }

                yield break;
            }

            if (callback == null) yield break;
            var postResult = request.downloadHandler.data;
            callback.Invoke(postResult);
        }
    }

    /// <summary>
    /// 与服务器的Get请求，请求的反馈类型是byte[]，回调函数自己处理反馈内容（转string或者转texture）
    /// </summary>
    /// <returns>The request post.</returns>
    /// <param name="url">URL.</param>
    /// <param name="callback">Callback.</param>
    public void WebRequestGet(string url, Func<byte[], bool> callback = null, Action<string> errorCallback = null, bool addToken = false)
    {
        StartCoroutine(DoWebRequestGet(url, callback, errorCallback, addToken));
    }

    private IEnumerator DoWebRequestGet(string url, Func<byte[], bool> callback = null,
        Action<string> errorCallback = null, bool addToken = false)
    {
        using (var request = UnityWebRequest.Get(url))
        {
            if (addToken)
            {
                request.SetRequestHeader("pm-token", userToken.token);
                Debug.Log(request.GetRequestHeader("pm-token"));
            }
            else
            {

            }

            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                var errorLog = $"Web request get error————Code:{request.responseCode}, Error Info:{request.error}";
                Debug.Log(errorLog);

                if (errorCallback != null)
                {
                    errorCallback.Invoke(errorLog);
                }

                yield break;
            }

            if (callback == null) yield break;
            var getResult = request.downloadHandler.data;
            callback.Invoke(getResult);
            yield return null;
        }
    }

    /// <summary>
    /// 与服务器的Del请求，请求的反馈类型是byte[]，回调函数自己处理反馈内容（转string或者转texture）
    /// </summary>
    /// <returns>The request post.</returns>
    /// <param name="url">URL.</param>
    /// <param name="callback">Callback.</param>
    public void WebRequestDel(string url, Func<byte[], bool> callback = null, Action<string> errorCallback = null, bool addToken = false)
    {
        StartCoroutine(DoWebRequestDel(url, callback, errorCallback, addToken));
    }

    private IEnumerator DoWebRequestDel(string url, Func<byte[], bool> callback = null,
        Action<string> errorCallback = null, bool addToken = false)
    {
        using (var request = UnityWebRequest.Delete(url))
        {
            if (addToken)
            {
                request.SetRequestHeader("pm-token", userToken.token);
            }
            else
            {

            }

            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                var errorLog = $"Web request get error————Code:{request.responseCode}, Error Info:{request.error}";
                Debug.Log(errorLog);

                if (errorCallback != null)
                {
                    errorCallback.Invoke(errorLog);
                }

                yield break;
            }

            if (callback == null) yield break;
            var getResult = request.downloadHandler;
            Debug.Log(getResult.ToString());
            //callback.Invoke(getResult);
            yield return null;
        }
    }


    /// <summary>
    /// 设置文件为公共读权限
    /// </summary>
    /// <param name="fileUrl">File URL.</param>
    public void SetAclToPublicRead(string fileUrl)
    {
        client.SetObjectAcl(bucketName, fileUrl, CannedAccessControlList.PublicRead);
    }

    public void CancelDownloadByUrl(string url)
    {
        var cancelToken = cancelTokenDic[url];
        cancelToken?.Cancel();
    }
    public void SaveToken(UserToken userToken)
    {
        this.userToken = userToken;
        string token = JsonConvert.SerializeObject(userToken);

        ResourceManager.Instance.WriteTextFile("/UserToken", "UserToken", token);
    }

    public bool GetToken()
    {
        if (ResourceManager.Instance.IsFileExist("/UserToken", "UserToken"))
        {
            string token = "";
            token = ResourceManager.Instance.ReadTextFile("/UserToken", "UserToken");

            Debug.Log("GetToken:" + token);
            userToken = JsonConvert.DeserializeObject<UserToken>(token);

            if (CheckExpire(userToken.expire))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    public void DeletedToken()
    {
        userToken = null;
        ResourceManager.Instance.DeleteFile("/UserToken", "UserToken");
        UnityWebRequest.ClearCookieCache();
    }

    public bool CheckExpire(string expire)
    {
        var exStr = expire.ToString();
        var shortStr = exStr.Substring(0, 10);
        var oldExpire = Convert.ToInt64(shortStr);
        TimeSpan st = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0);
        var currentTime = Convert.ToInt64(st.TotalSeconds);
        var delta = oldExpire - currentTime;
        if (delta > 0)
        {
            return true;
        }
        else
        {
            DeletedToken();
            return false;
        }
    }

    public string canceledUploadUrl;
    public void CheckOSSFileExist(string path)
    {
        canceledUploadUrl = path;
        Invoke("CheckAndDeleteOSSFileExist", 1);
    }

    public void CheckAndDeleteOSSFileExist()
    {
        var exist = client.DoesObjectExist(bucketName, canceledUploadUrl);
        //Debug.Log(canceledUploadUrl+":Object exist ? " + exist);
        if (exist)
        {
            client.DeleteObject(bucketName, canceledUploadUrl);
            //Debug.Log("Delete object succeeded");
            canceledUploadUrl = "";
        }
    }
}

public struct GetOssFileTask
{
    public string url;

    /// <summary>
    /// tag标签，用于回传一些必要的数据
    /// </summary>
    public object tag;

    public Action<object, object> callback;
    public Action<string> errorCallback;
    public Action<int> progressCallback;

    /// <summary>
    /// 是否可取消
    /// </summary>
    public bool cancelable;

    /// <summary>
    /// 是否可缓存
    /// </summary>
    public bool cacheable;
}

public struct PutOssFileTask
{
    public string url;

    /// <summary>
    /// tag标签，用于回传一些必要的数据
    /// </summary>
    public object tag;

    public Action<object, object> callback;
    public Action<string> errorCallback;
    public byte[] data;
    public Action<int> progressCallback;
}