﻿    using System;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// 核心：事件管理器，采用委托方式实现观察者模式
/// </summary>
public class EventManager : SingletonMonoManager<EventManager>
{
    public delegate void EventListenerDelegate(BaseEvent evt);

    //事件以及对应的处理函数字典
    Dictionary<string, EventListenerDelegate> eventListenerDic;

    public override IEnumerator Init()
    {
        eventListenerDic = new Dictionary<string, EventListenerDelegate>();
        yield return null;
    }

    /// <summary>
    /// 核心：添加事件侦听器，同样的事件同样的EventListener不会重复添加
    /// </summary>
    /// <param name="type">Type.</param>
    /// <param name="targetEventListener">Event handler.</param>
    public void AddEventListener(string type, EventListenerDelegate targetEventListener)
    {
        if (!eventListenerDic.ContainsKey(type))
        {
            eventListenerDic.Add(type, targetEventListener);
        }
        else
        {
            bool isFind = false;
            foreach (var eventListenerDelegate in eventListenerDic[type].GetInvocationList())
            {
                if (eventListenerDelegate.Equals(targetEventListener))
                {
                    isFind = true;
                    break;
                }
            }
            if (isFind == false)
            {
                eventListenerDic[type] += targetEventListener;
            }

        }
    }

    /// <summary>
    /// 核心：移除指定的事件侦听器，当所有该事件的侦听器被移除后，自动从事件字典中移除该事件条目
    /// </summary>
    /// <param name="type">Type.</param>
    /// <param name="targetEventListener">Target event listener.</param>
    public void RemoveEventListener(string type, EventListenerDelegate targetEventListener)
    {
        if (eventListenerDic.ContainsKey(type))
        {
            if (eventListenerDic[type] != null)
            {
                eventListenerDic[type] -= targetEventListener;
            }
            if (eventListenerDic[type] == null)
            {
                eventListenerDic.Remove(type);
                //Debug.Log("RemoveEventListener: No " + type + " Listener, Dic is clear");
            }
        }


    }

    /// <summary>
    /// 核心：移除所有的事件侦听器，当所有该事件的侦听器被移除后，自动从事件字典中移除该事件条目
    /// </summary>
    /// <param name="type">Type.</param>
    public void RemoveEventListeners(string type)
    {
        if (eventListenerDic.ContainsKey(type))
        {
            if (eventListenerDic[type] != null)
            {
                eventListenerDic[type] -= eventListenerDic[type];
            }
            if (eventListenerDic[type] == null)
            {
                eventListenerDic.Remove(type);
                //Debug.Log("RemoveEventListeners: No " + type + " Listener, Dic is clear");
            }
        }

    }

    /// <summary>
    /// 核心：传入指定的事件变量，派发指定事件，运行所有该事件的侦听函数
    /// </summary>
    /// <param name="evt">Evt.</param>
    public void DispathchEvent(BaseEvent evt)
    {
        if (eventListenerDic.ContainsKey(evt.Type) && eventListenerDic[evt.Type] != null)
        {
            eventListenerDic[evt.Type](evt);
        }
    }

    /// <summary>
    /// 核心：传入指定的事件参数，派发指定事件，运行所有该事件的侦听函数
    /// </summary>
    /// <param name="type">Type.</param>
    /// <param name="data">Data.</param>
    /// <param name="sender">Sender.</param>
    public void DispathchEvent(string type, object data = null, object sender = null)
    {
        var evt = new BaseEvent(type, data, sender);
        if (eventListenerDic.ContainsKey(evt.Type) && eventListenerDic[evt.Type] != null)
        {
            eventListenerDic[evt.Type](evt);
        }
    }

}
