﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

/// <summary>
/// 核心管理器，多线程管理器。用于Unity当中线程间的切换操作
/// </summary>
public class LoomManager : SingletonMonoManager<LoomManager>
{
    /// <summary>
    /// 支持的最大并发线程数
    /// </summary>
    public int maxThreads = 8;

    /// <summary>
    /// 当前正在运行的线程数，用于原子操作
    /// </summary>
    static int _numThreads;

    /// <summary>
    /// 主线程当中准备执行的action队列
    /// </summary>
    private List<NoDelayedQueueItem> actions;

    /// <summary>
    /// 主线程当中准备执行的延迟action队列
    /// </summary>
    private List<DelayedQueueItem> delayedActions;

    /// <summary>
    /// 主线程当中当前正在执行的action队列
    /// </summary>
    List<NoDelayedQueueItem> currentActions;

    /// <summary>
    /// 主线程当中当前正在执行的延迟action队列
    /// </summary>
    List<DelayedQueueItem> currentDelayedActions;

    public override IEnumerator Init()
    {
        actions = new List<NoDelayedQueueItem>();
        delayedActions = new List<DelayedQueueItem>();
        currentActions = new List<NoDelayedQueueItem>();
        currentDelayedActions = new List<DelayedQueueItem>();
        yield return null;
    }

    /// <summary>
    /// 添加指定action到主线程当中准备执行
    /// </summary>
    /// <param name="action">Action.</param>
    /// <param name="param">Parameter.</param>
    /// <param name="tagObj"></param>
    public void QueueOnMainThread(Action<object, object> action, object param, object tagObj = null)
    {
        QueueOnMainThread(action, param, 0f, tagObj);
    }

    /// <summary>
    /// 添加指定延迟action到主线程当中准备执行
    /// </summary>
    /// <param name="action">Action.</param>
    /// <param name="param">Parameter.</param>
    /// <param name="delayTime">Delay time.</param>
    /// <param name="tagObj"></param>
    private void QueueOnMainThread(Action<object, object> action, object param, float delayTime, object tagObj = null)
    {
        if (delayTime > 0)
        {
            lock (delayedActions)
            {
                delayedActions.Add(new DelayedQueueItem
                    {time = Time.time + delayTime, action = action, param = param, tag = tagObj});
            }
        }
        else
        {
            lock (actions)
            {
                actions.Add(new NoDelayedQueueItem {action = action, param = param, tag = tagObj});
            }
        }
    }

    public void RunAsync(Action action)
    {
        while (_numThreads >= maxThreads)
        {
            Thread.Sleep(1);
            //LogUtil.Log("命中Sleep");
        }

        //原子操作，标记当前正在运行的线程数+1
        Interlocked.Increment(ref _numThreads);
        //使用线程池，将action放到线程池中
        ThreadPool.QueueUserWorkItem(RunAction, action);
    }

    private void RunAction(object action)
    {
        try
        {
            ((Action) action)();
            //LogUtil.Log("当前子线程Id:{0}", Thread.CurrentThread.ManagedThreadId);
        }
        catch (Exception e)
        {
            Debug.Log("RunAction exception:"+ e.ToString());
        }
        finally
        {
            //原子操作，标记当前正在运行的线程数-1
            Interlocked.Decrement(ref _numThreads);
        }
    }

    public void RunAsync2(CancelableAction action)
    {
        while (_numThreads >= maxThreads)
        {
            Thread.Sleep(1);
            //LogUtil.Log("命中Sleep");
        }

        //原子操作，标记当前正在运行的线程数+1
        Interlocked.Increment(ref _numThreads);
        //使用线程池，将action放到线程池中
        ThreadPool.QueueUserWorkItem(RunAction2, action);
    }

    private void RunAction2(object action)
    {
        try
        {
            var cancelableAction = (CancelableAction) action;
            //核心：将CancellationTokenSource的一个实例作为参数传入多线程任务当中，以此辅助实现主线程当中取消子线程相关任务的功能
            cancelableAction.action(cancelableAction.param);
            //LogUtil.Log("当前子线程Id:{0}", Thread.CurrentThread.ManagedThreadId);
        }
        catch (Exception e)
        {
            Debug.Log("RunAction exception:"+ e.ToString());
        }
        finally
        {
            //原子操作，标记当前正在运行的线程数-1
            Interlocked.Decrement(ref _numThreads);
        }
    }

    void Update()
    {
        DoUpdate();
    }

    private void DoUpdate()
    {
        //核心就是下面这几句，本质上是通过在主线程的Update当中轮询队列并执行之
        if (actions.Count > 0)
        {
            lock (actions)
            {
                currentActions.Clear();
                currentActions.AddRange(actions);
                actions.Clear();
            }

            for (int i = 0; i < currentActions.Count; i++)
            {
                currentActions[i].action(currentActions[i].param, currentActions[i].tag);
            }

            //LogUtil.Log("当前主线程Id:{0}", Thread.CurrentThread.ManagedThreadId);
        }

        if (delayedActions.Count > 0)
        {
            lock (delayedActions)
            {
                currentDelayedActions.Clear();
                currentDelayedActions.AddRange(delayedActions.Where(d => d.time <= Time.time));
                for (int i = 0; i < currentDelayedActions.Count; i++)
                {
                    delayedActions.Remove(currentDelayedActions[i]);
                }
            }

            for (int i = 0; i < currentDelayedActions.Count; i++)
            {
                currentDelayedActions[i].action(currentDelayedActions[i].param, currentDelayedActions[i].tag);
            }
        }
    }
}

public struct DelayedQueueItem
{
    public float time;
    public Action<object, object> action;
    public object param;

    /// <summary>
    /// tag标签，用于回传一些必要的数据
    /// </summary>
    public object tag;
}

public struct NoDelayedQueueItem
{
    public Action<object, object> action;
    public object param;

    /// <summary>
    /// tag标签，用于回传一些必要的数据
    /// </summary>
    public object tag;
}

/// <summary>
/// 可追踪Action，用于辅助多线程下载任务取消的功能
/// </summary>
public class CancelableAction
{
    public Action<CancellationTokenSource> action;

    public CancellationTokenSource param;
}