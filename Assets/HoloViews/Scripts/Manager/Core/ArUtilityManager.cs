﻿using System.Collections;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

/// <summary>
/// Ar辅助类
/// </summary>
public class ArUtilityManager : SingletonMonoManager<ArUtilityManager>
{
#if UNITY_IOS && !UNITY_EDITOR
    [DllImport("__Internal")]
    static extern void _ShowStatusBar(bool isShow);
#endif
    
#if UNITY_ANDROID
#endif

    public override IEnumerator Init()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
#endif
        yield return null;
    }

    /// <summary>
    /// 面向ar相机
    /// </summary>
    /// <param name="augmentation">Augmentation.</param>
    public void RotateTowardCamera(GameObject augmentation)
    {
        if (Vuforia.VuforiaManager.Instance.ARCameraTransform != null)
        {
            var lookAtPosition = Vuforia.VuforiaManager.Instance.ARCameraTransform.position - augmentation.transform.position;
            lookAtPosition.y = 0;
            //仅沿着Up旋转一个方向
            var rotation = Quaternion.LookRotation(lookAtPosition);
            augmentation.transform.rotation = rotation;
        }
        else
        {
            var lookAtPosition = Camera.main.transform.position - augmentation.transform.position;
            lookAtPosition.y = 0;
            var rotation = Quaternion.LookRotation(lookAtPosition);
            augmentation.transform.rotation = rotation;
        }
    }
    
    /// <summary>
    /// 获取当前追踪的anchor数量
    /// </summary>
    /// <returns>The number of active anchors.</returns>
    public int GetNumberOfActiveAnchors()
    {
        int numOfAnchors = 0;

        Vuforia.StateManager stateManager = Vuforia.TrackerManager.Instance.GetStateManager();

        if (stateManager != null)
        {
            foreach (Vuforia.TrackableBehaviour behaviour in stateManager.GetActiveTrackableBehaviours())
            {
                if (behaviour is Vuforia.AnchorBehaviour)
                {
                    numOfAnchors += 1;
                    Debug.Log("Anchor #" + numOfAnchors + ": " + behaviour.TrackableName);
                }
            }
        }
        return numOfAnchors;
    }

    public void ReleaseTexture(Texture tex)
    {
        if (tex)
            Destroy(tex);
    }

    /// <summary>
    /// 返回当前时间的照片序号
    /// </summary>
    /// <returns>The name in time.</returns>
    public string GetNameInTime()
    {
        string tName = DateTime.Now.ToString("yyyyMMddHHmm");
        return tName;
    }
    
    public void ShowStatusBar(bool isShow)
    {
#if UNITY_IOS && !UNITY_EDITOR
        _ShowStatusBar(isShow);
#endif

#if UNITY_ANDROID && !UNITY_EDITOR

#endif
    }
    
    private AndroidJavaObject currentActivity
    {
        get
        {
            return new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        }
    }
 
    /// <summary>
    ///  隐藏上方状态栏
    /// </summary>
    public void HideStatusBar()
    {
#if UNITY_ANDROID
        currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            currentActivity.Call<AndroidJavaObject>("getWindow").Call("clearFlags", 2048);
 
        }));
#endif
        Debug.Log("hide status bar");
    }
 
    /// <summary>
    ///  显示上方状态栏
    /// </summary>
    public void ShowStatusBar()
    {
#if UNITY_ANDROID
        currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
        {
            currentActivity.Call<AndroidJavaObject>("getWindow").Call("addFlags", 2048);// WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN
        }));
#endif
        Debug.Log("show status bar");
    }
}
