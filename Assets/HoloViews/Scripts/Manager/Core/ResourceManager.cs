using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;

/// <summary>
/// 核心管理器：资源管理器。这个管理器将负责程序资源的加载/读取/资源与线上版本的比对/资源的本地存储和删除
/// </summary>
public class ResourceManager : SingletonMonoManager<ResourceManager>
{
    
    public override IEnumerator Init()
    {
        yield return null;
    }

    /// 递归遍历文件夹并对文件夹当中的文件进行处理
    public void CheckFolder(string dir, Action<FileInfo> handler)
    {
        //文件夹信息
        DirectoryInfo dif = new DirectoryInfo(dir);
        //遍历文件夹中的各个子文件夹
        foreach (DirectoryInfo di in dif.GetDirectories())
        {
            CheckFolder(di.FullName, handler);
        }

        foreach (var fileInfo in dif.GetFiles())
        {
            handler.Invoke(fileInfo);
        }
    }

    /// 动态创建文件夹。这里的文件夹放在Application.persistentDataPath下，所以传入的folderPath是相对于Application.persistentDataPath的路径。
    public string CreateFolder(string folderPath)
    {
        string path = Application.persistentDataPath + folderPath;
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        return path;
    }
    
    /// 读取指定文件。这里的文件放在Application.persistentDataPath下，所以传入的folderPath是相对于Application.persistentDataPath的
    /// 路径。传入的fileName带文件名后缀。
    public IEnumerator ReadFile(string fileType, string folderPath, string fileName, Action<UnityWebRequest> callback)
    {
        string fullPath = Application.persistentDataPath + folderPath + "/" + fileName;
        yield return ReadFile(fileType, fullPath, callback);
    }
    
    public IEnumerator ReadFileRealPath(string fileType, string fullPath, Action<UnityWebRequest> callback)
    {
        yield return ReadFile(fileType, fullPath, callback);
    }
    
    
    
    /// 读取指定文件。这里传入的fullPath是带文件名和后缀的完整路径。
    public IEnumerator ReadFile(string fileType, string fullPath, Action<UnityWebRequest> callback = null
        , Func<UnityWebRequest, IEnumerator> corut = null)
    {
        string path = "file:///" + fullPath;
        //这里需要根据传入的文件类型来指定不同的UnityWebRequest
        UnityWebRequest webRequest = null;
        if (fileType == Constant.EResourceType_Text || fileType == Constant.EResourceType_BinaryData)
        {
            webRequest = UnityWebRequest.Get(path);
        }
        else if (fileType == Constant.EResourceType_AssetBundle)
        {
            webRequest = UnityWebRequestAssetBundle.GetAssetBundle(path);
        }
        else if (fileType == Constant.EResourceType_Texture)
        {
            webRequest = UnityWebRequest.Get(path);
        }

        yield return webRequest.SendWebRequest();
        if (corut != null)
            yield return StartCoroutine(corut(webRequest));
        else
            callback?.Invoke(webRequest);
    }

    /// 将传入的字节数组写入文件。这里的文件放在Application.persistentDataPath下，所以传入的folderPath是相对于Application.persistentDataPath的路径。传入的fileName带文件名后缀。如果该目录下有同名旧文件，则会将该文件清空然后重写
    public bool WriteFile(string folderPath, string fileName, byte[] data)
    {
        bool result = false;
        //如果没有指定目录，必须先创建一个否则IOS报错
        CreateFolder(folderPath);
        string fullPath = Application.persistentDataPath + folderPath + "/" + fileName;
        using (var fileStream = File.Open(fullPath, FileMode.OpenOrCreate))
        {
            //先清空原先的文件内容
            fileStream.SetLength(0);
            //写入新内容
            fileStream.Write(data, 0, data.Length);
            result = true;
        }
        return result;
    }
    
    /// 将传入的字符串写入文件
    public void WriteTextFile(string folderPath, string fileName, string str)
    {
        //如果没有指定目录，必须先创建一个否则IOS报错
        CreateFolder(folderPath);
        string fullPath = Application.persistentDataPath + folderPath + "/" + fileName;
        File.WriteAllText(fullPath, str);
    }

    public string ReadTextFile(string folderPath, string fileName)
    {
        //如果没有指定目录，必须先创建一个否则IOS报错
        CreateFolder(folderPath);
        string fullPath = Application.persistentDataPath + folderPath + "/" + fileName;
        return File.ReadAllText(fullPath);
    }
    
    /// 删除文件。这里的文件放在Application.persistentDataPath下，所以传入的folderPath是相对于Application.persistentDataPath的路径。传入的fileName带文件名后缀
    public bool DeleteFile(string folderPath, string fileName)
    {
        bool result = false;
        string path = Application.persistentDataPath + folderPath;
        if (IsFileExist(folderPath, fileName))
        {
            File.Delete(path + "/" + fileName);
            result = true;
        }

        return result;
    }

    /// 删除文件夹
    public bool DeleteFolder(string folderPath)
    {
        bool result = false;
        string path = Application.persistentDataPath + folderPath;
        if (IsFolderExist(folderPath))
        {
            Directory.Delete(path, true);
            result = true;
        }

        return result;
    }

    /// 判断文件是否存在。这里的文件放在Application.persistentDataPath下，所以传入的folderPath是相对于Application.persistentDataPath的路径。传入的fileName带文件名后缀
    public bool IsFileExist(string folderPath, string filesName)
    {
        string path = Application.persistentDataPath + folderPath;
        //Debug.Log(path);
        return File.Exists(path + "/" + filesName);
    }

    /// 文件夹是否存在
    public bool IsFolderExist(string folderPath)
    {
        bool result;
        string path = Application.persistentDataPath + folderPath;
        result = Directory.Exists(path);
        return result;
    }
    
    /// 判断文件是否存在。这里传入的fullPath是包含Application.persistentDataPath在内的完整路径，注意带文件名后缀
    public bool IsFileExist(string fullPath)
    {
        return File.Exists(fullPath);
    }
}
