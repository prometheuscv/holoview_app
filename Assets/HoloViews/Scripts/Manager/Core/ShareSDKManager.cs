using System.Collections;
using cn.sharesdk.unity3d;
using System;
using UnityEngine;

/// <summary>
/// ShareSDK管理器，负责ShareSDK组件的初始化和各类相关设置
/// </summary>
public class ShareSDKManager : SingletonMonoManager<ShareSDKManager>
{
    ShareSDK shareSdk;
    /// <summary>
    /// 标记当前环境下ShareSDK是否可用
    /// </summary>
    public bool isShareSDKAvailable;
    /// <summary>
    /// 获取用户信息的回调函数
    /// </summary>
    private Func<Hashtable, bool> getUserInfoCallback;
    /// <summary>
    /// 进行平台授权的回调函数
    /// </summary>
    private Func<Hashtable, bool> authorizeCallback;


    public override IEnumerator Init()
    {
#if !UNITY_EDITOR
        //获取ShareSDK组件
        shareSdk = gameObject.GetComponent<ShareSDK>();
        //对ShareSDK组件回调进行设定
        shareSdk.shareHandler = ShareResultHandler;
        shareSdk.authHandler = AuthResultHandler;
        shareSdk.showUserHandler = UserResultHandler;
        isShareSDKAvailable = true;
#endif
        yield return null;
    }

    private void UserResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable data)
    {
        switch (state)
        {
            case ResponseState.Success:
                {
                    Debug.Log("get user Json success");
                    //如果获取用户信息回调存在，则调用该回调函数
                    getUserInfoCallback?.Invoke(data);
                    break;
                }
            case ResponseState.Fail:
                {
                    Debug.Log("get user Json fail");
                    break;
                }
            case ResponseState.Cancel:
                {
                    Debug.Log("get user Json cancel");
                    break;
                }
        }
    }

    private void AuthResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable data)
    {
        switch (state)
        {
            case ResponseState.Success:
                {
                    Debug.Log("authorize success");
                    //如果授权回调存在，则调用该回调函数
                    authorizeCallback?.Invoke(data);
                    break;
                }

            case ResponseState.Fail:
                {
                    Debug.Log("authorize fail");
                    break;
                }
            case ResponseState.Cancel:
                {
                    Debug.Log("authorize cancel");
                    break;
                }
        }
    }

    private void ShareResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
    {
        Debug.Log("reqID:" + reqID + " state:" + state + " PlatformType:" + type + " data:" + result);

        if (state == ResponseState.Success)
        {
            print("share successfully - share result :");
            print(MiniJSON.jsonEncode(result));
        }
        else if (state == ResponseState.Fail)
        {
#if UNITY_ANDROID
            print("fail! throwable stack = " + result["stack"].ToString() + "; error msg = " + result["msg"]);
#elif UNITY_IPHONE
			print ("fail! error code = " + result["error_code"] + "; error msg = " + result["error_msg"]);
#endif
        }
        else if (state == ResponseState.Cancel)
        {
            print("cancel !");
        }
    }

    /// <summary>
    /// 平台授权。目前使用的是微信平台
    /// </summary>
    /// <param name="authorizeCallback">Authorize callback.</param>
    public void Authorize(Func<Hashtable, bool> authorizeCallback)
    {
        if (shareSdk.IsClientValid(PlatformType.WeChat))
        {
            //将回调函数传入
            this.authorizeCallback = authorizeCallback;
            shareSdk.CancelAuthorize(PlatformType.WeChat);
            shareSdk.Authorize(PlatformType.WeChat);
        }
    }

    /// <summary>
    /// 通过ShareSDK获取用户在相关平台（目前是微信平台）的用户信息
    /// </summary>
    public void GetUserInfo(Func<Hashtable, bool> getUserInfoCallback)
    {
        if (shareSdk.IsClientValid(PlatformType.WeChat))
        {
            //将回调函数传入
            this.getUserInfoCallback = getUserInfoCallback;
            shareSdk.GetUserInfo(PlatformType.WeChat);
        }
    }

    /// <summary>
    /// 判断平台是否可用并且经过了授权，目前是使用微信平台
    /// </summary>
    /// <returns><c>true</c>, if valid was cliented, <c>false</c> otherwise.</returns>
    public bool IsClientValidAndAuthorized()
    {
        return IsClientValid() && IsClientAuthorized();
    }


    public bool IsClientValid()
    {
        return shareSdk.IsClientValid(PlatformType.WeChat);
    }

    public bool IsClientAuthorized()
    {
        return shareSdk.IsAuthorized(PlatformType.WeChat);
    }


    public void ShareImg(PlatformType type, string uploadFileUrl)
    {
        ShareContent content = new ShareContent();
        string tContentUrl = string.Format("{0}/{1}", Constant.OSS_endPoint, uploadFileUrl);
        content.SetImageUrl(tContentUrl);
        content.SetShareType(ContentType.Image);
        shareSdk.ShareContent(type, content);
    }


    public void ShareVideo(PlatformType type, string uploadFileUrl, string uploadFirstFrameUrl)
    {
        ShareContent content = new ShareContent();
        string tContentUrl = string.Format("{0}/{1}", Constant.OSS_endPoint, uploadFileUrl);
        Debug.Log(tContentUrl);
        string imgUrl = string.Format("{0}/{1}", Constant.OSS_endPoint, uploadFirstFrameUrl);
        string webPageUrl = string.Format("{0}?url={1}&type=video&firstFrame={2}",
        Constant.shareMainDomainName, tContentUrl, imgUrl);
        content.SetText(Constant.EShareTips_slogan);
        
        if (Application.platform == RuntimePlatform.Android)
        {
            content.SetImageUrl(imgUrl);
        }else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            content.SetImageUrl(imgUrl);
        }
        
        content.SetTitle(Constant.EShareTips_title);
        content.SetUrl(webPageUrl);
        content.SetShareType(ContentType.Webpage);
        shareSdk.ShareContent(type, content);
    }
}
