﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using cn.sharesdk.unity3d;
using NatCorder.Clocks;
using NatCorder;
using NatCorder.Inputs;
using UnityEngine.Video;

public class MediaShareManager : SingletonMonoClass<MediaShareManager>
{
    public Texture2D sharePhoto;
    public VideoPlayer videoPlayer;
    private RenderTexture renderTexture;

    public string ShareImageLocalPath;
    public ShareTexs ShareTexs
    {
        get;
        private set;
    }
    /// <summary>
    /// 是否视频
    /// </summary>
    public bool isVideo;
    // [HideInInspector]
    /// <summary>
    /// 是否开始播放视频
    /// </summary>
    public bool isVideoPlaying;
    [HideInInspector]
    /// <summary>
    /// 是否分享到朋友圈
    /// </summary>
    public bool isMomentShare;
    [HideInInspector]
    public string uploadFileUrl;
    [HideInInspector]
    public string uploadFirstFrameUrl;

    private SharePageController sharePageController;

    bool isVideoUploaded;
    bool isFirstFrameUploaded;

    string shareType;

    
    private void Awake()
    {
        Instance = this;
    }
    
    public IEnumerator TakePhoto(Action callback)
    {
        yield return new WaitForEndOfFrame();

        if (sharePhoto != null)
        {
            Destroy(sharePhoto);
        }

        sharePhoto = ScreenCapture.CaptureScreenshotAsTexture();
        if (callback!=null)
        {
            callback.Invoke();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ShareTexs = new ShareTexs();
        renderTexture = new RenderTexture(Screen.width,Screen.height,0);
        videoPlayer.targetTexture = renderTexture;
        videoPlayer.loopPointReached += OnVideoPlayerEnd;
        sharePageController = SharePageController.Instance;
        ShareImageLocalPath = Application.dataPath + "/ShareImage.jpg";
    }

    public void InitVideoPlayer()
    {
        videoPlayer.url = RecordManager.replayResultUrl;
        ShareTexs.result = videoPlayer.targetTexture;

        sharePageController.ShowSharePageVideo(ShareTexs.result);
        StartCoroutine(SetFirstFrame());
    }

    IEnumerator SetFirstFrame()
    {
        Debug.Log("SetFirstFrame");
        AudioManager.Instance.SetMute(true);
        yield return new WaitForSeconds(0.6f);
        RenderTexture currentActiveRT = RenderTexture.active;
        if (!ShareTexs.firstFrame)
        {
            if (Application.platform ==RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsEditor)
            {
                ShareTexs.firstFrame = new Texture2D(videoPlayer.targetTexture.width, videoPlayer.targetTexture.height, TextureFormat.RGBA32, false);
            }
            else if(Application.platform ==RuntimePlatform.IPhonePlayer || Application.platform ==RuntimePlatform.OSXEditor)
            {
                ShareTexs.firstFrame = new Texture2D(videoPlayer.targetTexture.width, videoPlayer.targetTexture.height, TextureFormat.ARGB32, false);
            }
        }
        RenderTexture.active = videoPlayer.targetTexture;
        ShareTexs.firstFrame.ReadPixels(new Rect(0, 0, videoPlayer.targetTexture.width, videoPlayer.targetTexture.height), 0, 0, false);
        ShareTexs.firstFrame.Apply();
        RenderTexture.active = currentActiveRT;
    }

    public void PauseVideo()
    {
        videoPlayer.Pause();
        isVideoPlaying = false;

    }

    public void Update()
    {
        if (videoPlayer.isPlaying)
        {
            double percent = videoPlayer.time / videoPlayer.length;
            sharePageController.SetVideoProgress(percent);
        }
    }

    private void OnVideoPlayerEnd(VideoPlayer source)
    {
        isVideoPlaying = false;
        sharePageController.videoPlayButton.SetActive(true);
    }

    public void PlayVideo()
    {
        videoPlayer.Play();
        isVideoPlaying = true;
    }

    void CheckCallBack(string result)
    {
        if (result.Contains("fail"))
        {
            if (result.Contains(Constant.EShareTips_weixin))
            {
                isMomentShare = false;
                ToastManager.Instance.ShowToastBox(Constant.EShareTips_noWeixinHips);
            }
            else if (result.Contains(Constant.EShareTips_weibo))
                ToastManager.Instance.ShowToastBox(Constant.EShareTips_noWeiboHips);
            else if (result.Contains(Constant.EShareTips_qq))
                ToastManager.Instance.ShowToastBox(Constant.EShareTips_noQQHips);
        }
        else
        {
            sharePageController.HIdeSharePlatform();
            ToastManager.Instance.ShowProgressBox("正在上传", () =>
            {
                ToastManager.Instance.ShowToastBox("上传成功，正在分享",0.5f);
            });
            UploadFile(result);
        }
    }

    public void AndroidShare(string result)
    {
        sharePageController.HIdeSharePlatform();
        ToastManager.Instance.ShowProgressBox("正在上传", () =>
        {
            ToastManager.Instance.ShowToastBox("上传成功，正在分享",0.5f);
        });
        UploadFile(result);
    }

    void UploadFile(string result)
    {
        isVideoUploaded = false;
        isFirstFrameUploaded = false;
        if (isVideo)
        {
            uploadFileUrl = string.Format("{0}{1}/video/{2}.mp4",Constant.OSS_uploadShareFilePath, ArUtilityManager.Instance.GetNameInTime(), ArUtilityManager.Instance.GetNameInTime());//todo 暂时无用户数据 接入账号系统后修改
            uploadFirstFrameUrl = string.Format("{0}{1}/video/{2}_firstFrame.png", Constant.OSS_uploadShareFilePath, ArUtilityManager.Instance.GetNameInTime(), ArUtilityManager.Instance.GetNameInTime());

            NetworkManager.Instance.UploadOssFileByUrlAsync(uploadFileUrl, File.ReadAllBytes(RecordManager.replayResultUrl),
               OnUploadComplete, tagObj: "video", errorCallback: (string error) =>
               {
                   ToastManager.Instance.ShowToastBox("网络异常，分享失败");
               });

            NetworkManager.Instance.UploadOssFileByUrlAsync(uploadFirstFrameUrl, ShareTexs.firstFrame.EncodeToPNG(),
            OnUploadComplete, tagObj: "firstFrame", errorCallback: (string error) =>
            {
                ToastManager.Instance.ShowToastBox("网络异常，分享失败");
            });
        }
        else
        {
            ResourceManager.Instance.WriteFile("","ShareImage.jpg",sharePhoto.EncodeToPNG());
            
            uploadFileUrl = string.Format("{0}{1}/photo/{2}.png", Constant.OSS_uploadShareFilePath,
              ArUtilityManager.Instance.GetNameInTime(), ArUtilityManager.Instance.GetNameInTime());

            NetworkManager.Instance.UploadOssFileByUrlAsync(uploadFileUrl, sharePhoto.EncodeToPNG(),
            OnUploadComplete, tagObj: "photo", errorCallback: (string error) =>
            {
                ToastManager.Instance.ShowToastBox("网络异常，分享失败");
            });
        }
        shareType = result;
    }

    void OnUploadComplete(object arg1, object arg2)
    {
        if ((string)arg2 == "video" || (string)arg2 == "firstFrame")
        {
            if ((string)arg2 == "video")
            {
                NetworkManager.Instance.SetAclToPublicRead(uploadFileUrl);
                isVideoUploaded = true;
            }
            
            if ((string)arg2 == "firstFrame")
            {
                NetworkManager.Instance.SetAclToPublicRead(uploadFirstFrameUrl);
                isFirstFrameUploaded = true;
            }
            
            if (isVideoUploaded && isFirstFrameUploaded)
            {
                Debug.Log("上传视频完成");
                NetworkManager.Instance.SetAclToPublicRead(uploadFileUrl);
                isVideoUploaded = true;
                ShareFile();
            }
        }
        
        if ((string)arg2 == "photo")
        {
            NetworkManager.Instance.SetAclToPublicRead(uploadFileUrl);
            Debug.Log("上传照片完成");
            ShareFile();
        }
    }
    
    //新版分享流程
    void ShareFile()
    {
        ToastManager.Instance.HideProgressBox();
        Debug.Log("shareType"+shareType+" ，isVideo:"+isVideo);
        if (!isVideo)
        {
            if (shareType.Contains(Constant.EShareTips_weixin))
            {
                if (isMomentShare)
                {
                    ShareSDKManager.Instance.ShareImg(PlatformType.WeChatMoments, uploadFileUrl);
                    isMomentShare = false;
                }
                else
                {
                    ShareSDKManager.Instance.ShareImg(PlatformType.WeChat, uploadFileUrl);
                }
            }
            else if (shareType.Contains(Constant.EShareTips_weibo))
            {
                ShareSDKManager.Instance.ShareImg(PlatformType.SinaWeibo, uploadFileUrl);
            }
            else if (shareType.Contains(Constant.EShareTips_qq))
            {
                ShareSDKManager.Instance.ShareImg(PlatformType.QQ, uploadFileUrl);
            }
        }
        else if (isVideoUploaded == true && isFirstFrameUploaded == true)
        {
            if (shareType.Contains(Constant.EShareTips_weixin))
            {
                if (isMomentShare)
                {
                    ShareSDKManager.Instance.ShareVideo(PlatformType.WeChatMoments, uploadFileUrl, uploadFirstFrameUrl);
                    isMomentShare = false;
                }
                else
                {
                    ShareSDKManager.Instance.ShareVideo(PlatformType.WeChat, uploadFileUrl, uploadFirstFrameUrl);
                }

                
            }
            else if (shareType.Contains(Constant.EShareTips_weibo))
            {
                ShareSDKManager.Instance.ShareVideo(PlatformType.SinaWeibo, uploadFileUrl, uploadFirstFrameUrl);
            }
            else if (shareType.Contains(Constant.EShareTips_qq))
            {
                ShareSDKManager.Instance.ShareVideo(PlatformType.QQ, uploadFileUrl, uploadFirstFrameUrl);
            }
            isVideoUploaded = false;
            isFirstFrameUploaded = false;
        }
    }

    private void OnDestroy()
    {
        Destroy(ShareTexs.firstFrame);
        Destroy(ShareTexs.result);
        Destroy(sharePhoto);
    }

    public void Dispose()
    {

    }
}
public class ShareTexs
{
    public Texture2D firstFrame;
    public Texture result;
}
