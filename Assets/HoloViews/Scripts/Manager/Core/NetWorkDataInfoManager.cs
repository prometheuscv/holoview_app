﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

/// <summary>
/// 核心管理器，数据管理器。数据管理器是所有程序非配置类型数据的总入口，通过调用逻辑管理器下的各个子管理器实现各自对应的数据处理
/// </summary>
public class NetworkDataInfoManager : SingletonMonoManager<NetworkDataInfoManager>
{
    public UserDataInfo userDataInfo;
    public UserVideoDataInfo userVideoDataInfo;
    public List<ModelItemDataInfo> homeVideoDataInfo;
    public bool isLogin;
    public bool test;
    public override IEnumerator Init()
    {
        test = false;
        ReadyDownload = 0;
        TaskCount = 0;
        executedTaskCount = 0;
        yield return null;
    }

    public float TaskCount;
    public float executedTaskCount;
    public int ReadyDownload;

    public void GetIndexDatas()
    {
        Debug.Log("getIndexDatas");
        //首页部分
        GetHomeInfo(() =>
        {
            TaskCount += homeVideoDataInfo.Count;
            CheckReadyDownload();
        });
        //个人部分
        if (NetworkManager.Instance.GetToken())
        {
            string path = MainManager.Instance.currentURLType +
                            Constant.getUserInfo;
            //Debug.Log(path);
            NetworkManager.Instance.WebRequestGet(path, (bytes) =>
            {
                if (bytes != null)
                {
                    string data = Encoding.Default.GetString(bytes);
                    Debug.Log(data);
                    userDataInfo = JsonConvert.DeserializeObject<UserDataInfo>(data);

                    isLogin = true;
                    EventManager.Instance.DispathchEvent(Constant.EVENT_UPDATE_INDEX_AVATAR);
                    GetUserVideoDataInfo(() =>
                    {
                        TaskCount += userVideoDataInfo.data.Count;
                        CheckReadyDownload();
                    });

                    return true;
                }
                else
                {
                    ToastManager.Instance.ShowToastBox("网络错误，个人信息拉取失败");
                    CheckReadyDownload();
                    return false;
                }
            }, addToken: true);
        }
        else
        {
            CheckReadyDownload();
        }
    }

    public void CheckReadyDownload()
    {
        ReadyDownload += 1;
        if (ReadyDownload >= 2)
        {
            ReadyDownload = 0;
            if (homeVideoDataInfo != null)
            {
                for (int i = 0; i < homeVideoDataInfo.Count; i++)
                {
                    DownVideoCover(homeVideoDataInfo[i]);
                }
            }

            if (userVideoDataInfo != null)
            {
                if (userVideoDataInfo.data != null)
                {
                    for (int i = 0; i < userVideoDataInfo.data.Count; i++)
                    {
                        DownVideoCover(userVideoDataInfo.data[i]);
                    }
                }
            }
        }
    }

    public void DownVideoCover(ModelItemDataInfo videoItemDataInfo)
    {
        NetworkManager.Instance.GetOssFileByUrlAsync(videoItemDataInfo.image, (data, para) =>
        {
            byte[] resourceData = (byte[])data;
            if (resourceData != null)
            {
                videoItemDataInfo.imageData = resourceData;
                CheckDownVideoCoverProgress();
            }
        });
    }

    public void CheckDownVideoCoverProgress()
    {
        executedTaskCount += 1;
        EventManager.Instance.DispathchEvent(Constant.EVENT_UPDATE_OSS_DOWNLOAD_PROGRESS, executedTaskCount / TaskCount);

        if (executedTaskCount >= TaskCount)
        {
            executedTaskCount = 0;
            ReadyDownload = 0;
            TaskCount = 0;
            EventManager.Instance.DispathchEvent(Constant.EVENT_UPDATE_OSS_DOWNLOAD_COMPLETE);
        }
    }

    //拉取首页视频信息
    public void GetHomeInfo(Action callback)
    {
        string path = MainManager.Instance.currentURLType +
                      Constant.getHomeVideoInfo;
        NetworkManager.Instance.WebRequestGet(path, (bytes) =>
        {
            if (bytes != null)
            {
                string data = Encoding.Default.GetString(bytes);
                Debug.Log(data);
                homeVideoDataInfo = JsonConvert.DeserializeObject<List<ModelItemDataInfo>>(data);
                if (callback != null)
                {
                    callback.Invoke();
                }
                return true;
            }
            else
            {
                return false;
            }
        });
    }

    //获取用户信息
    public void GetUserInfo()
    {
        if (NetworkManager.Instance.GetToken())
        {
            string path = MainManager.Instance.currentURLType +
                         Constant.getUserInfo;

            Debug.Log(path);
            NetworkManager.Instance.WebRequestGet(path, (bytes) =>
            {
                if (bytes != null)
                {
                    string data = Encoding.Default.GetString(bytes);
                    Debug.Log(data);
                    isLogin = true;
                    userDataInfo = JsonConvert.DeserializeObject<UserDataInfo>(data);
                    EventManager.Instance.DispathchEvent(Constant.EVENT_UPDATE_INDEX_AVATAR);
                    GetUserVideoDataInfo(null);
                    return true;
                }
                else
                {
                    ToastManager.Instance.ShowToastBox("网络异常");
                    return false;
                }
            }, addToken: true);
        }
        else
        {

        }
    }

    //获取用户视频信息
    public void GetUserVideoDataInfo(Action callback)
    {
        string path = MainManager.Instance.currentURLType +
                      Constant.getUserVideoInfo;
        //Debug.Log(path);
        NetworkManager.Instance.WebRequestGet(path, (bytes) =>
        {
            if (bytes != null)
            {
                string data = Encoding.Default.GetString(bytes);
                Debug.Log(data);
                userVideoDataInfo = JsonConvert.DeserializeObject<UserVideoDataInfo>(data);
                if (userVideoDataInfo != null)
                {
                    if (callback != null)
                    {
                        callback.Invoke();
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }, addToken: true);
    }

    public void GetVerifySMS(string phoneNum)
    {
        string path = MainManager.Instance.currentURLType +
                      Constant.getVerifySMS + "?phone=" + phoneNum;
        Debug.Log(path);
        NetworkManager.Instance.WebRequestGet(path, (bytes) =>
        {
            if (bytes != null)
            {
                string data = Encoding.Default.GetString(bytes);
                CommonDataInfo info = JsonConvert.DeserializeObject<CommonDataInfo>(data);

                if (info.success == 1)
                {
                    Debug.Log(data);

                    if (test)
                    {
                        ToastManager.Instance.ShowToastBox(info.code);
                    }

                    StartCoroutine(UserPageController.Instance.GetvVerifyNumCountDown(60));
                }
                else
                {
                    UserPageController.Instance.getVerifyNumButton.interactable = true;
                    UserPageController.Instance.UpdateErrorText(info.msg);
                }
                return true;
            }
            else
            {
                return false;
            }
        });
    }

    public void Login(string phoneNum, string verifyCode, Action callback)
    {
        string path = MainManager.Instance.currentURLType +
                      Constant.userSign;
        Debug.Log(path);

        UserLoginJson userLoginJson = new UserLoginJson();
        userLoginJson.phone = phoneNum;
        userLoginJson.code = verifyCode;
        string json = JsonConvert.SerializeObject(userLoginJson);

        NetworkManager.Instance.WebRequestJson(path, json, (bytes) =>
        {
            if (bytes != null)
            {
                string data = Encoding.Default.GetString(bytes);
                Debug.Log(data);

                var userToken = JsonConvert.DeserializeObject<UserToken>(data);
                NetworkManager.Instance.SaveToken(userToken);
                isLogin = true;
                GetUserInfo();
                GetUserVideoDataInfo(() =>
                {
                    EventManager.Instance.DispathchEvent(Constant.EVENT_UPDATE_USER_VIDEO);
                    if (callback != null)
                    {
                        callback.Invoke();
                    }
                });

                return true;
            }
            else
            {
                ToastManager.Instance.ShowToastBox("网络异常");
                return false;
            }
        });
    }

    public void UserQuitLogin()
    {
        isLogin = false;
        string path = MainManager.Instance.currentURLType +
                      Constant.userSignOut;

        Debug.Log(path);
        NetworkManager.Instance.WebRequestJson(path, "", (bytes) =>
         {
             if (bytes != null)
             {
                 NetworkManager.Instance.DeletedToken();
                 string data = Encoding.Default.GetString(bytes);
                 Debug.Log(data);
                 return true;
             }
             else
             {
                 return false;
             }
         }, addToken: true);
    }

    public void DeleteUserVideo(DeleteVideoDataInfo deleteVideoDataInfo, Action callback)
    {
        string path = MainManager.Instance.currentURLType +
                      Constant.batchDeleteUserVideoInfo;

        Debug.Log(path);
        string listStr = JsonConvert.SerializeObject(deleteVideoDataInfo);
        Debug.Log("listStr:" + listStr);
        NetworkManager.Instance.WebRequestJson(path, listStr, (bytes) =>
         {
             if (bytes != null)
             {
                 string data = Encoding.Default.GetString(bytes);
                 CommonDataInfo info = JsonConvert.DeserializeObject<CommonDataInfo>(data);
                 callback?.Invoke();

                 return true;
             }
             else
             {
                 ToastManager.Instance.ShowToastBox("网络错误，删除失败");
                 return false;
             }
         }, addToken: true);
    }
}