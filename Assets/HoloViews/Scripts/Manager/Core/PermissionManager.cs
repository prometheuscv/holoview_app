﻿using System.Collections;
using System.Runtime.InteropServices;

public class PermissionManager : SingletonMonoManager<PermissionManager>
{
    
#if UNITY_IOS 
    [DllImport("__Internal")]
    public static extern void _IsOpenRecord();
    [DllImport("__Internal")]
    public static extern void _IsOpenAlbum();
    [DllImport("__Internal")]
    public static extern void _OpenSystemSetting();
#endif
    
#if UNITY_ANDROID
    public static void _IsOpenRecord()
    {
        
    }

    public static void _IsOpenAlbum()
    {
    }

    public static void _OpenSystemSetting()
    {
    }
#endif
    
    
  

    public override IEnumerator Init()
    {
        yield return null;
    }

    /// <summary>
    /// ios切换权限后会自动重启 暂不使用
    /// </summary>
    /// <param name="base64">Base64.</param>
    //private void OnApplicationFocus(bool focus)
    //{
    //    if (LogicManager.Instance.photoCameraManager != null)
    //    {
    //        if (focus && LogicManager.Instance.photoCameraManager.isNoPermission)
    //        {
    //            LogicManager.Instance.photoCameraManager.isNoPermission = false;
    //            StartCoroutine(LogicManager.Instance.photoCameraManager.CameraReady());
    //        }
    //    }
    //}

    /// <summary>
    /// 是否拥有权限 0无权限
    /// </summary> 
    void PermissionCallback(string base64Result)
    {
        EventManager.Instance.DispathchEvent(Constant.EVENT_GET_PERMISSION_RESULT, base64Result);
    }
}
