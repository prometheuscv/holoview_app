﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 音效管理器
/// </summary>
public class AudioManager : SingletonMonoManager<AudioManager>
{
    /// <summary>
    /// 音效池音效数量
    /// </summary>
    const int POOL_NUM = 5;
    /// <summary>
    /// 常规音效音效池
    /// </summary>
    readonly List<AudioSource> audioPool = new List<AudioSource>();

    /// <summary>
    /// 音效文件字典
    /// </summary>
    readonly Dictionary<string, AudioClip> audioClipsDic = new Dictionary<string, AudioClip>();

    GameObject audios;

    public override IEnumerator Init()
    {
        if (gameObject.GetComponent<AudioListener>() == null)
        {
            gameObject.AddComponent<AudioListener>();
        }

        audios = new GameObject("Audios");
        audios.transform.parent = gameObject.transform;

        for (int i = 0; i < POOL_NUM; i++)
        {
            audioPool.Add(audios.AddComponent<AudioSource>());
        }
        yield return null;
    }

    /// <summary>
    /// 从Unity的Resource目录下加载音效文件
    /// </summary>
    public void LoadAudiosFromResourceFolder()
    {
//        var clips = Resources.LoadAll<AudioClip>(AssetsPathManager.Instance.GetAudioPath());
//
//        foreach (var clip in clips)
//        {
//            audioClipsDic.Add(clip.name, clip);
//        }
    }

    /// <summary>
    /// 从本地AB包当中加载音效文件
    /// </summary>
    /// <param name="assetBundle">Asset bundle.</param>
    public void LoadAudiosFromAssetBundle(AssetBundle assetBundle)
    {
        var clips = assetBundle.LoadAllAssets<AudioClip>();
        foreach (var clip in clips)
        {
            audioClipsDic.Add(clip.name, clip);
        }
    }

    /// <summary>
    /// 获取音效，从音效池中找到一个空闲音效，如果找不到，则拓展音效池
    /// </summary>
    /// <returns>The audio source.</returns>
    public AudioSource GetAudioSource()
    {
        AudioSource audioSource = null;
        for (int i = 0; i < audioPool.Count; i++)
        {
            if (!audioPool[i].isPlaying)
            {
                audioSource = audioPool[i];
                break;
            }
        }
        if (audioSource == null)
        {
            audioSource = audios.AddComponent<AudioSource>();
            audioPool.Add(audioSource);
        }
        return audioSource;
    }

    public AudioSource GetLastAudioSource()
    {
        return audioPool[audioPool.Count - 1];
    }


    /// <summary>
    /// 根据指定的音效文件名从字典中获取对应的音效源文件
    /// </summary>
    /// <returns>The audio clip.</returns>
    /// <param name="audioName">Audio name.</param>
    AudioClip GetAudioClip(string audioName)
    {
        if (audioClipsDic.ContainsKey(audioName))
        {
            AudioClip audioClip = audioClipsDic[audioName];
            return audioClip;
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// 根据指定的音效Id、音量、是否循环等设定，播放对应的音效
    /// </summary>
    /// <param name="audioId">Audio Id.</param>
    /// <param name="volume">Vol.</param>
    /// <param name="loop">If set to <c>true</c> loop.</param>
    public void PlayAudio(int audioId, float volume = 1f, bool loop = false, float time = 0f)
    {
//        if (audioId != 0)
//        {
//            string audioFileName = ConfigInfoManager.Instance.audioCfgMgr.GetAudioFileNameById(audioId);
//            if (audioFileName != null && audioClipsDic.ContainsKey(audioFileName))
//            {
//                AudioClip audioClip = audioClipsDic[audioFileName];
//                DoPlayAudio(audioClip, volume, loop, time);
//            }
//        }
    }

    /// <summary>
    /// 根据指定的音效Id停止播放对应的音效
    /// </summary>
    /// <param name="audioId">Audio Id.</param>
    public void StopAudio(int audioId)
    {
//        string audioFileName = ConfigInfoManager.Instance.audioCfgMgr.GetAudioFileNameById(audioId);
//        if (audioFileName != null && audioClipsDic.ContainsKey(audioFileName))
//        {
//            AudioClip audioClip = audioClipsDic[audioFileName];
//            for (int i = 0; i < audioPool.Count; i++)
//            {
//                if (audioPool[i].clip == audioClip)
//                {
//                    audioPool[i].Stop();
//                }
//            }
//
//        }
    }

    void DoPlayAudio(AudioClip audioClip, float volume, bool loop = false, float time = 0f)
    {
        AudioSource audioSource = GetAudioSource();
        audioSource.priority = 0;
        audioSource.clip = audioClip;
        audioSource.loop = loop;
        audioSource.volume = volume;
        audioSource.time = time;
        audioSource.Play();
        Debug.Log("play audio" + audioSource.clip);
    }


    /// <summary>
    /// 停止所有音效（不包括背景音乐）
    /// </summary>
    public void StopAllAudios()
    {
        for (int i = 0; i < audioPool.Count; i++)
        {
            if (audioPool[i] != null)
            {
                audioPool[i].Stop();
                audioPool[i].clip = null;
            }
        }
    }

    /// <summary>
    /// 设置指定音效的音量
    /// </summary>
    /// <param name="audioName">Audio name.</param>
    /// <param name="voluem">New vol.</param>
    public void SetAudioVolume(string audioName, float voluem)
    {
        AudioClip audioClip = GetAudioClip(audioName);
        for (int i = 0; i < audioPool.Count; i++)
        {
            AudioSource audioSource = audioPool[i];
            if (audioSource.clip == audioClip)
            {
                audioSource.volume = voluem;
            }
        }
    }

    /// <summary>
    /// 设置全部音效和背景音乐的音量
    /// </summary>
    /// <param name="volume">Volume.</param>
    public void ChangeAllVolume(float volume)
    {
        AudioSource audioSource;
        for (int i = 0; i < audioPool.Count; i++)
        {
            audioSource = audioPool[i];
            audioSource.volume = volume;
        }
    }

    /// <summary>
    /// 是否静音
    /// </summary>
    /// <param name="isMute">If set to <c>true</c> is mute.</param>
    public void SetMute(bool isMute)
    {
        AudioListener.volume = isMute ? 0 : 1;
    }

    /// <summary>
    /// 暂停某个音频
    /// </summary>
    /// <returns>The audio.</returns>
    /// <param name="audioId">Audio identifier.</param>
    public float PauseAudio(int audioId)
    {
//        string audioFileName = ConfigInfoManager.Instance.audioCfgMgr.GetAudioFileNameById(audioId);
//        if (audioFileName != null && audioClipsDic.ContainsKey(audioFileName))
//        {
//            AudioClip audioClip = audioClipsDic[audioFileName];
//            for (int i = 0; i < audioPool.Count; i++)
//            {
//                if (audioPool[i].clip == audioClip)
//                {
//                    audioPool[i].Pause();
//                    return audioPool[i].time;
//                }
//            }
//        }
        return 0;
    }


    public void PauseAllAudio()
    {
        AudioListener.pause = true;
    }


    public void ResumeAllAudio()
    {
        AudioListener.pause = false;
    }
}
