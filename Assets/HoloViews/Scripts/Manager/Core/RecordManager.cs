using UnityEngine;
using System.Collections;
using NatCorder.Clocks;
using NatCorder.Inputs;
using NatCorder;
using System.IO;

public class RecordManager : SingletonMonoClass<RecordManager>
{
    [Header("Recording")]
    private int videoWidth;
    private int videoHeight;
    public bool recordMicrophone;

    Camera mainCamera;
    private IMediaRecorder recorder;
    private CameraInput cameraInput;
    private AudioInput audioInput;
    private AudioSource microphoneSource;
    public static string replayResultUrl;
    public bool isRecording;

    private void Awake()
    {
        Instance = this;
    }

    private IEnumerator Start()
    {
        if (!mainCamera)
        {
            mainCamera = Camera.main;
        }
        videoWidth = Screen.width;
        videoHeight = Screen.height;
        // Start microphone
        microphoneSource = gameObject.AddComponent<AudioSource>();
        microphoneSource.mute = false;
        microphoneSource.loop = true;
        microphoneSource.bypassEffects = false;
        microphoneSource.bypassListenerEffects = false;
        microphoneSource.clip = Microphone.Start(null, true, 10, AudioSettings.outputSampleRate);
        yield return new WaitUntil(() => Microphone.GetPosition(null) > 0);
        microphoneSource.Play();
    }

    public void ReadyRecording()
    {
        Debug.Log("ready record");
#if UNITY_IOS && !UNITY_EDITOR
            PermissionManager._IsOpenRecord();
            EventManager.Instance.AddEventListener(Constant.EVENT_GET_PERMISSION_RESULT, OnGetPermissionResult);
#elif UNITY_ANDROID && !UNITY_EDITOR
        StartRecording();
#elif UNITY_EDITOR
        StartRecording();
#endif
    }

    void OnGetPermissionResult(BaseEvent evt)
    {
        EventManager.Instance.RemoveEventListener(Constant.EVENT_GET_PERMISSION_RESULT, OnGetPermissionResult);
        if ((string)evt.Data == "0")
        {
            recordMicrophone = false;
            ToastManager.Instance.ShowDialogBox("请到系统“设置-HoloView”中授权相应功能", "", () => { PermissionManager._OpenSystemSetting(); }, null);
        }
        else
        {
            recordMicrophone = true;
            StartRecording();
        }
    }

    private void OnDestroy()
    {
        if (replayResultUrl != null)
        {
            if (File.Exists(replayResultUrl))
                File.Delete(replayResultUrl);
        }
        Debug.Log("OnDestroy Record");
        // Stop microphone
        microphoneSource.Stop();
        Microphone.End(null);
    }

    public void StartRecording()
    {
        // Start recording
        var frameRate = 30;
        var sampleRate = recordMicrophone ? AudioSettings.outputSampleRate : 0;
        var channelCount = recordMicrophone ? (int)AudioSettings.speakerMode : 0;
        var clock = new RealtimeClock();
        recorder = new MP4Recorder(videoWidth, videoHeight, frameRate, sampleRate, channelCount);
        // Create recording inputs
        cameraInput = new CameraInput(recorder, clock, Camera.main);
        audioInput = recordMicrophone ? new AudioInput(recorder, clock, microphoneSource, true) : null;
        // Unmute microphone
        microphoneSource.mute = audioInput == null;
        isRecording = true;
    }

    public async void StopRecording()
    {
        if (isRecording)
        {
            Debug.Log("StopRecording");
            // Mute microphone
            microphoneSource.mute = true;
            // Stop recording
            audioInput?.Dispose();
            cameraInput.Dispose();
            var path = await recorder.FinishWriting();
            // Playback recording
            Debug.Log($"Saved recording to: {path}");
            //LogUtil.Log("Saved recording to: " + path);
            //删除上一个视频
            if (replayResultUrl != null)
            {
                if (File.Exists(replayResultUrl))
                    File.Delete(replayResultUrl);
            }
            replayResultUrl = path;
            MediaShareManager.Instance.InitVideoPlayer();
            isRecording = false;
        }
        //var prefix = Application.platform == RuntimePlatform.IPhonePlayer ? "file://" : "";
        //Handheld.PlayFullScreenMovie($"{prefix}{path}");
    }
}
