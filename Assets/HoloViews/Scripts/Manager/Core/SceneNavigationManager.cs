﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

/// <summary>
/// 场景导航管理器，负责场景的直接加载、异步加载、预加载以及卸载等方法。
/// </summary>
public class SceneNavigationManager : SingletonMonoManager<SceneNavigationManager>
{
    private AsyncOperation asyncOpt;

    /// <summary>
    /// 当前预加载的场景名称
    /// </summary>
    public string CurPreLoadSceneName { get; private set; }

    public string currentSceneName;
    /// <summary>
    /// 当前预加载场景（如果存在的话）的加载状态————0：无场景预加载，1：场景预加载中，2：场景预加载完成，但是尚未激活切换
    /// </summary>
    public int CurPreLoadSceneState { get; private set; }

    public override IEnumerator Init()
    {
        yield return null;
    }

    /// <summary>
    /// 直接加载场景。常规情况下请采用该方法，简单场景的直接加载比异步加载效率更高用时更短
    /// </summary>
    /// <param name="sceneName">Scene name.</param>
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        currentSceneName = sceneName;
    }

    /// <summary>
    /// 异步加载场景。参数当中的callback为加载完成后的回调函数
    /// </summary>
    /// <param name="sceneName">Scene name.</param>
    /// <param name="isActive">记载完成是否激活</param>
    /// <param name="callback">Callback.</param>
    public void LoadSceneAsync(string sceneName, bool isActive = true, Action<AsyncOperation> callback = null)
    {
        asyncOpt = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        asyncOpt.allowSceneActivation = isActive;
        if (callback != null)
            asyncOpt.completed += callback;
    }

    /// <summary>
    /// 获取预加载目标场景的加载进程
    /// </summary>
    /// <returns>The preloading scene progress.</returns>
    public float GetAsyncLoadedProgress()
    {
        float result = -1;
        if (asyncOpt != null)
            result = asyncOpt.progress;
        return result;
    }

    /// <summary>
    /// 激活并切换到已预加载完成的场景
    /// </summary>
    public void ActiveAsyncLoadedScene()
    {
        if (asyncOpt != null)
            asyncOpt.allowSceneActivation = true;
    }

    /// <summary>
    /// 卸载场景
    /// </summary>
    /// <param name="sceneName">Scene name.</param>
    public void UnLoadScene(string sceneName)
    {
        SceneManager.UnloadSceneAsync(sceneName);
    }

    public void UnLoadCurrentScene()
    {
        SceneManager.UnloadSceneAsync(currentSceneName);
    }
}
