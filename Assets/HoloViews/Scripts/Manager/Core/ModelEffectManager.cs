﻿using System.Collections;
using UnityEngine;

public class ModelEffectManager : SingletonMonoManager<ModelEffectManager>
{
    public bool isDissolved;
    Material dissolveMtr;
    float dissolveDuration;
    float dissolveCache;
    float dissolveStep;
    float dissolveMax;
    const float dissolveStaHeight = 1.2f;
    const float particleStaLifeTime = 1.8f;
    const float parentStaScale = 0.3f;
    const float colliderStaHeight = 0.546615f;

    public override IEnumerator Init()
    {
        yield return null;
    }
    
    // Update is called once per frame
    void Update()
    {
        Dissolve();
    }


    public void InitDissolve(Material mtr, float min, float colliderHeight, float duration, ParticleSystem particleSystem = null, float parentScale = parentStaScale)
    {
        if (!isDissolved)
        {
            isDissolved = true;
            float colliderStaHeightNew = colliderStaHeight * parentScale / parentStaScale;
            float dissolveStaRatio = dissolveStaHeight / colliderStaHeightNew;
            float particleStaRatio = particleStaLifeTime / colliderStaHeightNew;
            dissolveMtr = mtr;
            float maxDissolve = colliderHeight * dissolveStaRatio + min + 0.1f;//offset 0.1fd
            dissolveMax = maxDissolve;
            dissolveDuration = duration;
            dissolveCache = min;
            dissolveStep = (maxDissolve - min) / duration;
            if (particleSystem != null)
            {
                var mainModule = particleSystem.main;
                mainModule.duration = duration;
                mainModule.startLifetime = colliderHeight * particleStaRatio;
                particleSystem.Play();
            }
        }
    }

    void Dissolve()
    {
        if (dissolveMtr != null)
        {
            dissolveCache += Time.deltaTime / dissolveDuration * dissolveStep;
            dissolveMtr.SetFloat("_Fill", dissolveCache);
            if (dissolveCache >= dissolveMax)
                dissolveMtr = null;
        }
    }

    public void Clear()
    {
        isDissolved = false;
        dissolveMtr = null;
    }
}
