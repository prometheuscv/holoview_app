﻿using System.Collections;
using System.Collections.Generic;
using prometheus;
using UnityEngine;
using UnityEngine.UI;

public class PreviewSceneManager : MonoBehaviour
{
    public Image playButtonImage;
    public Sprite[] playButtonSprite;
    public MeshPlayerPlugin meshPlayerPlugin;

    public GameObject groundARButton;
    public GameObject markerARButton;

    // Start is called before the first frame update
    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {

        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXEditor)
        {
            groundARButton.SetActive(true);
            markerARButton.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void OnClickBack()
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_VRScene);
    }

    public void ClosePreviewScene()
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_HomeScene);
    }

    public void ToVR()
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_VRScene);
    }

    public void ToAR()
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_GroundARScene);
    }

    public void ToARMark()
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_MakerARScene);
    }

    public void OnClickPlayVideo()
    {
        if (meshPlayerPlugin.IsPlaying)
        {
            playButtonImage.sprite = playButtonSprite[0];
            meshPlayerPlugin.Pause();
        }
        else
        {
            playButtonImage.sprite = playButtonSprite[1];
            meshPlayerPlugin.Play();
        }
    }
}
