﻿using System;
using System.Collections;
using System.Collections.Generic;
using prometheus;
using UnityEngine;
using UnityEngine.UI;

public class MakerARSceneManager : SingletonMonoClass<MakerARSceneManager>
{
    public Image playButtonImage;
    public Sprite[] playButtonSprite;
    public MeshPlayerPlugin MeshPlayerPlugin;
    public GameObject planeFinderAni;
    public GameObject placeModelTip;
    public GameObject modelPosIndicateLeft;
    public GameObject modelPosIndicateRight;
    public bool needPause;

    private void Start()
    {
        ArUtilityManager.Instance.ShowStatusBar(false);
        EventManager.Instance.AddEventListener(Constant.EVENT_PAUSE_MESHVIDEO, PauseMeshVideo);
    }

    public void PauseMeshVideo(BaseEvent baseEvent)
    {
        bool bo = (bool)baseEvent.Data;

        if (bo)
        {
            if (MeshPlayerPlugin.IsPlaying == true)
            {
                needPause = true;
                MeshPlayerPlugin.Pause();
            }
        }
        else
        {
            if (needPause)
            {
                MeshPlayerPlugin.Play();
            }
            needPause = false;
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus == false)
        {
            OnClickResetPosition();
        }
    }

    //0都隐藏，1左边出现，2右边出现
    public void ShowModelPosIndicate(int pos)
    {
        if (pos == 0)
        {
            modelPosIndicateLeft.SetActive(false);
            modelPosIndicateRight.SetActive(false);
        }
        else if (pos == 1)
        {
            modelPosIndicateLeft.SetActive(true);
            modelPosIndicateRight.SetActive(false);
        }
        else if (pos == 2)
        {
            modelPosIndicateLeft.SetActive(false);
            modelPosIndicateRight.SetActive(true);
        }
    }

    public void SetPlaneFinderAni(bool bo)
    {
        if (bo)
        {
            planeFinderAni.SetActive(true);
        }
        else
        {
            planeFinderAni.SetActive(false);
        }
    }

    public void SetPlaceModelTip(bool bo)
    {
        if (bo)
        {
            placeModelTip.SetActive(true);
        }
        else
        {
            placeModelTip.SetActive(false);
        }
    }

    public void OnClickCloseScene()
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_HomeScene);
    }

    public void OnClickBackScene()
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_PreviewScene);
    }

    public void OnClickResetPosition()
    {
        ArModelManager.Instance.HandleAutomaticHitFocusFalse();

        if (ArTouchManager.Instance != null)
        {
            ArTouchManager.Instance.ResetTouch();
        }
    }

    public void OnClickPlayVideo()
    {
        if (MeshPlayerPlugin.IsPlaying)
        {
            playButtonImage.sprite = playButtonSprite[0];
            MeshPlayerPlugin.Pause();
        }
        else
        {
            playButtonImage.sprite = playButtonSprite[1];
            MeshPlayerPlugin.Play();
        }
    }

    public void OnDestroy()
    {
        RecordManager.Instance.StopRecording();
        ModelEffectManager.Instance.Clear();
        EventManager.Instance.RemoveEventListener(Constant.EVENT_PAUSE_MESHVIDEO, PauseMeshVideo);
    }
}
