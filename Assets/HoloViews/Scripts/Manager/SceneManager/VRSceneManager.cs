﻿using System;
using System.Collections;
using System.Collections.Generic;
using prometheus;
using UnityEngine;
using UnityEngine.UI;

public class VRSceneManager : SingletonMonoClass<VRSceneManager>
{
    public Image playButtonImage;
    public Sprite[] playButtonSprite;
    public MeshPlayerPlugin meshPlayerPlugin;

    public GameObject modelPosIndicateLeft;
    public GameObject modelPosIndicateRight;
    public bool needPause;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        ArUtilityManager.Instance.ShowStatusBar(false);
        EventManager.Instance.AddEventListener(Constant.EVENT_PAUSE_MESHVIDEO, PauseMeshVideo);
    }

    public void PauseMeshVideo(BaseEvent baseEvent)
    {
        bool bo = (bool)baseEvent.Data;

        if (bo)
        {
            if (meshPlayerPlugin.IsPlaying == true)
            {
                needPause = true;
                meshPlayerPlugin.Pause();
            }
        }
        else
        {
            if (needPause)
            {
                meshPlayerPlugin.Play();
            }
            needPause = false;
        }
    }


    //0都隐藏，1左边出现，2右边出现
    public void ShowModelPosIndicate(int pos)
    {
        if (pos == 0)
        {
            modelPosIndicateLeft.SetActive(false);
            modelPosIndicateRight.SetActive(false);
        }
        else if (pos == 1)
        {
            modelPosIndicateLeft.SetActive(true);
            modelPosIndicateRight.SetActive(false);
        }
        else if (pos == 2)
        {
            modelPosIndicateLeft.SetActive(false);
            modelPosIndicateRight.SetActive(true);
        }
    }

    public void OnClickCloseScene()
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_HomeScene);
    }

    public void OnClickBackScene()
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_PreviewScene);
    }

    public void OnClickResetPosition()
    {
        VRTouchManager.Instance.ResetTouch();
        VRModelManager.Instance.ResetModelPos();
    }

    public void OnClickPlayVideo()
    {
        if (meshPlayerPlugin.IsPlaying)
        {
            playButtonImage.sprite = playButtonSprite[0];
            meshPlayerPlugin.Pause();
        }
        else
        {
            playButtonImage.sprite = playButtonSprite[1];
            meshPlayerPlugin.Play();
        }
    }

    public void ShowVRStageChangeScene()
    {
        StageChangeController.Instance.show();
    }

    public void CloseVRStageChangeScene()
    {
        StageChangeController.Instance.hide();
    }

    public void OnDestroy()
    {
        RecordManager.Instance.StopRecording();
        ModelEffectManager.Instance.Clear();
        EventManager.Instance.RemoveEventListener(Constant.EVENT_PAUSE_MESHVIDEO, PauseMeshVideo);
    }
}
