﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class HomeSceneManager : SingletonMonoClass<HomeSceneManager>
{
    public GameObject leftIndicate;
    public Text allItemButtonText;
    public Text downLoadItemButtonText;
    public Image avatarImage;
    public Sprite[] avatarSprites;
    public GameObject videoItemPrefab;
    public List<VideoItemPrefabController> homeVideoItems;
    public List<VideoItemPrefabController> homeLocalVideoItems;
    public List<VideoItemPrefabController> deletedVideoList;

    public GameObject manageItemButton;
    public RectRef homeVideoPageRectRef;
    public CanvasGroup homeVideoPage;
    public GameObject homeVideoParent;
    public CanvasGroup homeLocalVideoPage;
    public GameObject homeLocalVideoParent;
    public GameObject deleteBar;
    public Button deleteButton;
    public Toggle deleteSelectAllToggle;

    public GameObject refTip;

    public bool isLocalVideoManaged;
    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        ArUtilityManager.Instance.ShowStatusBar(true);
        NetworkDataInfoManager.Instance.GetHomeInfo(InitHomeVideoItemsWithLocalItem);
        EventManager.Instance.AddEventListener(Constant.EVENT_UPDATE_INDEX_AVATAR, InitAvatarUI);
        deleteSelectAllToggle.onValueChanged.AddListener(SelectAllToggle);
        InitAvatarUI(null);
        homeVideoPageRectRef.callback1 = (bo) => { refTip.SetActive(bo); };
        homeVideoPageRectRef.callback2 = () => { NetworkDataInfoManager.Instance.GetHomeInfo(InitHomeVideoItems); };
    }

    public void SelectAllToggle(bool bo)
    {
        if (bo)
        {
            for (int i = 0; i < homeLocalVideoItems.Count; i++)
            {
                homeLocalVideoItems[i].toggle.isOn = true;
                //homeLocalVideoItems[i].OnToggle(true);
            }
        }
        else
        {
            for (int i = 0; i < homeLocalVideoItems.Count; i++)
            {
                homeLocalVideoItems[i].toggle.isOn = false;
                //homeLocalVideoItems[i].OnToggle(false);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void InitAvatarUI(BaseEvent baseEvent)
    {
        if (!NetworkDataInfoManager.Instance.isLogin)
        {
            avatarImage.sprite = avatarSprites[0];
        }
        else
        {
            avatarImage.sprite = avatarSprites[1];
        }
    }

    public void InitHomeVideoItemsWithLocalItem()
    {
        CleanVideoItems();
        CleanLocalVideoItems();

        List<ModelItemDataInfo> homeVideoDataInfo = NetworkDataInfoManager.Instance.homeVideoDataInfo;
        for (int i = 0; i < NetworkDataInfoManager.Instance.homeVideoDataInfo.Count; i++)
        {
            var obj = Instantiate(videoItemPrefab, homeVideoParent.transform);
            var comp = obj.GetComponent<VideoItemPrefabController>();
            comp.InitItem(homeVideoDataInfo[i]);
            homeVideoItems.Add(comp);

            if (comp.exist)
            {
                var objLocal = Instantiate(videoItemPrefab, homeLocalVideoParent.transform);
                var compLocal = objLocal.GetComponent<VideoItemPrefabController>();
                compLocal.InitItem(homeVideoDataInfo[i]);
                homeLocalVideoItems.Add(compLocal);
            }
        }
    }

    public void InitHomeVideoItems()
    {
        CleanVideoItems();

        List<ModelItemDataInfo> homeVideoDataInfo = NetworkDataInfoManager.Instance.homeVideoDataInfo;
        for (int i = 0; i < NetworkDataInfoManager.Instance.homeVideoDataInfo.Count; i++)
        {
            var obj = Instantiate(videoItemPrefab, homeVideoParent.transform);
            var comp = obj.GetComponent<VideoItemPrefabController>();
            comp.InitItem(homeVideoDataInfo[i]);
            homeVideoItems.Add(comp);
        }
    }

    public void OnClickIndexAvatarButton()
    {
        if (isLocalVideoManaged)
        {
            OnClickManagerItem();
        }

        if (NetworkDataInfoManager.Instance.isLogin)
        {
            UserPageController.Instance.ShowUserPage();
        }
        else
        {
            UserPageController.Instance.ShowLoginPage();
        }
    }

    public void OnClickAllItemButton()
    {
        if (isLocalVideoManaged)
        {
            OnClickManagerItem();
        }

        ResetTopButton();
        allItemButtonText.color = Color.black;
        allItemButtonText.fontSize = 50;
        allItemButtonText.fontStyle = FontStyle.Bold;
        MoveLeftIndicate(allItemButtonText.gameObject);

        homeVideoPage.alpha = 1;
        homeVideoPage.blocksRaycasts = true;
        homeLocalVideoPage.alpha = 0;
        homeLocalVideoPage.blocksRaycasts = false;

        manageItemButton.SetActive(false);
    }

    public void OnClickDownLoadItemButton()
    {
        if (isLocalVideoManaged)
        {
            OnClickManagerItem();
        }
        ResetTopButton();
        downLoadItemButtonText.color = Color.black;
        downLoadItemButtonText.fontSize = 50;
        downLoadItemButtonText.fontStyle = FontStyle.Bold;
        MoveLeftIndicate(downLoadItemButtonText.gameObject);

        homeVideoPage.alpha = 0;
        homeVideoPage.blocksRaycasts = false;
        homeLocalVideoPage.alpha = 1;
        homeLocalVideoPage.blocksRaycasts = true;

        manageItemButton.SetActive(true);
    }

    public void ResetTopButton()
    {
        if (downLoadItemButtonText.fontSize != 32)
        {
            downLoadItemButtonText.color = new Color(0.52f, 0.51f, 0.56f);
            downLoadItemButtonText.fontStyle = FontStyle.Normal;
            downLoadItemButtonText.fontSize = 32;
        }

        if (allItemButtonText.fontSize != 32)
        {
            allItemButtonText.color = new Color(0.52f, 0.51f, 0.56f);
            allItemButtonText.fontStyle = FontStyle.Normal;
            allItemButtonText.fontSize = 32;
        }
    }

    public void OnClickManagerItem()
    {
        if (isLocalVideoManaged)
        {
            manageItemButton.GetComponentInChildren<Text>().text = "管理";
            deleteBar.SetActive(false);
            deletedVideoList.Clear();

            for (int i = 0; i < homeLocalVideoItems.Count; i++)
            {
                homeLocalVideoItems[i].OpenSeletcedToggle(false);
            }
        }
        else
        {
            for (int i = 0; i < homeLocalVideoItems.Count; i++)
            {
                homeLocalVideoItems[i].OpenSeletcedToggle(true);
            }

            manageItemButton.GetComponentInChildren<Text>().text = "取消";
            deleteBar.SetActive(true);
        }
        isLocalVideoManaged = !isLocalVideoManaged;
    }

    public void OnClickDeleteLocalVideo()
    {
        ToastManager.Instance.ShowDialogBox($"确定删这{deletedVideoList.Count}个资源包吗？", "(仅删除资源包，不删除购买记录)", () =>
        {
            var resourceManager = ResourceManager.Instance;
            var meshVideoManager = MeshVideoManager.Instance;

            for (int i = 0; i < deletedVideoList.Count; i++)
            {
                homeLocalVideoItems.Remove(deletedVideoList[i]);
                string videoName = meshVideoManager.GetNameByPath(deletedVideoList[i].videoItemDataInfo.video);
                if (resourceManager.IsFileExist("/video", videoName))
                {
                    resourceManager.DeleteFile("/video", videoName);
                    Destroy(deletedVideoList[i].gameObject);
                }
            }
            OnClickManagerItem();
            CheckExist();
        }, null, 1);
    }

    public void CheckExist()
    {
        for (int i = 0; i < homeVideoItems.Count; i++)
        {
            homeVideoItems[i].CheckExsit();
        }
    }

    public void OnClickFeedBack()
    {

    }

    public void CleanVideoItems()
    {
        homeVideoItems.Clear();
        foreach (Transform trans in homeVideoParent.transform)
        {
            Destroy(trans.gameObject);
        }
    }

    public void CleanLocalVideoItems()
    {
        homeLocalVideoItems.Clear();
        foreach (Transform trans in homeLocalVideoParent.transform)
        {
            Destroy(trans.gameObject);
        }
    }

    public void MoveLeftIndicate(GameObject tarPos)
    {
        leftIndicate.transform.DOMoveX(tarPos.transform.position.x, 0.2f);
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveEventListener(Constant.EVENT_UPDATE_INDEX_AVATAR, InitAvatarUI);
    }
}
