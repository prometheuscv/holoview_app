﻿using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Loading场景控制器
/// </summary>
public class LoadingSceneController : MonoBehaviour
{
    public Text speedTxt;
    public Text tipsTxt;
    const string tipsOrigin = "疯狂加载中";
    const string tipsDone = "加载完成,解压中";
    int tipsDotCount;
    public Slider slider;
    private float progressValue;
    
    private void Start()
    {
        ArUtilityManager.Instance.ShowStatusBar(false);
        EventManager.Instance.AddEventListener(Constant.EVENT_UPDATE_OSS_DOWNLOAD_PROGRESS,OnUpdateResourceLoadingProgress);
        EventManager.Instance.AddEventListener(Constant.EVENT_UPDATE_OSS_DOWNLOAD_COMPLETE,
            OnLoadResourceComplete);
        EventManager.Instance.AddEventListener(Constant.EVENT_UPDATE_OSS_DOWNLOAD_SPEED,
            OnUpdateOSSDownloadSpeed);
        EventManager.Instance.AddEventListener(Constant.EVENT_OSS_DOWNLOAD_ERROR,
            OnNetworkError);
    }
    
    public void Update()
    {
        slider.value = progressValue;
    }

    private void OnUpdateOSSDownloadSpeed(BaseEvent evt)
    {
        var speed = (long)evt.Data;
        if (speed > 1024)
            speedTxt.text = string.Format("{0:N2}MB/s", speed / 1024f);
        else
            speedTxt.text = string.Format("{0}KB/s", speed);
    }

    private void OnUpdateResourceLoadingProgress(BaseEvent evt)
    {
        var speed = (float)evt.Data;
        progressValue = speed;
    }
    
    private void OnLoadResourceComplete(BaseEvent evt)
    {

    }
    
    private void OnNetworkError(BaseEvent evt)
    {
        SceneNavigationManager.Instance.UnLoadCurrentScene();
        SceneNavigationManager.Instance.LoadScene(Constant.EScene_HomeScene);
        //关闭oss下载速度统计
    }

    public void CancelDownload()
    {
        MeshVideoManager.Instance.CancelDownload();
    }

    private void OnDestroy()
    {
        EventManager.Instance.RemoveEventListener(Constant.EVENT_UPDATE_OSS_DOWNLOAD_PROGRESS,OnUpdateResourceLoadingProgress);
        EventManager.Instance.RemoveEventListener(Constant.EVENT_UPDATE_OSS_DOWNLOAD_COMPLETE,
            OnLoadResourceComplete);
        EventManager.Instance.RemoveEventListener(Constant.EVENT_UPDATE_OSS_DOWNLOAD_SPEED,
            OnUpdateOSSDownloadSpeed);
        EventManager.Instance.RemoveEventListener(Constant.EVENT_OSS_DOWNLOAD_ERROR,
            OnNetworkError);
    }
}
