﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRStageManager : SingletonMonoClass<VRStageManager>
{
    public Material currentSkybox;
    public GameObject currentModel;
    public int CurrentSceneID;
    public int defaultSceneID;
    public  List<VRSceneCfgInfo>  vrSceneCfgInfos;
    // Start is called before the first frame update

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        LoadVRSceneFromResouce(vrSceneCfgInfos[defaultSceneID]);
        if (vrSceneCfgInfos!=null)
        {
            StageChangeController.Instance.InitItem(vrSceneCfgInfos);
        }
    }

    public void LoadVRSceneByID(int id)
    {
        var info = vrSceneCfgInfos[id];
        if (info!=null)
        {
            LoadVRSceneFromResouce(info);
        }
    }

    public void LoadVRSceneFromResouce(VRSceneCfgInfo vrSceneCfgInfo)
    {
        CurrentSceneID = vrSceneCfgInfo.stageId;
        LoadSkyBoxFromResouce(vrSceneCfgInfo.stageId);
        LoadModelFromResouce(vrSceneCfgInfo);
    }

    public void LoadSkyBoxFromResouce(int id)
    {
        currentSkybox = Resources.Load<Material>("VRScene/SkyBox/" + id + "/Skybox_"+id);
        RenderSettings.skybox = currentSkybox;
    }
    
    public void LoadModelFromResouce(VRSceneCfgInfo vrSceneCfgInfo)
    {
        if (currentModel != null)
        {
            Destroy(currentModel);
            currentModel = null;
        }

        var  tempCurrentModel = Resources.Load<GameObject>("VRScene/Model/" + vrSceneCfgInfo.stageId + "/VRScene_"+vrSceneCfgInfo.stageId);
        currentModel = Instantiate(tempCurrentModel,new Vector3(vrSceneCfgInfo.PosX,vrSceneCfgInfo.PosY,vrSceneCfgInfo.PosZ),Quaternion.identity,gameObject.transform);
    }
    
    public Texture2D LoadVRSceneIconByID(int id)
    {
        var vrSceneIcon = Resources.Load<Texture2D>("VRScene/Icon/icon_"+id);
        return vrSceneIcon;
    }

    public void OnDestroy()
    {
        //RenderSettings.skybox = null;
        //RenderSettings.
        //DestroyImmediate(currentSkybox,true);
       // Destroy(currentSkybox);
    }
}
