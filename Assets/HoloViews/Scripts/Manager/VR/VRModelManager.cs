using prometheus;
using UnityEngine;
using Vuforia;

/// <summary>
/// Ar模型管理器
/// </summary>
public class VRModelManager : SingletonMonoClass<VRModelManager>
{
    /// <summary>
    /// ar摄像机
    /// </summary>
    public Camera vrCamera;
    public Vector3 defaultScale;
    public MeshPlayerPlugin meshPlayerPlugin;
    private VRSceneManager vrSceneManager;
    /// <summary>
    /// Ar交互的对象
    /// </summary>
    public GameObject model;

    public bool isModelShow;
    /// <summary>
    /// Ar相机指向模型方向
    /// </summary>
    Vector3 modelDirection;
    /// <summary>
    /// 模型定位器检测扇形幅度 
    /// </summary>
    float locatorFowardRange;
    /// <summary>
    /// 判断是否已经监测到平面
    /// </summary>
    bool groundChecked;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        locatorFowardRange = Mathf.Cos(Mathf.Deg2Rad * 40);
        vrSceneManager = VRSceneManager.Instance;
    }

    void CheckModelPos()
    {
        modelDirection = (model.transform.position - vrCamera.transform.position).normalized;
        //cosRange
        float currentRange = Vector3.Dot(vrCamera.transform.forward, modelDirection);
        //当前角度小于定位角度
        if (currentRange >= locatorFowardRange)
            vrSceneManager.ShowModelPosIndicate(0);
        else
        {
            currentRange = Vector3.Dot(vrCamera.transform.right, modelDirection);
            if (currentRange > 0)
                vrSceneManager.ShowModelPosIndicate(2);
            else if (currentRange < 0)
                vrSceneManager.ShowModelPosIndicate(1);
        }
    }

    private void FixedUpdate()
    {
        CheckModelPos();
    }

    /// <summary>
    /// 重置模型位置 
    /// </summary>
    /// <param name="isResetTransformData">If set to <c>true</c> is reset transform data.</param>
    public void ResetModelPos()
    {
        if (model)
        {
            model.transform.parent.transform.localPosition = Vector3.zero;
            model.transform.parent.transform.localScale = defaultScale;
            model.transform.parent.transform.localEulerAngles = Vector3.zero;
        }
    }

    private void OnDestroy()
    {
    }

}
