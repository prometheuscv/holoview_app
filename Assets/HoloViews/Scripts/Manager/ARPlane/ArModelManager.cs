using prometheus;
using UnityEngine;
using Vuforia;

/// <summary>
/// Ar模型管理器
/// </summary>
public class ArModelManager : SingletonMonoClass<ArModelManager>
{
    /// <summary>
    /// ar摄像机
    /// </summary>
    public Camera arCamera;
    public GameObject planeFinder;
    public Vector3 defaultScale;
    public MeshPlayerPlugin meshPlayerPlugin;
    private ARSceneManager arSceneManager;
    public bool isARMaker;
    /// <summary>
    /// Ar交互的对象
    /// </summary>
    public GameObject model;

    public bool isModelShow;
    /// <summary>
    /// Ar相机指向模型方向
    /// </summary>
    Vector3 modelDirection;
    /// <summary>
    /// 模型定位器检测扇形幅度 
    /// </summary>
    float locatorFowardRange;
    /// <summary>
    /// 判断是否已经监测到平面
    /// </summary>
    bool groundChecked;

    public GameObject light;

    private bool inited;
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        locatorFowardRange = Mathf.Cos(Mathf.Deg2Rad * 40);
        arSceneManager = ARSceneManager.Instance;
        light.SetActive(true);
    }

    void CheckModelPos()
    {
        modelDirection = (model.transform.position - arCamera.transform.position).normalized;
        //cosRange
        float currentRange = Vector3.Dot(VuforiaManager.Instance.ARCameraTransform.forward, modelDirection);
        //当前角度小于定位角度
        if (currentRange >= locatorFowardRange)
            arSceneManager.ShowModelPosIndicate(0);
        else
        {
            currentRange = Vector3.Dot(VuforiaManager.Instance.ARCameraTransform.right, modelDirection);
            if (currentRange > 0)
                arSceneManager.ShowModelPosIndicate(2);
            else if (currentRange < 0)
                arSceneManager.ShowModelPosIndicate(1);
        }
    }

    private void FixedUpdate()
    {
        if (isModelShow)
        {
            CheckModelPos();
        }
    }

    /// <summary>
    /// 重置模型位置 
    /// </summary>
    /// <param name="isResetTransformData">If set to <c>true</c> is reset transform data.</param>
    public void ResetModelPos(bool isResetTransformData = true)
    {
        if (model)
        {
            if (isARMaker)
            {
                model.transform.localPosition = Vector3.zero;
                model.transform.localScale = defaultScale;
                model.transform.localEulerAngles = Vector3.zero;
            }
            else
            {
                model.transform.parent.transform.localPosition = Vector3.zero;
                model.transform.parent.transform.localScale = defaultScale;
                model.transform.parent.transform.localEulerAngles = Vector3.zero;
            }


        }
    }

    public void HandleAutomaticHit()
    {
        Debug.Log("HandleAutomaticHit");
        if (!groundChecked)
        {
            isModelShow = true;
            groundChecked = true;

            if (inited == false)
            {
                inited = true;
                model.GetComponent<MeshRenderer>().enabled = true;
            }
            else
            {
                model.SetActive(true);
            }

            planeFinder.SetActive(false);
            //meshPlayerPlugin.PlayNewVideo(meshPlayerPluginTestUrl);
            ARSceneManager.Instance.SetPlaceModelTip(false);
        }
    }

    public void HandleAutomaticHitFocusFalse()
    {
        ResetModelPos();
        Debug.Log("HandleAutomaticHitFocusFalse");
        if (groundChecked)
        {
            isModelShow = false;
            groundChecked = false;
            model.SetActive(false);
            planeFinder.SetActive(true);
            ARSceneManager.Instance.SetPlaceModelTip(true);
        }
    }

    private void OnDestroy()
    {
        //VuforiaManager.Instance.
        Destroy(VuforiaRenderer.Instance.VideoBackgroundTexture);
        //VuforiaBehaviour.Instance

    }

}
