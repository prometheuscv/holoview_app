﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Reflection;
using Crosstales.FB;
using prometheus;

public class FileManager : MonoBehaviour
{
    public MeshPlayerPlugin meshPlayerPlugin;
    public void OpenFolder()
    {

        //string path = FileBrowser.OpenSingleFile("Open single file", testPath, new ExtensionFilter("Image Files", "png", "jpg", "jpeg"), new ExtensionFilter("Sound Files", "mp3", "wav"), new ExtensionFilter(Util.Constants.TEXT_ALL_FILES, "*"));
        //string path = FileBrowser.OpenSingleFile("Open single file", testPath, "txt", "jpg", "pdf");
        //string path = FileBrowser.OpenSingleFile("txt");
        string path = FileBrowser.OpenSingleFile("mp4");

        Debug.Log("Selected file: " + path);

    }

    public void OpenSingleFile()
    {
        //string path = FileBrowser.OpenSingleFile("Open single file", testPath, new ExtensionFilter("Image Files", "png", "jpg", "jpeg"), new ExtensionFilter("Sound Files", "mp3", "wav"), new ExtensionFilter(Util.Constants.TEXT_ALL_FILES, "*"));
        //string path = FileBrowser.OpenSingleFile("Open single file", testPath, "txt", "jpg", "pdf");
        //string path = FileBrowser.OpenSingleFile("txt");
        string path = FileBrowser.OpenSingleFile("mp4");
        //meshPlayerPlugin.sourceUrl = path;
        Debug.Log("Selected file: " + path);
    }

    public void OpenFiles()
    {
        //string[] paths = FileBrowser.OpenFiles("Open files", testPath, new ExtensionFilter("Image Files", "png", "jpg", "jpeg"), new ExtensionFilter("Sound Files", "mp3", "wav"), new ExtensionFilter(Util.Constants.TEXT_ALL_FILES, "*"));
        //string[] paths = FileBrowser.OpenFiles("txt", "jpg", "pdf");
        //string[] paths = FileBrowser.OpenFiles("txt");
        string[] paths = FileBrowser.OpenFiles();

        Debug.Log("Selected file: " + paths);
    }

    public void OpenSingleFolder()
    {
        //string path = FileBrowser.OpenSingleFolder("Open folder", testPath);
        string path = FileBrowser.OpenSingleFolder();

        Debug.Log("Selected file: " + path);
    }

    public void OpenFolders()
    {
        //string[] paths = FileBrowser.OpenFolders("Open folders", testPath);
        string[] paths = FileBrowser.OpenFolders();

        Debug.Log("Selected file: " + paths);
    }

    public void SaveFile()
    {
        //string path = FileBrowser.SaveFile("Save file", testPath, "MySaveFile", new ExtensionFilter("Binary", "bin"), new ExtensionFilter("Text", "txt", "md"), new ExtensionFilter("C#", "cs"));
        //string path = FileBrowser.SaveFile("Save file", testPath, "MySaveFile", "bin", "txt", "cs");
        //string path = FileBrowser.SaveFile("MySaveFile", "txt");
        string path = FileBrowser.SaveFile();

        Debug.Log("Selected file: " + path);
    }

    public void OpenFilesAsync()
    {
        //FileBrowser.OpenFilesAsync((string[] paths) => { writePaths(paths); }, "Open files", testPath, true, new ExtensionFilter("Image Files", "png", "jpg", "jpeg"), new ExtensionFilter("Sound Files", "mp3", "wav"), new ExtensionFilter(Util.Constants.TEXT_ALL_FILES, "*"));
        //FileBrowser.OpenFilesAsync((string[] paths) => { writePaths(paths); }, "Open files", testPath, true, "txt", "png");
        //FileBrowser.OpenFilesAsync((string[] paths) => { writePaths(paths); }, true, "txt");
        FileBrowser.OpenFilesAsync(paths => { Debug.Log("Selected file: " + paths); ; });
    }

    public void OpenFoldersAsync()
    {
        //FileBrowser.OpenFoldersAsync((string[] paths) => { writePaths(paths); }, "Open folders", testPath, true);
        FileBrowser.OpenFoldersAsync(paths => { Debug.Log("Selected file: " + paths); ; });
    }
}