﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserVideoDataInfo
{
    public List<ModelItemDataInfo> data;
}
