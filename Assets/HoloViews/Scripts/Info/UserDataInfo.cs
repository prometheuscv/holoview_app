﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserDataInfo
{
    public string id;
    public string name;
    public string avatar;
    public string phone;
}

[System.Serializable]
public class UserLoginInfo
{
    public string token;
    public int expire;
    public string refresh_token;
}

[System.Serializable]
public class UserLoginJson
{
    public string phone;
    public string code;
}

[System.Serializable]
public class UserToken
{
    public string token;
    public string expire;
    public string refresh_token;
}
