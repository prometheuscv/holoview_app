using System;

public struct PurchasedProduct
{
    public string productId;
    public DateTime date;

    public PurchasedProduct(string productId, DateTime date)
    {
        this.productId = productId;
        this.date = date;
    }
}