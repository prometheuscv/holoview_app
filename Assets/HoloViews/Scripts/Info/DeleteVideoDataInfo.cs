﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DeleteVideoDataInfo
{
    public List<string> ids;
}
