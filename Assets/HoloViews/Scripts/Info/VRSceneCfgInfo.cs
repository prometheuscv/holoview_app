﻿// ender
// 2019/5/22
using System.Collections.Generic;

[System.Serializable]
/// <summary>
/// Uma_Slot配置信息，对应着SlotCfg.json当中的配置字段
/// </summary>
public class VRSceneCfgInfo
{
    public int stageId;
    public float PosX;
    public float PosY;
    public float PosZ;
    public float Scale;
}



