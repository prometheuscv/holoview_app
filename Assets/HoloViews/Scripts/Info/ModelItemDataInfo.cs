﻿
[System.Serializable]
public class ModelItemDataInfo
{
    public string id;
    public string title;
    public string video;
    public string info;
    public string image;
    public string size;
    public byte[] imageData;
}
