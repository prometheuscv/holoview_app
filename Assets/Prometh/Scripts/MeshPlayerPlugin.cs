/* Mesh Player Plugin for Unity.
*  All rights reserved. Prometheus 2020.
*  Contributor(s): Neil Z. Shao, Hongyi Li.
*/
using UnityEngine;
using System.Runtime.InteropServices;
using System.Collections;
using UnityEditor;

namespace prometheus
{
    public enum SOURCE_TYPE
    {
        PLAYBACK = 0,
        RTMP = 2,
    }

    public class MeshPlayerPlugin : MonoBehaviour
    {

        #region Properties
        //-----------------------------//
        //-  PROPERTIES               -//
        //-----------------------------//
        public float CurrentSec { get { return mCurrentPts; } set { GotoSecond(value); } }
        public int CurrentFrame { get { return mCurrentFrameIdx; } set { GotoFrameIdx((int)value); } }
        public float SourceDurationSec { get { return mSourceDurationSec; } }
        public float FPS { get { return mSourceFPS; } }
        public int SourceNbFrames { get { return mSourceNbFrames; } }

        public bool IsInitialized { get { return mIsInitialized; } }
        public bool IsOpened { get { return mIsOpened; } }
        public bool AutoPlay { get { return mAutoPlay; } set { mAutoPlay = value; } }
        public bool IsPlaying { get { return mIsPlaying; } set { mIsPlaying = value; } }
        public bool Loop { get { return mIsLoop; } set { mIsLoop = value; } }

        public SOURCE_TYPE SourceType { get { return mSourceType; } set { mSourceType = value; } }
        public string SourceUrl { get { return mSourceUrl; } set { mSourceUrl = value; } }
        public bool DataInStreamingAssets { get { return mDataInStreamingAssets; } set { mDataInStreamingAssets = value; } }
        //public string SequenceDataPath { get { return _mainDataPath; } set { _mainDataPath = value; } }

        public float SpeedRatio { get { return mSpeedRatio; } set { mSpeedRatio = value; } }
        public float PreviewSec { get { return mPreviewSec; } set { mPreviewSec = value; } }

        public bool DebugInfo { get { return mDebugInfo; } set { mDebugInfo = value; } }

        public bool IsVfxMtr { get; private set; }
        #endregion

        #region Variables
        //-----------------------------//
        //-  Variables                -//
        //-----------------------------//
        public MeshReader mReader = null;

        [SerializeField] private string mSourceUrl;
        [SerializeField] private SOURCE_TYPE mSourceType = SOURCE_TYPE.PLAYBACK;
        [SerializeField] private bool mDataInStreamingAssets = false;

        [SerializeField] private float mSourceDurationSec = 0;
        [SerializeField] private float mSourceFPS = 0;
        [SerializeField] private int mSourceNbFrames = 0;

        [SerializeField] private float mCurrentPts = -1;
        [SerializeField] private int mCurrentFrameIdx = 0;
        [SerializeField] private float mStartSecond = 0;
        [SerializeField] private float mEndSecond = -1;
        [SerializeField] private float mSpeedRatio = 1.0f;
        [SerializeField] private float mPreviewSec = -1.0f;

        private bool mIsInitialized = false;
        private bool mIsOpened = false;
        private int mApiKey = -1;
        [HideInInspector]
        public bool mIsPlaying = false;
        [SerializeField] private bool mAutoPlay = true;
        [SerializeField] private bool mIsLoop = true;

        [SerializeField] private bool mDebugInfo = true;

        // gui handler
        private MeshFilter mMeshComponent;
        private Renderer mRendererComponent;
        private MeshCollider mMeshCollider;

        // mesh buffer
        private Mesh[] mMeshes = null;
        private Texture2D[] mTextures = null;
        private int mBufferSize = 2;
        private int mBufferIdx = 0;

        private bool mIsMeshDataUpdated = false;

        // short max
        private const int MAX_SHORT = 65535;

        #endregion


        #region Events
        //-----------------------------//
        //-  EVENTS                   -//
        //-----------------------------//
        public delegate void EventFDV();
        public event EventFDV OnNewModel;
        public event EventFDV OnModelNotFound;
        public event EventFDV OnLastFrame;
        #endregion

        #region Functions
        //-----------------------------//
        //- Functions                 -//
        //-----------------------------//
        public void GotoSecond(float sec)
        {
            mCurrentPts = sec;
            if (mReader == null)
                return;

            Debug.Log("[MeshPlayerPlugin] GotoSecond(): " + sec);
            mReader.StartFromSecond(sec);
        }

        public bool GotoFrameIdx(int frmIdx)
        {
            if (mReader != null)
                return mReader.StartFromFrameIdx(frmIdx);

            return false;
        }

        public void Initialize()
        {
            if (mIsInitialized && mReader != null)
                return;

            Debug.Log("[MeshPlayerPlugin] Initialize()");

            // disable if cannot compile -->
            if (mDebugInfo)
            {
                DebugDelegate callback_delegate = new DebugDelegate(CallbackDebug);
                System.IntPtr intptr_delegate = Marshal.GetFunctionPointerForDelegate(callback_delegate);
                ReaderAPI.SetDebugFunction(intptr_delegate);
            }
            // <-- disable if cannot compile

            // create reader
            if (mReader == null)
            {
                Debug.Log("[MeshPlayerPlugin] Create Reader Instance");
                mReader = MeshReader.CreateMeshReader(mApiKey);
                if (mReader == null)
                {
                    Debug.Log("[MeshPlayerPlugin][WARNING] Create Reader Instance Failed");
                    OnModelNotFound?.Invoke();
                    return;
                }
                Debug.Log("[MeshPlayerPlugin] Create Reader Success");
            }

            mMeshComponent = GetComponent<MeshFilter>();
            mRendererComponent = GetComponent<Renderer>();
            mMeshCollider = GetComponent<MeshCollider>();

            mIsInitialized = true;
        }

        public void Uninitialize()
        {
            if (!mIsInitialized)
                return;

            ReaderAPI.UnityPluginUnload();
            mReader = null;
            mIsOpened = false;

            //_isSequenceTriggerON = false;
            mIsInitialized = false;

#if UNITY_EDITOR
            //EditorApplication.pauseStateChanged -= HandlePauseState;
#endif
        }

        public void OpenSource(string str)
        {
            mSourceUrl = str;
            OpenCurrentSource(allowAutoPlay: true);
        }

        public bool OpenCurrentSource(bool allowAutoPlay = true)
        {

            //Test 使用
            //IsVfxMtr = true;
            Initialize();

            if (mSourceUrl == "")
                return false;

            Debug.Log("[MeshPlayerPlugin] Open " + mSourceUrl);
            mIsOpened = mReader.OpenMeshStream(mSourceUrl, mDataInStreamingAssets);

            if (mIsOpened)
            {
                mSourceDurationSec = mReader.SourceDurationSec;
                mSourceFPS = mReader.SourceFPS;
                mSourceNbFrames = mReader.SourceNbFrames;
                AllocMeshBuffers();

                mReader.SetSpeedRatio(mSpeedRatio);
                mCurrentPts = -1;
                mReader.StartFromSecond(mStartSecond);
                if (allowAutoPlay && mAutoPlay)
                {
                    Debug.Log("[MeshPlayerPlugin] Auto Play");
                    Play();
                }
                else
                    Preview();
            }
            else
            {
                Debug.Log("[MeshPlayerPlugin] Open Failed!");
            }

            return mIsOpened;
        }

        private void AllocMeshBuffers()
        {
            //Allocates objects buffers for double buffering
            mMeshes = new Mesh[mBufferSize];
            mTextures = new Texture2D[mBufferSize];

            for (int i = 0; i < mBufferSize; i++)
            {
                //Mesh
                Mesh mesh = new Mesh();

                Bounds newBounds = mesh.bounds;
                newBounds.extents = new Vector3(2, 2, 2);
                mesh.bounds = newBounds;
                mMeshes[i] = mesh;
            }

            for (int i = 0; i < mBufferSize; i++)
            {
                //Texture
                Texture2D texture = new Texture2D(mReader.TextureWidth, mReader.TextureHeight, mReader.TextFormat, false)
                {
                    wrapMode = TextureWrapMode.Clamp,
                    filterMode = FilterMode.Bilinear
                };
                texture.Apply(); //upload to GPU
                mTextures[i] = texture;
            }

            mBufferIdx = 0;
        }

        public void Play()
        {
            if (mReader == null)
                return;

            if (!mIsPlaying)
            {
                StartCoroutine("SequenceTrigger");

                Debug.Log("[MeshPlayerPlugin] Play()");
                mReader.Play();
                mIsPlaying = true;
            }
        }

        public void Pause()
        {
            if (mReader == null)
                return;

            if (mIsPlaying)
            {
                StopCoroutine("SequenceTrigger");

                Debug.Log("[MeshPlayerPlugin] Pause()");
                mReader.Pause();
                mIsPlaying = false;
            }
        }

        public void Preview()
        {
            if (mReader == null)
                return;

            Debug.Log("[MeshPlayerPlugin] Preview()");
            Pause();
            GotoSecond(mPreviewSec);
            mReader.ForwardOneFrame();

            mIsPlaying = true;
            UpdateMesh();
            Update();
            mIsPlaying = false;
        }

        public void Destroy()
        {
            Pause();
            Uninitialize();
            mPreviewSec = -1.0f;

            if (mMeshes != null)
            {
                for (int i = 0; i < mMeshes.Length; i++)
                    DestroyImmediate(mMeshes[i]);
                mMeshes = null;
            }
            if (mTextures != null)
            {
                for (int i = 0; i < mTextures.Length; i++)
                    DestroyImmediate(mTextures[i]);
                mTextures = null;
            }
        }

        #endregion


        #region routine
        // callback for debug
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void DebugDelegate(string str);

        [AOT.MonoPInvokeCallback(typeof(DebugDelegate))]
        static void CallbackDebug(string str)
        {
            Debug.Log(str);
        }

        void InitPlugin()
        {
            Debug.Log("[MeshPlayerPlugin] InitPlugin()");
#if UNITY_WEBGL && !UNITY_EDITOR
		    RegisterPlugin();
#endif
            Uninitialize();

            mIsInitialized = false;
            mIsOpened = false;
            mIsPlaying = false;

            mApiKey = ReaderAPI.CreateApiInstance();
            Initialize();
        }

        void Start()
        {
            Debug.Log("[MeshPlayerPlugin] Start()");

            if (!mIsInitialized)
                Initialize();

            // disable if cannot compile -->
            if (mDebugInfo)
            {
                DebugDelegate callback_delegate = new DebugDelegate(CallbackDebug);
                System.IntPtr intptr_delegate = Marshal.GetFunctionPointerForDelegate(callback_delegate);
                ReaderAPI.SetDebugFunction(intptr_delegate);
            }
            // <-- disable if cannot compile

            if (mReader == null)
                return;

            if (mSourceUrl != null)
                OpenCurrentSource();
        }

        public void SetMaterial(Material mat, bool isVfxMtr = false)
        {
            mRendererComponent.material = mat;
            IsVfxMtr = isVfxMtr;
        }

        void Update()
        {
            if (mDebugInfo)
                Debug.Log("[MeshPlayerPlugin] Update()");

            if (!mIsInitialized)
                Initialize();

            if (!mIsOpened)
                return;

            if (mIsPlaying && mIsMeshDataUpdated)
            {
                Mesh mesh = mMeshes[mBufferIdx];
                mesh.MarkDynamic();
                if (mReader.MeshData.vertices.Length > MAX_SHORT)
                    mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

                mesh.vertices = mReader.MeshData.vertices;
                mesh.normals = mReader.MeshData.normals;
                mesh.uv = mReader.MeshData.uv;
                mesh.triangles = mReader.MeshData.triangles;
                mesh.UploadMeshData(false);

                Texture2D texture = mTextures[mBufferIdx];
                //texture.LoadRawTextureData(mReader.MeshData.colors);
                texture.SetPixels32(mReader.MeshData.colors);
                texture.Apply();

                // display
                mMeshComponent.sharedMesh = mesh;


#if UNITY_EDITOR
                if (IsVfxMtr)
                {
                    mRendererComponent.sharedMaterial.SetTexture("_Emission", texture);
                    mRendererComponent.sharedMaterial.SetTexture("_Albedo", texture);
                }
                else
                    //如果场景中需要多个meshPlayerPlugin实例，将sharedMaterial替换成Material
                    mRendererComponent.sharedMaterial.mainTexture = texture;
#else
                if (IsVfxMtr)
                {
                    mRendererComponent.material.SetTexture("_Emission", texture);
                    mRendererComponent.material.SetTexture("_Albedo", texture);
                }
                else
                    mRendererComponent.material.mainTexture = texture;
#endif

                //                if (mRendererComponent.sharedMaterial.HasProperty("_BaseMap"))
                //                    mRendererComponent.sharedMaterial.SetTexture("_BaseMap", texture);
                //                else if (mRendererComponent.sharedMaterial.HasProperty("_BaseColorMap"))
                //                    mRendererComponent.sharedMaterial.SetTexture("_BaseColorMap", texture);
                //                else if (mRendererComponent.sharedMaterial.HasProperty("_UnlitColorMap"))
                //                    mRendererComponent.sharedMaterial.SetTexture("_UnlitColorMap", texture);
                //                else
                //                {
                //#if UNITY_EDITOR
                //                    var tempMaterial = new Material(mRendererComponent.sharedMaterial);
                //                    tempMaterial.mainTexture = texture;
                //                    mRendererComponent.sharedMaterial = tempMaterial;
                //#else
                //                    mRendererComponent.material.mainTexture = texture;
                //#endif
                //                }

                // done with buffer
                mBufferIdx = (mBufferIdx + 1) % mBufferSize;

                // event
                OnNewModel?.Invoke();
                mIsMeshDataUpdated = false;

                if (mMeshCollider && mMeshCollider.enabled)
                    mMeshCollider.sharedMesh = mesh;
            }
        }

        void UpdateMesh()
        {
            if (mDebugInfo)
                Debug.Log("[MeshPlayerPlugin] UpdateMesh()");

            float ptsSec = -1;
            if (mIsPlaying && mReader.ReadNextFrame(ref ptsSec))
            {
                mIsMeshDataUpdated = true;
            }
        }

        private IEnumerator SequenceTrigger()
        {
            float triggerRate = 0.3f;
            float gap = (triggerRate / (float)mReader.SourceFPS);
            Debug.Log("[SequenceTrigger] Trigger Gap = " + gap);

            //infinite loop to keep executing this coroutine
            while (true)
            {
                UpdateMesh();
                yield return new WaitForSeconds(gap);
            }
        }

        private void OnDestroy()
        {
            if (mDebugInfo)
                Debug.Log("[MeshPlayerPlugin] OnDestroy()");
            Destroy();
        }

        void Awake()
        {
            Debug.Log("[MeshPlayerPlugin] Awake()");
            Uninitialize();

            if (SourceUrl != "")
                Initialize();

            //Hide preview mesh
            if (mMeshComponent != null)
                mMeshComponent.mesh = null;

            //#if UNITY_EDITOR
            //            EditorApplication.pauseStateChanged += HandlePauseState;
            //#endif
        }
        #endregion
    }


}
